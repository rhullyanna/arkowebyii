<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Responsavel;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'grupo_nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'responsavel_cpf')->textInput(['maxlength' => true, 'class'=>'form-control cpf']) ?>

    <div class="btn btn-primary" onclick="getResponsavelAjax()">Consultar CPF</div>

    <?= $form->field($model, 'responsavel_id')->
        dropDownList(ArrayHelper::map(Responsavel::find()->all(), 'id', 'nome'), 
        ['prompt' => '','disabled'=>true]) 
    ?>
    

    <?= $form->field($model, 'grupo_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem;?>";
    
    function getResponsavelAjax(){
        let retorno = false;
        let urlbase = window.location.origin;
        var request = $.ajax({
        url: urlbase+"/responsavel/consultaresp",
        type: "GET",
        data: {cpf : $("#grupo-responsavel_cpf").val()},
        dataType: ""
        });

        request.done(function(msg) {
            msg = JSON.parse(msg);
            $("#grupo-responsavel_id").val(msg.id);
            ;
        }).then(function(){
            console.log('saff');
            retorno = true;
        });

        request.fail(function(jqXHR, textStatus) {
            if(jqXHR.status){
                sweetAlert('info', 'CPF' ,'Informe um cpf valido');
            }
        });

        return retorno;
    }
    $(document).ready(function($){
        $('#grupo-grupo_status').val('ATIVO');
        $('.cpf').mask("999.999.999-99");    
        if(mensagem != ""){
            sweetAlert('info','Consulte CPF','Consulte cpf para validar o responsavel!')
        }
    })
</script>