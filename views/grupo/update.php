<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */

$this->title = 'Alterar Grupo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grupo-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form-alterar-com-responsavel', [
        'model' => $model,
        'responsavel' => $responsavel,
    ]) ?>

</div>
