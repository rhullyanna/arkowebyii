<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */

$this->title = 'Cadastrar Grupo';
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-create">

    <?= $this->render('_form-com-responsavel', [
        'model' => $model,
        'responsavel' => $responsavel,
        'mensagem' => $mensagem
    ]) ?>

</div>
