<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sexo;
use app\models\Pcd;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-form">

    <?php $form = ActiveForm::begin(); ?>

    <fieldset class="fieldset-grupo">
        <legend>Dados Grupo</legend>
        <?= $form->field($model, 'nome_grupo')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    </fieldset>

    <fieldset class="fieldset-grupo">
        <legend>Dados Responsavel</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'nome_responsavel')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'nascimento')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'sexo_id')
                            ->dropDownList(
                                ArrayHelper::map(Sexo::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione um sexo','name'=>'Grupo[sexo_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'rg')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'cpf')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'telefone')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'pcd_id')
                            ->dropDownList(
                                ArrayHelper::map(Pcd::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Grupo[pcd_id]' ,'disabled'=>true]    // options
                            ); ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <?php //echo Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>
    <div class="row" >
        <p>
            <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Tem certeza que deseja deletar esse item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>   

    <?php ActiveForm::end(); ?>

</div>
