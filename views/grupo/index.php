<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Responsavel;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GrupoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grupos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastrar Grupo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
        // GridView::widget([
        //     'dataProvider' => $dataProvider,
        //     'filterModel' => $searchModel,
        //     'columns' => [
        //         ['class' => 'yii\grid\SerialColumn'],

        //         'id',
        //         'grupo_nome',
        //         'responsavel_id',
        //         'grupo_status',

        //         ['class' => 'yii\grid\ActionColumn'],
        //     ],
        // ]); 
    ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
            ],
            [
                'attribute' => 'grupo_nome',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 40%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'responsavel_id',
                'headerOptions' => ['class' => 'text-center'],
                'value'  => function ($data) {
                    return Responsavel::findOne($data->responsavel_id)->nome;
                },
                'contentOptions' => ['style' => 'width: 30%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'grupo_status',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 10%; color:#337ab7;', 'class' => 'text-left'],
            ],

            [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Ações', 
            'headerOptions' => ['width' => '80','style'=>'width: 10%; color:#337ab7;', 'class'=>'text-center'],
            'template' => '{view} {mudastatus} {impressao}',
            'buttons' => [
                    'view' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>', Url::to(['grupo/view','id'=>$key]));
                    },
                    'mudastatus' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-off" title="Ativar/Desativar"></span>', Url::to(['grupo/alterarstatus','id'=>$key]));
                    },
                    'impressao' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-file" title="Imprimir"></span>', Url::to(['#']));
                    },
                ],
            ],
        ],
    ]); ?>
</div>
