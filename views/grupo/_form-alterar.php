<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sexo;
use app\models\Pcd; 

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-form">

    <?php $form = ActiveForm::begin(); ?>

    <fieldset class="fieldset-grupo">
        <legend>Dados Grupo</legend>
        <?= $form->field($model, 'nome_grupo')->textInput(['maxlength' => true]) ?>
    </fieldset>

    <fieldset class="fieldset-grupo">
        <legend>Dados Responsavel</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'nome_responsavel')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'nascimento')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-grupo-nascimento has-success">
                            <label class="control-label" for="grupo-nascimento">Nascimento</label>
                            <input type="text" value="<?php echo $model->nascimento;?>" id="grupo-nascimento" class="form-control nascimento" name="Grupo[nascimento]" maxlength="45" aria-invalid="false">

                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'sexo_id')
                            ->dropDownList(
                                ArrayHelper::map(Sexo::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione um sexo','name'=>'Grupo[sexo_id]']    // options
                            ); ?>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'rg')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-grupo-cpf has-success">
                            <label class="control-label" for="grupo-cpf">Cpf</label>
                            <input type="text" value="<?php echo $model->cpf;?>" id="grupo-cpf" class="form-control cpf" name="Grupo[cpf]" maxlength="14" aria-invalid="false">

                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-grupo-telefone required has-success">
                            <label class="control-label" for="grupo-telefone">Telefone</label>
                            <input type="text" value="<?php echo $model->telefone?>" id="grupo-telefone" class="form-control telefone" name="Grupo[telefone]" maxlength="45" aria-required="true" aria-invalid="true">
                        </div>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'pcd_id')
                            ->dropDownList(
                                ArrayHelper::map(Pcd::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Grupo[pcd_id]']    // options
                            ); ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <?php //echo Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>
    <div class="row" >
        <div class="btn btn-info" style="margin: 0px 0px 0px 46%;" onclick="$('#w0').submit()">
            Salvar
        </div>
    </div>   

    <?php ActiveForm::end(); ?>

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.cep').mask("99999-999");
        $('.telefone').mask("(99)99999-9999");
        $('.nascimento  ').mask("99/99/9999");
        // $('.rg').mask("9999999-9");
        
    })
</script>
