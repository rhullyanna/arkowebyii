<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sexo;
use app\models\Pcd;

$this->title = 'Visualizar Grupo';
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grupo-form">

    <?php $form = ActiveForm::begin(); ?>

    <fieldset>
        <legend><h3>Dados Grupo</h3></legend>

            <div class="col-md-12">
            
                <div class="col-md-6">
                    <?= $form->field($model, 'grupo_nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'grupo_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '','disabled'=>true]) ?>
                </div>

            </div>           

                <div class="form-group">
                    <?php //echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

            <?php //ActiveForm::end(); ?>
    </fieldset>

    <fieldset class="fieldset-grupo">
        <legend><h1>Dados Responsavel</h1></legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <div class="metade">
                        <?= $form->field($responsavel, 'nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($responsavel, 'cargo')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($responsavel, 'nascimento')->textInput(['type'=>'date','disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($responsavel, 'sexo')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="metade">
                        <?= $form->field($responsavel, 'rg')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($responsavel, 'cpf')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($responsavel, 'telefone')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($responsavel, 'email')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <?php //echo Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>
    <div class="row" >
        <p>
            <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Ativar/Desativar', ['alterarstatus', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        </p>
    </div>   

    <?php ActiveForm::end(); ?>

</div>
