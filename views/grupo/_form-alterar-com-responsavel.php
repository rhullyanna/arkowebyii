<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Responsavel;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="grupo-form">

    <fieldset>
        <legend><h1>Cadastrar Grupo</h1></legend>
            <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-12">
            
                <div class="col-md-6">
                    <?= $form->field($model, 'grupo_nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'grupo_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>
                </div>

            </div>           

                <div class="form-group">
                    <?php //echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

            <?php //ActiveForm::end(); ?>
    </fieldset>

    <fieldset>
        <legend><h1>Cadastrar Responsável</h1></legend>
        <div class="col-md-12">
            <?php //$form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($responsavel, 'nome')->textInput(['maxlength' => true]) ?>

                <?= $form->field($responsavel, 'rg')->textInput(['maxlength' => 8]) ?>

                <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>

                <div class="form-group field-usuario-cpf ">
                    <label class="control-label" for="responsavel-cpf">Cpf</label>
                    <input type="text" id="responsavel-cpf" required class="form-control cpf" name="Responsavel[cpf]" maxlength="14" placeholder="000.000.000-00" aria-invalid="false">
                </div>

                <?php echo $form->field($responsavel, 'nascimento')->Input('date') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($responsavel, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '']) ?>

                <?php //echo $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>
                <div class="form-group field-usuario-telefone ">
                    <label class="control-label" for="usuario-telefone">Telefone </label>
                    <input type="text" id="responsavel-telefone" class="form-control telefone" name="Responsavel[telefone]" maxlength="14" placeholder="(00)00000-0000" aria-invalid="false">

                    <div class="help-block"></div>
                </div>

                <?= $form->field($responsavel, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($responsavel, 'cargo')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        
    </fieldset>

</div>


<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem;?>";
    
    function getResponsavelAjax(){
        let retorno = false;
        let urlbase = window.location.origin;
        var request = $.ajax({
        url: urlbase+"/responsavel/consultaresp",
        type: "GET",
        data: {cpf : $("#grupo-responsavel_cpf").val()},
        dataType: ""
        });

        request.done(function(msg) {
            msg = JSON.parse(msg);
            $("#grupo-responsavel_id").val(msg.id);
            ;
        }).then(function(){
            console.log('saff');
            retorno = true;
        });

        request.fail(function(jqXHR, textStatus) {
            if(jqXHR.status){
                sweetAlert('info', 'CPF' ,'Informe um cpf valido');
            }
        });

        return retorno;
    }
    $(document).ready(function($){
        $('#grupo-grupo_status').val('ATIVO');
        $('.cpf').mask("999.999.999-99");    
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
    })
</script>