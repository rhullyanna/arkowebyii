<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enderecamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inventario_id')->textInput() ?>

    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
