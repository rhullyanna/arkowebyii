<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Inventario;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enderecamento-form">


<fieldset>
    <legend><h3>Gerar Impressão <?= Html::a('', ['enderecamento/configimpressora'], ['target'=>'_blank','title'=>'Configurações','class' => 'glyphicon glyphicon-cog']) ?></h3></legend>


    <?php $form = ActiveForm::begin(
        ['options' => ['enctype' => 'multipart/form-data']]
    ); ?>

    <?php
        if($tem_prefixo):
    ?>

    <?= $form->field($model, 'inventario_id')
        ->dropDownList(
            ArrayHelper::map(inventario::find()->all(), 'id', 'numero_inventario'),         // Flat array ('id'=>'label')
            ['prompt'=>'Selecione ','disabled'=>true]    // options
        ); ?>
    
    <?php echo $form->field($model, 'inventario_id')->textInput(['readonly'=>true,'class'=>'hide'])->label(false) ?>
    
    <?php endif; ?>
    
    
    <?= $form->field($model, 'prefixo')->textInput(['required'=>true,'placeholder'=>'Max 4 caracteres','value'=>$prefixo, 'maxlength'=>4,'readonly'=>$tem_prefixo]) ?>
    
    <?= $form->field($model, 'inicio')->textInput(['type'=>'number','min'=>1, 'required'=>true]) ?>

    <?= $form->field($model, 'fim')->textInput(['type'=>'number', 'min'=>1, 'required'=>true]) ?>

    
    <div class="form-group">
        <?= Html::a('Voltar', ['inventario/view', 'id' => $model->inventario_id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::submitButton('Imprimir', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    </fieldset>

</div>



<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>

<script>


var mensagem = "<?php echo $mensagem;?>";
$(document).ready(function(){

    if(mensagem != ""){
        alert(mensagem);
    }
})

</script>
