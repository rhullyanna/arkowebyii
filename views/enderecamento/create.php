<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */

?>
<div class="enderecamento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
        'ultimo_sequencial'=>$ultimo_sequencial,
        'tem_prefixo'=>$tem_prefixo
    ]) ?>

</div>
