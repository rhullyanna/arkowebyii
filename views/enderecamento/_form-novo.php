<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enderecamento-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'prefixo')->textInput(['required'=>true,'readonly'=>$tem_prefixo]) ?>

    <?= $form->field($model, 'inicio')->textInput(['type'=>'number','value'=> $ultimo_sequencial,'min'=>$ultimo_sequencial, 'required'=>true]) ?>
    <?= $form->field($model, 'fim')->textInput(['type'=>'number', 'min'=>$ultimo_sequencial, 'required'=>true]) ?>

    <div class="form-group">
        <?= Html::a('Voltar', ['enderecamento/listarenderecamentos', 'invid' => $model->inventario_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
