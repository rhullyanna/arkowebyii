<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */

$this->title = 'Endereçamentos do inventário numero: '.$inventario->numero_inventario;
// $this->params['breadcrumbs'][] = ['label' => 'Enderecamentos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="enderecamento-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cadastrar Endereçamentos', ['create', 'invid' => $inventario->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Importar Endereçamentos', ['importarenderecamentos', 'invid' => $inventario->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Imprimir Endereçamentos', ['gerarimpressao', 'invid' => $inventario->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Exceções', ['excecao', 'invid' => $inventario->id], ['class' => 'btn btn-success']) ?>
        <?php if($listasequenciais): ?>
            <?= Html::a('Zerar Endereçamentos', ['zerarenderecamentos', 'invid' => $inventario->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Deseja mesmo excluir todos os endereçamentos desse inventário?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </p>

    <div class="agendamento-view">
        <fieldset>
            <legend><h2>Sequência de Endereçamentos</h2></legend>
            <div class="col-md-12">
                <?php $contador = 1;?>
                <?php foreach($listasequenciais as $key=>$value):?>
                    <?php if ($contador > 1):?>
                        <?php if($contador == 20):?>
                            <?php $contador = 1; ?>
                                <div><b> 
                                    <?php 
                                        if($value['descricao_proprietario'] != ""){
                                            echo $value['descricao']. "-" . $value['descricao_proprietario']; ;     
                                        }else{
                                            echo $value['descricao']; 
                                        }
                                    ?> 
                                </b></div><hr>
                            </div>
                        <?php else:?>
                            <div><b> 
                                <?php 
                                    if($value['descricao_proprietario'] != ""){
                                        echo $value['descricao']. "-" . $value['descricao_proprietario']; ;     
                                    }else{
                                        echo $value['descricao']; 
                                    }
                                ?> 
                            </b></div>
                            <?php $contador++; ?>
                        <?php endif;?>
                        
                    <?php else: ?>
                        <div class="col-md-2">
                            <div><b> 
                                <?php 
                                    if($value['descricao_proprietario'] != ""){
                                        echo $value['descricao']. "-" . $value['descricao_proprietario']; ;     
                                    }else{
                                        echo $value['descricao']; 
                                    }
                                ?> 
                            </b></div>
                            <?php $contador++; ?>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
            <hr>
        </fieldset>
        <?= Html::a('Voltar', ['inventario/view', 'id' => $inventario->id], ['class' => 'btn btn-primary']) ?>
    </div>

</div>