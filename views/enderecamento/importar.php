<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Inventario;

/* @var $this yii\web\View */
/* @var $model app\models\Enderecamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enderecamento-form">


<fieldset>
    <legend><h3>Importar Endereçamentos</h3></legend>


    <?php $form = ActiveForm::begin(
        ['options' => ['enctype' => 'multipart/form-data']]
    ); ?>

    <?= $form->field($model, 'inventario_id')
        ->dropDownList(
            ArrayHelper::map(inventario::find()->all(), 'id', 'numero_inventario'),         // Flat array ('id'=>'label')
            ['prompt'=>'Selecione ','disabled'=>true]    // options
        ); ?>
    
    <?php echo $form->field($model, 'inventario_id')->textInput(['required'=>true,'readonly'=>true,'class'=>'hide'])->label(false) ?>
    
    <?= $form->field($model, 'prefixo')->textInput(['required'=>true,'readonly'=>$tem_prefixo]) ?>
    
    <?= $form->field($model, 'nome_coluna_enderecamento')->textInput(['required'=>true,'title'=>'nome da coluna exatamente como no arquivo','placeholder'=>'informe o nome da coluna dos endereçamentos']) ?>    

    <?= $form->field($model, 'arquivo')->fileInput(['class'=>'form-control','required'=>true]) ?>

    
    <div class="form-group">
        <?= Html::a('Voltar', ['inventario/view', 'id' => $model->inventario_id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::submitButton('Importar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    </fieldset>

</div>



<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>

<script>


var mensagem = "<?php echo $mensagem;?>";
$(document).ready(function(){

    if(mensagem != ""){
        alert(mensagem);
    }
})

</script>
