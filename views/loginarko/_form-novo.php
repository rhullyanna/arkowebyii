<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loginarko-form">

<fieldset>
    <legend><h3>Liberar Acesso</h3></legend>
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12">
        
            <div class="row">
                <div class="form-group field-loginarko-cpf">
                    <label class="control-label" for="loginarko-cpf">Cpf</label>
                    <input type="text" id="loginarko-cpf" class="cpf form-control" name="Loginarko[cpf]" maxlength="45" aria-invalid="false"><br>
                    <div class="btn btn-primary" onclick="getUsuarioAjax()">Consultar CPF</div>
                </div>                          
            </div>
            
        <div class="col-md-6">
            <div class="row">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'usuario_id')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'), ['prompt' => '', 'disabled'=>true]) ?>
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'placeholder'=>'Min. 4 dígitos']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'name'=>'', 'id'=>'login-password-validation','placeholder'=>'****']) ?>
            </div>
        </div>
    </div>
</fieldset>

    <?php ActiveForm::end(); ?>

    <div class="form-group">
        <div class="btn btn-success" onclick="salvar()">Salvar</div>
        <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>

function salvar(){
    if(validaPassword() == false){
        if(validaCPF() == true){
            if(validaUsuario() == true){
                $("#w0").submit();
            }
        }
    }
}

function validaCPF(){
    let preenchido = true;
    if($('.cpf').val()==""){
        $('.field-loginarko-cpf').addClass('has-error');
        preenchido = false;
    }else{
        $('.field-loginarko-cpf').removeClass('has-error');
    }
    return preenchido;
}

function validaUsuario(){
    let preenchido = false;
    if($("#loginarko-usuario_id").val()==""){
        sweetAlert('info', 'Consultar CPF' ,'consulte o cpf para preencher os demais campos')
    }else{
        preenchido = true;
    }
    return preenchido;
}

function getUsuarioAjax(usu_cpf){
    loading();
    var request = $.ajax({
    url: "<?php echo Url::to(['loginarko/consultausuario'])?>",
    type: "GET",
    data: {cpf : $("#loginarko-cpf").val()},
    dataType: ""
    });

    request.done(function(msg) {
        msg = JSON.parse(msg);
        let username = msg.nome;
        username = username.split(' ')[0];
        if($("#loginarko-username").val() == ""){
            $("#loginarko-username").val(username);
        }
        $("#loginarko-usuario_id").val(msg.id);
        encerraLoading();
    });

    request.fail(function(jqXHR, textStatus) {
        if(jqXHR.status){
            sweetAlert('info', 'CPF' ,'Informe um cpf valido');
        }
    });
}
    
$(document).ready(function($){
    $('#loginarko-login_status').val('ATIVO');
    $('.cpf').mask("999.999.999-99");    
})
</script>