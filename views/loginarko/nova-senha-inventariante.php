<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Usuario;


/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loginarko-form">

<fieldset>
    <legend><h3>Recuperar Acesso</h3></legend>
    <?php $form = ActiveForm::begin(['action' => 'alterarsenha']); ?>
    
    <div class="col-md-12">
            <div class="metade">
                <div class="form-group field-loginarko-cpf">
                    <label class="control-label" for="loginarko-cpf">Cpf</label>
                    <input type="text" id="loginarko-cpf" class="cpf form-control" name="Loginarko[cpf]" maxlength="45" aria-invalid="false"><br>
                    <div class="btn btn-primary" onclick="getUsuarioAjax()">Consultar CPF</div>
                </div>                          
            </div>
            <div class="metade">
                <?= $form->field($model, 'login_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>
            </div>   
        <div class="col-md-6">
            <div class="row">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'usuario_id')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'), ['prompt' => '', 'disabled'=>true]) ?>
            </div>
            <!-- <div class="row">
                <div class="form-group field-loginarko-cpf">
                    <label class="control-label" for="loginarko-cpf">Cpf</label>
                    <input type="text" id="loginarko-cpf" class="cpf form-control" name="Loginarko[cpf]" maxlength="45" aria-invalid="false">
                    <div class="btn btn-primary" onclick="getUsuarioAjax()">Consultar cpf</div>
                </div>                          
            </div> -->
        </div>
        <div class="col-md-6">
            <div class="row">
                <?php //echo  $form->field($model, 'login_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'placeholder'=>'Min. 4 dígitos']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'name'=>'', 'id'=>'login-password-validation','placeholder'=>'****']) ?>
            </div>
        </div>
    </div>
</fieldset>
</fieldset>

    <?php ActiveForm::end(); ?>

    <div class="form-group">
        <div class="btn btn-success" onclick="gerarSenha()">Gerar Senha</div>
        <?php //echo Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>

function salvar(){
    if(validaPassword() == false){
        if(validaCPF() == true){
            if(validaUsuario() == true){
                $("#w0").submit();
            }
        }
    }
}

function validaCPF(){
    let preenchido = true;
    if($('.cpf').val()==""){
        $('.field-loginarko-cpf').addClass('has-error');
        preenchido = false;
    }else{
        $('.field-loginarko-cpf').removeClass('has-error');
    }
    return preenchido;
}

function validaUsuario(){
    let preenchido = false;
    if($("#loginarko-usuario_id").val()==""){
        sweetAlert('info', 'Consultar CPF' ,'consulte o cpf para preencher os demais campos')
    }else{
        preenchido = true;
    }
    return preenchido;
}

function getUsuarioAjax(usu_cpf){
    loading();
    var request = $.ajax({
    url: "<?php echo Url::to(['loginarko/consultalogin'])?>",
    type: "GET",
    data: {cpf : $("#loginarko-cpf").val()},
    dataType: ""
    });

    request.done(function(msg) {
        msg = JSON.parse(msg);
        
        $("#loginarko-username").val(msg.username);
        $("#loginarko-usuario_id").val(msg.usuario_id);
        $("#loginarko-login_status").val(msg.login_status);
        encerraLoading();
    });

    request.fail(function(jqXHR, textStatus) {
        if(jqXHR.status){
            sweetAlert('info', 'CPF' ,'Informe um cpf valido');
        }
    });
}

function gerarSenha(){
    let senha = "";
    for(var i = 0; i< 4; i++){
        senha = senha+Math.floor((Math.random() * 9) + 1);
    }
    $('#loginarko-password').val(senha);
    $('#login-password-validation').val(senha);
    // sweetAlert('info','Nova senha: '+senha,'Clique em ok para salvar o login com a nova senha');
    swal({
        title: 'Nova senha: '+senha,
        type: 'info',
        html: 'Clique em ok para salvar o login com a nova senha',
        showCloseButton: false,
        showCancelButton: false,
        focusConfirm: true,
        // cancelButtonAriaLabel: 'Voltar',
        confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> Salvar!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        preConfirm: function () {
            salvarRegistro();
        }
    });
}

function salvarRegistro(){
    
    let form = $("#w0");
    let url = form.attr('action');
    var request = $.ajax({
    url: url,
    type: "POST",
    data: form.serialize(),
    dataType: ""
    });

    request.done(function(msg) {
        msg = JSON.parse(msg);
        let tipo = 'success';
        let texto = "";
        let url = "<?php echo Url::to(['loginarko/index'])?>";
        if(msg.STATUS == 400){
            tipo = 'warning';
            texto = 'Verifique se o cpf está correto!';
            url = "#";
        }
        swal({
            title: msg.message,
            type: tipo,
            html: texto,
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: true,
            // cancelButtonAriaLabel: 'Voltar',
            confirmButtonText:
                '<i class="fa fa-thumbs-up"></i> ok!',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            preConfirm: function () {
                window.location.href=url;
            }
        });
    });

    request.fail(function(jqXHR, textStatus) {
        if(jqXHR.status){
            sweetAlert('warning', 'erro' ,jqXHR.message);
        }
    });
}

$(document).ready(function($){
    $('.cpf').mask("999.999.999-99");    
})
</script>