<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loginarko-form">

<fieldset>
    <legend><h3>Alterar Acesso</h3></legend>
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
            <div class="metade">
                <div class="form-group field-loginarko-cpf">
                    <label class="control-label" for="loginarko-cpf">Cpf</label>
                    <input type="text" id="loginarko-cpf" class="cpf form-control" name="Loginarko[cpf]" maxlength="45" aria-invalid="false"><br>
                    <div class="btn btn-primary" onclick="getUsuarioAjax()">Consultar CPF</div>
                </div>                          
            </div>
            <div class="metade">
                <?= $form->field($model, 'login_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>
            </div>   
        <div class="col-md-6">
            <div class="row">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'usuario_id')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'), ['prompt' => '', 'disabled'=>true]) ?>
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'placeholder'=>'Min. 4 dígitos']) ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'name'=>'', 'id'=>'login-password-validation','placeholder'=>'****']) ?>
            </div>
        </div>
    </div>
</fieldset>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>



function getUsuarioAjax(usu_cpf){
    let urlbase = window.location.origin;
    var request = $.ajax({
    url: urlbase+"/loginarko/consultausuario",
    type: "GET",
    data: {cpf : $("#loginarko-cpf").val()},
    dataType: ""
    });

    request.done(function(msg) {
        msg = JSON.parse(msg);
        let username = msg.nome;
        username = username.split(' ')[0];
        if($("#loginarko-username").val() == ""){
            $("#loginarko-username").val(username);
        }
        $("#loginarko-usuario_id").val(msg.id);
    });

    request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus );
    });
}
    
$(document).ready(function($){
    $('.cpf').mask("999.999.999-99");    
})
</script>