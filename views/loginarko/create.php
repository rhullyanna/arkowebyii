<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */

$this->title = 'Liberar Acesso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loginarko-create">

    <?= $this->render('_form-novo', [
        'model' => $model,
    ]) ?>

</div>
