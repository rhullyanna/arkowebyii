<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoginarkoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Acessos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loginarko-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Liberar Acesso', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Recuperar Senha', ['alterarsenha'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
            ],
            [
                'attribute' => 'username',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 40%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'cpf',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 30%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'login_status',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 10%; color:#337ab7;', 'class' => 'text-left'],
            ],

            [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Ações', 
            'headerOptions' => ['width' => '80','style'=>'width: 10%; color:#337ab7;', 'class'=>'text-center'],
            'template' => '{view} {update} {mudastatus} {impressaoficha} {produtividade}',
            'buttons' => [
                    'view' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>', Url::to(['loginarko/view','id'=>$key]));
                    },
                    'update' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-pencil" title="Alterar"></span>', Url::to(['loginarko/update','id'=>$key]));
                    },
                    'mudastatus' => function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-off" title="Ativar/Desativar"></span>', Url::to(['loginarko/alterarstatus','id'=>$key]));
                    },
                ],
            ],
        ],
        // 'columns' => [
        //     ['class' => 'yii\grid\SerialColumn'],

        //     'id',
        //     'username',
        //     'cpf',
        //     // 'password',
        //     // 'authKey:ntext',
        //     //'accessToken:ntext',
        //     //'usuario_id',
        //     'login_status',
        //     ['class' => 'yii\grid\ActionColumn'],
        // ],
    ]); ?>
</div>
