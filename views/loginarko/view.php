<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */

$this->title = 'Liberar Acesso';
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="loginarko-view">

    <h3><?php //echo Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
        <?= Html::a('Ativar/Desativar', ['alterarstatus', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Deseja mesmo ativar/desativar este perfil?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $form = ActiveForm::begin(); ?>
    <fieldset>
        <legend>Visualizar Login</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'usuario_id')->dropDownList(ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'), ['prompt' => '', 'disabled'=>true]) ?>
                </div>
                           
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group field-loginarko-cpf">
                        <label class="control-label" for="loginarko-cpf">Cpf</label>
                        <input type="text" value="<?php echo $model->cpf; ?>" id="loginarko-cpf" class="cpf form-control" name="Loginarko[cpf]" maxlength="45" aria-invalid="false" disabled="disabled">
                        <!-- <div class="btn btn-primary" onclick="getUsuarioAjax()">Consultar cpf</div> -->
                    </div>                          
                </div>
                <div class="row">
                    <?= $form->field($model, 'login_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '','disabled'=>true]) ?>
                </div> 
            </div>
        </div>
    </fieldset>
    <?php ActiveForm::end(); ?>
    <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
</div>
