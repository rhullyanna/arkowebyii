<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loginarko */

$this->title = 'Alterar Senha';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loginarko-update">

    <h1><?php //echo Html::encode($this->title) ?></h1>

    <?= $this->render('_form-alterar', [
        'model' => $model,
    ]) ?>

</div>
