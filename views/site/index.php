<?php
use yii\helpers\Html;
use app\models\User;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Usuario;
use miloschuman\highcharts\Highcharts;
use dosamigos\chartjs\ChartJs;

/* @var $this yii\web\View */

$this->title = 'Arko Soluctions';
?>

<style type="text/css">

    .box #map {
        height: 450px;   
    }    

    .box.box-info {border-top-color: #00c0ef;}
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        /* width: 40%; */
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);}
    .box-header.with-border {border-bottom: 1px solid #f4f4f4;}
    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;}
    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 10px;}
    .table-responsive {
        min-height: .01%;
        overflow-x: auto;}
    .table>thead>tr>th {
        padding: 8px;
        line-height: 1.42857143;
        border-bottom: 2px solid #f4f4f4;}
    .no-margin {margin: 0 !important;}
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;}
    thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;} 

    .btn_notificacao_email div{
        margin-left: 10%;
    }

</style>

<div class="site-index">
    <div class="col-md-6">
        <?php if (Yii::$app->user->isGuest) { ?>
            <div class="jumbotron">
                <h1><?php echo Html::img("@web/images/logo-login.png",['class'=>'img-index','height'=>"auto", 'width'=>"100%"]); ?></h1>

            </div>
        <?php } else if(!User::is_inventariante(Usuario::findOne(Yii::$app->user->identity))){ ?>
            <div class="body-content">
                <div class="row">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3>CONTRATOS À VENCER 
                                <span class="btn_notificacao_email"> 
                                    <div class="btn btn-info" onclick="enviarEmails();">Notificar Via E-mail</div> 
                                </span> 
                            </h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Número</th>
                                        <th>Vencimento</th>
                                        <th>Nome</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($contratos_a_vencer as $contrato):?>
                                        <tr>
                                            <td><?php echo $contrato['numero']; ?></td>
                                            <td><?= $contrato['vencimento'];?></td>
                                            <td><?=$contrato['loja']?> </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3>Próximos Inventários do Mês</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Loja</th>
                                        <th>Data</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($proximos_inventarios as $prox_inv):?>
                                        <tr>
                                            <td><?php echo $prox_inv['loja']; ?></td>
                                            <td><?= $prox_inv['data'];?></td>
                                            <td><?=$prox_inv['status']?> </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3>Localização das lojas</h3>  
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
        <?php } else { ?>
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h1>Usuário sem permissões <span class="glyphicon glyphicon-exclamation-sign"></span></h1>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script>

    function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: -3.1115577, lng: -60.0454601}
    });

    setMarkers(map);
    }

    var beaches = <?php echo $lojas; ?>;    

    function setMarkers(map) {
  
        var image = {
            url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        for (var i = 0; i < beaches.length; i++) {
            var beach = beaches[i];
            var marker = new google.maps.Marker({
            position: {lat: parseFloat(beach[1]), lng: parseFloat(beach[2])},
            map: map,
            icon: image,
            shape: shape,
            title: beach[0],
            zIndex: beach[3]
            });
        }
    }

    $(document).ready(function($){
        initMap();
    });
</script>