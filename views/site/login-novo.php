<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

// $this->title = 'Login';
// $this->params['breadcrumbs'][] = $this->title;
?>

<!-- <link rel="stylesheet" type="text/css" href="../css/site.css"> -->

<style type="text/css">
    

</style>

<div class="card">
    <div class="jumbotron">
        <div class="site-login">

            <?php echo Html::img("@web/images/logo-login.png",['class'=>'img-login','height'=>"auto", 'width'=>"100%"]); ?>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                ],
            ]); ?>

                <?= $form->field($model, 'cpf')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <div class=" col-lg-11">
                        <?= Html::submitButton('Login', ['class' => 'btn_login text-center', 'name' => 'login-button']) ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            <div class="col-lg-offset-1" style="color:#999;">
                admin/12345
            </div>
        </div>
    </div>
</div>
<!-- Page body end -->

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    $(document).ready(function($){
        $('#loginform-cpf').mask("999.999.999-99");
        
    })
</script>

