<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Usuario;
use app\models\Inventario;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarioenderecamento */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .inventariante-plus{
        margin: 0px 0px 17px 0px;
        /* position: absolute; */
        width: 51px;
    }
</style>

<div id="cadastrar-equipe" class="usuarioenderecamento-form" >

    <h3><b>Inventário</b></h3>
    <hr>

    <?php $form = ActiveForm::begin(); ?>
    <div class=" row">
        <div class=" col-md-6">
            <?= $form->field($model, 'inventario_id')
                ->dropDownList(
                    ArrayHelper::map($inventarios, 'id', 'numero_inventario'),         // Flat array ('id'=>'label')
                    ['prompt'=>'Selecione ','disabled'=>true]    // options
                )->label('<h4><b>Número</b></h4>'); 
            ?>
        </div>
    </div>
    <div class="row col-md-12">
        <h3><b>Equipe</b></h3>
        <hr>
        <div class="col-md-12">
            <h4><b>Inventariante</b></h4>
        </div>
    </div>

    

    <?php if($usuarios_inventario):?>
            <?php foreach($usuarios_inventario as $key => $usu_inv):?>

                <div class="row col-md-12">
                    <div class="col-md-6 ">
                        <?= $form->field($usu_inv, 'id')
                            ->dropDownList(
                                ArrayHelper::map(Usuario::find()->where(['cargo'=>Usuario::INVENTARIANTE])->orderBy(['nome'=>SORT_ASC])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','disabled'=>true]    // options
                            )->label(False); ?>
                    </div>

                    <div class="col-md-3">
                        <?= Html::a('<b>-</b>', ['delete', 'i' => $model->inventario_id, 'u'=>$usu_inv->id], [
                            'class' => 'btn btn-danger inventariante-plus',
                            'data' => [
                                'confirm' => 'Deseja mesmo excluir esta atribuição de inventariante?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php endforeach;?>
    <?php endif;?>

    
    
    <div class="row col-md-12">
        <div class="col-md-6">
            <?= $form->field($model, 'usuario_id')
                ->dropDownList(
                    ArrayHelper::map($inventariantes, 'id', 'nome'),         // Flat array ('id'=>'label')
                    ['focus'=>true,'prompt'=>'Selecione ','id'=>'usuario-1','name'=>'Usuarioinventario[inventariantes][0][usuario_id]']    // options
                )->label(false); ?>
        </div>
    

        <div class="col-md-3">
            <!-- <div class="btn btn-success inventariante-plus">+</div> -->
            <?php echo Html::submitButton('<b>+</b>', ['class' => 'btn btn-success inventariante-plus']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php echo Html::a('<button type="button" class="btn btn-info" >Voltar</button>', Url::to(['inventario/view','id'=>$model->inventario_id])); ?>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>

<script>

    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
    })
</script>