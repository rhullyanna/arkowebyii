<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioinventarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarioinventarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarioinventario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Usuarioinventario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'usuario_id',
            'inventario_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
