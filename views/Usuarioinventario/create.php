<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarioinventario */

$this->title = 'Equipe';
$this->params['breadcrumbs'][] = ['label' => 'Inventario', 'url' => ['inventario/view','id'=>$model->inventario_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarioinventario-create">

    <h1><?php //echo Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
        'usuarios_inventario' => $usuarios_inventario,                
        'inventarios' => $inventarios,                
        'inventariantes' => $inventariantes,
        'mensagem' => $mensagem
    ]) ?>

</div>
