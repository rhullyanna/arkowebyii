<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Usuario;
use app\models\Loginarko;
use app\models\Inventario;
/* @var $this yii\web\View */
/* @var $model app\models\Agendamento */

// $this->params['breadcrumbs'][] = ['label' => 'Contrato', 'url' => ['loja/view','id'=>$contrato->loja_id]];
// $this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="agendamento-view">
    <fieldset>
        <div class="col-md-12">
            <div class="col-md-3">
                <b>Data e Hora</b>
            </div>
            <div class="col-md-3">
                <b>Coordenador</b>
            </div>
            <div class="col-md-3">
                <b>Status</b>
            </div>
            <div class="col-md-3">
                <b>Ações</b>
            </div>
        </div>
        <hr>
        <?php if($agendamentos) :?>
            <?php foreach($agendamentos as $agendamento): ?>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?php echo $agendamento->data_agendamento . ' - ' . $agendamento->hora_agendamento;?>
                    </div>
                    <div class="col-md-3">
                        <?php echo explode(' ',Usuario::findOne($agendamento->usuario_id)->nome)[0];?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $agendamento->agendamento_status ;?>
                    </div>
                    <div class="col-md-3">
                        <?php $inventario = Inventario::find()->where(['agendamento_id'=>$agendamento->id])->one();?>
                        <?php if($inventario):?>
                    <!-- se tiver inventario cadastrado btn com url para visualizar o inventario -->
                            <?php echo Html::a('<button type="button" class="btn btn-success">Inventario</button>', Url::to(['inventario/view','id'=>$inventario->id])); ?>
                        <?php else:?>
                            <?php echo Html::a('<button type="button" class="btn btn-info">Inventario</button>', Url::to(['inventario/create','agid'=>$agendamento->id])); ?>
                        <?php endif;?>

                    <!-- se não tiver inventario cadastrado btn com url para cadastro de inventario -->
                        
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif;?>
    </fieldset>
</div>



