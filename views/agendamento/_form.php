<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agendamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agendamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contrato_id')->textInput() ?>

    <?= $form->field($model, 'data_agendamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_agendamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <?= $form->field($model, 'agendamento_status')->dropDownList([ 'AGUARDANDO' => 'AGUARDANDO', 'EM ANDAMENTO' => 'EM ANDAMENTO', 'CONCLUIDO' => 'CONCLUIDO', 'ATRASADO' => 'ATRASADO', 'CANCELADO' => 'CANCELADO', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
