
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Agendamento */
/* @var $form yii\widgets\ActiveForm */
?>


<fieldset>
    <legend>Contrato</Legend>
    <div class="tabela-contratos col-md-12">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">Número</th>
                <th scope="col">Vigência</th>
                <th scope="col">Status</th>
                <th scope="col">Contrato</th>
                </tr>
            </thead>
            <tbody>
                <?php if($contrato):?>
                    <tr>
                        <td> <?php echo $contrato->contrato_numero; ?> </td>
                        <td> <?php echo $contrato->data_adesao. " à " . $contrato->data_vencimento;?> </td>
                        <td> <?php echo $contrato->contrato_status; ?> </td>
                        <td> 
                            <a target="_blank" href="<?php echo "../../".$contrato->contrato_arquivo;?>">
                            <!-- <a target="_blank" href="<?php //echo Yii::$app->basePath;?>"> -->
                                Contrato
                            </a>
                        </td>
                    </tr>

                <?php endif;?>
            </tbody>
        </table>
    </div>
</fieldset>


<fieldset>
        <legend>Agendamentos</Legend>
        <div class="agendamento-form">

        <div class="col-md-12">


            <?php foreach($agendamentos as $agendamento): ?>

                <div class="col-md-4">
                    <?php $form = ActiveForm::begin(); ?>
                        <div class="hide">
                            <?= $form->field($agendamento, 'id')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                        </div>
                        <?= $form->field($agendamento, 'data_agendamento')->input('date') ?>

                        <?= $form->field($agendamento, 'hora_agendamento')->textInput(['maxlength' => true, "class"=>"form-control hora"]) ?> 

                        <?= $form->field($agendamento, 'usuario_id')
                            ->dropDownList(
                                ArrayHelper::map(Usuario::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Agendamento[usuario_id]']    // options
                            ); ?>

                        <?= $form->field($agendamento, 'agendamento_status')->dropDownList([ 'PRE AGENDADO'=>'PRE AGENDADO','AGUARDANDO' => 'AGUARDANDO', 'EM ANDAMENTO' => 'EM ANDAMENTO', 'CONCLUIDO' => 'CONCLUIDO', 'ATRASADO' => 'ATRASADO', 'CANCELADO' => 'CANCELADO', ], ['prompt' => '']) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Salvar', ['onclick'=>'loading()','class' => 'btn btn-success']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $agendamento->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Deseja mesmo excluir este agendamento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        </div>
                        

                    <?php ActiveForm::end(); ?>
                </div>
            
            <?php endforeach; ?>

        </div>
        <?php echo Html::a('<div class="btn btn-info">Voltar</div>', Url::to(['loja/view','id'=>$contrato->loja_id])); ?>
    </div>
</fieldset>



<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        $('.hora  ').mask("99:99");
        if(mensagem != ""){
            if(mensagem == "sucesso"){
                sweetAlert('success','Salvo','Agendamento salvo com sucesso!');
            }else{
                sweetAlert('info','Erro',mensagem);
            }
        }
        
    })
</script>