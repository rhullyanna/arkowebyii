<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Contrato;


/* @var $this yii\web\View */
/* @var $model app\models\Loja */
/* @var $form yii\widgets\ActiveForm */

?>

<div>
    
    <fieldset>
        <Legend><h2>Contratos loja: <?php echo $loja->nome_fantasia;?></h2></legend>

        <div class="tabela-contratos col-md-12">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Número</th>
                    <th scope="col">Vigência</th>
                    <th scope="col">Status</th>
                    <th scope="col">Contrato</th>
                    <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($contratos as $contrato):?>
                            <tr>
                                <td> <?php echo $contrato->contrato_numero; ?> </td>
                                <td> <?php echo $contrato->data_adesao. " à " . $contrato->data_vencimento;?> </td>
                                <td> <?php echo $contrato->contrato_status; ?> </td>
                                <td> 
                                    <a target="_blank" href="<?php echo "../../".$contrato->contrato_arquivo;?>">
                                    <!-- <a target="_blank" href="<?php //echo Yii::$app->basePath;?>"> -->
                                        Contrato
                                    </a>
                                </td>
                                <td> 
                                    <a href="<?php echo Url::to(['contrato/view','id'=>$contrato->id]);?>"><div class="btn btn-info">alterar</div></a> 
                                </td>
                            </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        
    </fieldset>

</div>


<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php //echo $mensagem; ?>"
    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.cep').mask("99999-999");
        $('.telefone').mask("(99)99999-9999");
        $('.data  ').mask("99/99/9999");
        $('.cnpj').mask('99.999.999/9999-99', {reverse: false});
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
        
    })
</script>