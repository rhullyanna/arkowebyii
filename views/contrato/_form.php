<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contrato */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contrato-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contrato_numero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data_adesao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data_vencimento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qtd_inventario')->textInput() ?>

    <?= $form->field($model, 'contrato_arquivo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'loja_id')->textInput() ?>

    <?= $form->field($model, 'contrato_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
