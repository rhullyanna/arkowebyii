<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contrato */

// $this->title = 'Update Contrato: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="contrato-update">

    <h1><?php //echo Html::encode($this->title) ?></h1>

    <?= $this->render('alterar-novo', [
        'contrato' => $model,
        'mensagem'=>$mensagem
    ]) ?>

</div>
