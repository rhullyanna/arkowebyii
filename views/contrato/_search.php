<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContratoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contrato-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'contrato_numero') ?>

    <?= $form->field($model, 'data_adesao') ?>

    <?= $form->field($model, 'data_vencimento') ?>

    <?= $form->field($model, 'qtd_inventario') ?>

    <?php // echo $form->field($model, 'contrato_arquivo') ?>

    <?php // echo $form->field($model, 'loja_id') ?>

    <?php // echo $form->field($model, 'contrato_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
