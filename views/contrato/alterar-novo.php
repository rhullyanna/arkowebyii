
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Contrato;

?>

<style>
    .btn-contrato{
        margin: 4% 0 0 9%;
        position: absolute;
    }
    
</style>

<fieldset>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <Legend><h2>Alterar Contrato: <?php echo $contrato->contrato_numero;?></h2></legend>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <?php echo $form->field($contrato, 'contrato_numero')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="row">
                        <div class="metade">
                            <?= $form->field($contrato, 'contrato_arquivo')->fileInput(['class'=>'form-control']) ?>
                        </div>
                        <a target="_blank" href="<?php echo "../../".$contrato->contrato_arquivo;?>">
                            <div class="btn btn-info btn-contrato">Contrato atual</div>
                        </a>
                        
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <?php echo $form->field($contrato, 'qtd_inventario')->textInput(['type'=>'number','min'=>0]) ?>
                    </div>                        

                    <div class="row">

                        <div class="metade">
                            <?= $form->field($contrato, 'data_adesao')->input('date') ?>
                            <?php //echo $form->field($contrato, 'data_adesao')->textInput(['maxlength' => true,'class'=>'form-control data']) ?>
                        </div>
                        <div class="metade">
                            <?= $form->field($contrato, 'data_vencimento')->input('date') ?>
                            <?php //echo $form->field($contrato, 'data_vencimento')->textInput(['maxlength' => true,'class'=>'form-control data']) ?>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="row" >
                <?php echo Html::a('<button type="button" class="btn btn-info" >Voltar</button>', Url::to(['loja/view','id'=>$contrato->loja_id])); ?>
                <div class="btn btn-success"  onclick="$('#w0').submit()">
                    Salvar
                </div>


            </div> 
        
        <?php ActiveForm::end(); ?>
        
    </fieldset>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        if(mensagem != ""){
            if(mensagem == "sucesso"){
                sweetAlert('success','Salvo','Contrato salvo com sucesso!');
            }else{
                sweetAlert('info','Erro',mensagem);
            }
        }
        
    })
</script>    