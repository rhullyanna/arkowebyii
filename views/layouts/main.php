<?php



/* @var $this \yii\web\View */

/* @var $content string */



use app\widgets\Alert;

use yii\helpers\Html;

use yii\bootstrap\Nav;

use yii\bootstrap\NavBar;

use yii\widgets\Breadcrumbs;

use app\assets\AppAsset;

use app\models\Usuario;

use app\models\User;



AppAsset::register($this);



?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>

    <meta charset="<?= Yii::$app->charset ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script async defer

    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfrATuTFCBcf1sbKD-QY0x4UHSS_lB20A&callback=initMap">

    </script>

    <?php $this->registerCsrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

</head>

<body>

<?php $this->beginBody() ?>



<div class="wrap">

    



<?php

    NavBar::begin([

        'brandLabel' => Html::img('@web/images/logo-login-copia.png', ['alt'=>"ArkoWeb", "class"=>"img-header",]),

        'brandUrl' => Yii::$app->homeUrl,

        'options' => [

            'class' => 'navbar-inverse navbar-fixed-top',

        ],

    ]);



    $itensMenu = array(

        

        );



    if (Yii::$app->user->isGuest) {

//        $itensMenu[] = ['label' => 'Sobre', 'url' => ['/site/about']];

        $itensMenu[] = ['label' => 'Aplicativo', 'url' => ['/site/aplicativo']];

        $itensMenu[] = ['label' => 'Login', 'url' => ['/site/login']];

    }else{



        // if(Usuario::findOne(User::is_gestor(Yii::$app->user->identity->usuario_id)->cargo)){

        // if(User::is_gestor(Usuario::findOne(Yii::$app->user->identity))){

            if(!User::is_inventariante(Usuario::findOne(Yii::$app->user->identity))){

            $itensMenu[] = [

                'label' => 'Usúario', 

                'items' => [

                    ['label' => 'Gerenciar', 

                    'url' => ['/usuario/index']],

                    '<li class="divider"></li>',

                    ['label' => 'Conceder acesso', 

                    'url' => ['/loginarko/index']],

                    '<li class="divider"></li>',

                ]

            ];

            $itensMenu[] = [

                'label' => 'Loja', 

                'items' => [

                    ['label' => 'gerenciar',

                    'url' => ['/loja/index']],

                    '<li class="divider"></li>',

                ]

            ];

            $itensMenu[] = ['label' => 'Impressão', 'url' => ['/enderecamento/gerarimpressao']];

            $itensMenu[] = [

                'label' => 'Cadastros adicionais', 

                'items' => [

                    ['label' => 'Grupo',

                    'url' => ['/grupo/index']],

                    '<li class="divider"></li>',

                    ['label' => 'Regime',

                    'url' => ['/regime/index']],

                    '<li class="divider"></li>',

                ]

            ];

            $itensMenu[] = ['label' => 'Inventarios', 'url' => ['/inventario/index']];

            // $itensMenu[] = ['label' => 'Equipe', 'url' => ['/usuarioenderecamento/index']];

            // $itensMenu[] = ['label' => 'Regime', 'url' => ['/regime/index']];

            // $itensMenu[] = ['label' => 'Cargo', 'url' => ['/cargo/index']];

            $itensMenu[] = ['label' => 'Log-sistema', 'url' => ['/loghistorico/index']];

        }else if(User::is_inventariante(Usuario::findOne(Yii::$app->user->identity))){

            // $itensMenu[] = ['label' => 'Regime', 'url' => ['/regime/index']];

            // $itensMenu[] = ['label' => 'Cargo', 'url' => ['/cargo/index']];

        }

        $itensMenu[] = '<li>'

                . Html::beginForm(['/site/logout'], 'post')

                . Html::submitButton(

                    'Logout (' . Yii::$app->user->identity->username . ')',

                    ['class' => 'btn btn-link logout']

                )

                . Html::endForm()

                . '</li>';

    }



    echo Nav::widget([

        'options' => ['class' => 'navbar-nav navbar-right'],

        'items' => $itensMenu,

    ]);

    NavBar::end();

    ?>



    <div class="container">

        <?= Breadcrumbs::widget([

            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],

        ]) ?>

        <?= Alert::widget() ?>

        <?= $content ?>

    </div>

</div>



<footer class="footer">

    <div class="container">

        <p class="pull-left">&copy; Arko Solutions <?= date('Y') ?></p>



        <p class="pull-right"><?= "Powered by RApp-Enterprise" ?></p>

    </div>

</footer>



<?php $this->endBody() ?>

</body>

</html>

<?php $this->endPage() ?>



