<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InventarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'agendamento_id') ?>

    <?= $form->field($model, 'usuario_resp_cadastro_inventario_id') ?>

    <?= $form->field($model, 'usuario_coordenador_id') ?>

    <?= $form->field($model, 'previsao_duracao') ?>

    <?php // echo $form->field($model, 'tipo_inventario') ?>

    <?php // echo $form->field($model, 'validade') ?>

    <?php // echo $form->field($model, 'fabricacao') ?>

    <?php // echo $form->field($model, 'lote') ?>

    <?php // echo $form->field($model, 'itens_embalagem') ?>

    <?php // echo $form->field($model, 'marca') ?>

    <?php // echo $form->field($model, 'fornecedor') ?>

    <?php // echo $form->field($model, 'ignorar_zero_esq') ?>

    <?php // echo $form->field($model, 'ignorar_zero_direita') ?>

    <?php // echo $form->field($model, 'data_inicio') ?>

    <?php // echo $form->field($model, 'hora_inicio') ?>

    <?php // echo $form->field($model, 'data_fim') ?>

    <?php // echo $form->field($model, 'hora_fim') ?>

    <?php // echo $form->field($model, 'inventario_status') ?>

    <?php // echo $form->field($model, 'numero_inventario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
