<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Agendamento;
use app\models\Inventario;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InventarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Inventario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'agendamento_id',
            // 'usuario_resp_cadastro_inventario_id',
            // 'usuario_coordenador_id',
            
            
            [
                'attribute' => 'loja',
                'label'=>'Loja',
                'headerOptions' => ['class' => 'text-center'],
                'value'  => function ($data) {
                    $loja = Inventario::getLoja($data->agendamento_id)->nome_fantasia;
                    return $loja;
                },
                'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'agendamento_id',
                'label'=>'Data agend.',
                'value'=>'agendamento.data_agendamento',
                'headerOptions' => ['class' => 'text-center'],
            ],
            [   
                'attribute'=>'usuario_coordenador_id',
                'label' => 'Coordenador',
                'value'=>'usuarioCoordenador.nome',
                'headerOptions' => ['class' => 'text-center'],
            ],
            'previsao_duracao',
            //'tipo_inventario',
            //'validade',
            //'fabricacao',
            //'lote',
            //'itens_embalagem',
            //'marca',
            //'fornecedor',
            //'ignorar_zero_esq',
            //'ignorar_zero_direita',
            //'data_inicio',
            //'hora_inicio',
            //'data_fim',
            //'hora_fim',
            'inventario_status',
            //'numero_inventario',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
