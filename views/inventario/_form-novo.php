<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Inventario */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    #inventario-filtros label{
        margin: 4px 30px 5px 0px;
    }
</style>

<fieldset>
    <?php $form = ActiveForm::begin(); ?>
    <legend><h3>Dados da Loja</h3></legend>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($loja, 'nome_fantasia')->textInput(['disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'cnpj')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'endereco')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($loja, 'endereco_numero')->textInput(['disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'complemento')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'bairro_id')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'cep')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend><h3>Inventário</h3></legend>
    <!-- <div class="col-md-"></div> -->
    
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($agendamento, 'data_agendamento')->Input('date') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'numero_inventario')->textInput(['maxlength' => true,'readonly'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'previsao_duracao')->textInput(['maxlength' => true,'class'=>'form-control hora']) ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'tipo_inventario')->dropDownList([ 'VARREDURA' => 'VARREDURA', 'SEMI VARREDURA' => 'SEMI VARREDURA' ], ['prompt' => 'selecione','onchange'=>'verificaTipoInventario()']) ?>
            </div>
            <div class="col-md-4">
                <?php //echo $form->field($model, 'usuario_coordenador_id')->textInput() ?>
                <?= $form->field($model, 'usuario_coordenador_id')
                    ->dropDownList(
                        ArrayHelper::map(Usuario::find()->where(['cargo'=>Usuario::COORDENADOR])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                        ['prompt'=>'Selecione ','name'=>'Inventario[usuario_coordenador_id]']    // options
                ); ?>
            </div>
        </div>
        <fieldset class="filtro-coleta">
            <legend>Filtros de coleta</legend>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->field($model, 'filtros[]')->checkboxList([
                            'validade' => 'Validade', 
                            'fabricacao' => 'Fabricação', 
                            'lote' => 'Lote',
                            'itens_embalagem' => 'Itens Embalagem',
                            'descricao' => 'Descrição',
                            'fornecedor'=>'Fornecedor',
                            'validade_por_meses'=>'Validade em meses'
                        ])->label(false);
                    ?>
                </div>
            </div>
        </fieldset>
    </div>


</fieldset>


    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        $('.hora').mask("99:99");
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
        
    })
</script>