<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use app\models\Enderecamento;

/* @var $this yii\web\View */
/* @var $model app\models\Inventario */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    #inventario-filtros label{
        margin: 4px 30px 5px 0px;
    }
</style>

<h1><?= Html::encode("Inventario número: ".$model->numero_inventario) ?></h1>
<fieldset>
    <?php $form = ActiveForm::begin(); ?>
    <legend><h3>Dados da Loja</h3></legend>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($loja, 'nome_fantasia')->textInput(['disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'cnpj')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'endereco')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($loja, 'endereco_numero')->textInput(['disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'complemento')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'bairro_id')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($loja, 'cep')->textInput(['maxlength' => true,'disabled'=>true]) ?>
            </div>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend><h3>Inventário</h3></legend>
    <!-- <div class="col-md-"></div> -->
    
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($agendamento, 'data_agendamento')->Input('date',['disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'numero_inventario')->textInput(['maxlength' => true,'readonly'=>true,'disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'previsao_duracao')->textInput(['maxlength' => true,'class'=>'form-control hora','disabled'=>true]) ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'tipo_inventario')->dropDownList([ 'VARREDURA' => 'VARREDURA', 'SEMI VARREDURA' => 'SEMI VARREDURA' ], ['prompt' => 'selecione','disabled'=>true]) ?>
            </div>
            <div class="col-md-4">
                <?php //echo $form->field($model, 'usuario_coordenador_id')->textInput() ?>
                <?= $form->field($model, 'usuario_coordenador_id')
                    ->dropDownList(
                        ArrayHelper::map(Usuario::find()->where(['cargo'=>Usuario::COORDENADOR])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                        ['prompt'=>'Selecione ','name'=>'Inventario[usuario_coordenador_id]','disabled'=>true]    // options
                ); ?>
            </div>
        </div>
        <fieldset class="filtro-coleta">
            <legend>Filtros de coleta</legend>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->field($model, 'filtros')->checkboxList([
                            'validade' => 'Validade', 
                            'fabricacao' => 'Fabricação', 
                            'lote' => 'Lote',
                            'itens_embalagem' => 'Itens Embalagem',
                            'descricao' => 'Descrição',
                            'validade_por_meses'=>'Validade em meses',
                            'fornecedor'=>'Fornecedor'
                        ])->label(false);
                    ?>
                </div>
            </div>
        </fieldset>
    </div>

</fieldset>

<?php ActiveForm::end(); ?>

<?php echo Html::a('<button type="button" class="btn btn-info" >Voltar</button>', Url::to(['loja/view','id'=>$loja->id])); ?>
<?php echo Html::a('<button type="button" class="btn btn-warning" >Alterar</button>', Url::to(['inventario/update','id'=>$model->id])); ?>
<?php echo Html::a('<button type="button" class="btn btn-success" >Endereçamentos</button>', Url::to(['enderecamento/listarenderecamentos','invid'=>$model->id])); ?>
<?php if(Enderecamento::find()->where(['inventario_id'=>$model->id])->one()){
        echo Html::a('<button type="button" class="btn btn-danger" >Equipe</button>', Url::to(['usuarioinventario/create','invid'=>$model->id]));
    }   
    
?>



<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>"
    
    $(document).ready(function($){
        verificaTipoInventario();
        $('.hora').mask("99:99");
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
        
    })
</script>