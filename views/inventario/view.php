<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inventario */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inventarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="inventario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'agendamento_id',
            'usuario_resp_cadastro_inventario_id',
            'usuario_coordenador_id',
            'previsao_duracao',
            'tipo_inventario',
            'validade',
            'fabricacao',
            'lote',
            'itens_embalagem',
            'marca',
            'fornecedor',
            'ignorar_zero_esq',
            'ignorar_zero_direita',
            'data_inicio',
            'hora_inicio',
            'data_fim',
            'hora_fim',
            'inventario_status',
            'numero_inventario',
        ],
    ]) ?>

</div>
