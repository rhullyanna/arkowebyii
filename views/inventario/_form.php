<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Inventario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'agendamento_id')->textInput() ?>

    <?= $form->field($model, 'usuario_resp_cadastro_inventario_id')->textInput() ?>

    <?= $form->field($model, 'usuario_coordenador_id')->textInput() ?>

    <?= $form->field($model, 'previsao_duracao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_inventario')->dropDownList([ 'VARREDURA' => 'VARREDURA', 'SEMI VARREDURA' => 'SEMI VARREDURA', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'validade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fabricacao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'itens_embalagem')->dropDownList([ 'SIM' => 'SIM', 'NAO' => 'NAO', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fornecedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ignorar_zero_esq')->dropDownList([ 'SIM' => 'SIM', 'NAO' => 'NAO', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ignorar_zero_direita')->dropDownList([ 'SIM' => 'SIM', 'NAO' => 'NAO', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'data_inicio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_inicio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'data_fim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_fim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inventario_status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', 'EM ANDAMENTO' => 'EM ANDAMENTO', 'EM DIVERGENCIA' => 'EM DIVERGENCIA', 'CONCLUIDO' => 'CONCLUIDO', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'numero_inventario')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
