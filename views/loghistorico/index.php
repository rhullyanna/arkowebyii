<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoghistoricoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loghistoricos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loghistorico-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Loghistorico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'usuario_id',
            'acao_realizada',
            'modulo',
            'data',
            //'observacao:ntext',

            ['class' => 'yii\grid\ActionColumn',
            'header'=>'<b style="color: #337ab7;">Ações<b>',
            'template'=>'{view}'],
        ],
    ]); ?>
</div>
