<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loghistorico */

$this->title = 'Create Loghistorico';
$this->params['breadcrumbs'][] = ['label' => 'Loghistoricos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loghistorico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
