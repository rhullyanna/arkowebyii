<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Regime */

$this->title = "Visualizar Regime";
$this->params['breadcrumbs'][] = ['label' => 'Regimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="regime-view">

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Deseja mesmo excluir este regime?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <fieldset>
        <legend>Visualizar Regime: <?php echo $model->descricao;?></legend>
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'descricao')->textInput(['maxlength' => true,'disabled'=>true]) ?>

            <?= $form->field($model, 'observacao')->textarea(['rows' => 6,'disabled'=>true]) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </fieldset>
    
    <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>

</div>
