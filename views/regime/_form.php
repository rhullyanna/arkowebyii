<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Regime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="regime-form">

    <fieldset>
        <!-- <legend>Visualizar Regime: <?php //echo $model->descricao;?></legend> -->
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'observacao')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
                <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </fieldset>

</div>
