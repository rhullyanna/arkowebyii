<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Regime */

$this->title = 'Alterar Regime: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Regimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="regime-update">

    <h3><?= Html::encode($this->title) ?></h3>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
