<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loja */

$this->title = 'Cadastro de Loja';
$this->params['breadcrumbs'][] = ['label' => 'Lojas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loja-create">

    <?= $this->render('_form-novo', [
        'model' => $model,
        'responsavel' => $responsavel,
        'contrato' => $contrato,
        'mensagem'=>$mensagem
    ]) ?>

</div>
