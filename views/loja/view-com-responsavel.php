
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Estado;
use app\models\Cidade;
use app\models\Bairro;
use app\models\Sexo;
use app\models\Grupo;
use app\models\Responsavel;
use app\models\Contrato;
use app\models\Agendamento;

/* @var $this yii\web\View */
/* @var $model app\models\Loja */
/* @var $form yii\widgets\ActiveForm */

?>

<style>
.fieldset-resp{
    margin: 30px 0 0 0;
}
.tabela-contratos{
    margin: 30px 0 0 0;
}
.btn-verContratos{
    margin: 5px 0 15px 0;
}

@media (min-width: 768px){
    .modal-content{
        width: 735px;
        margin: 0% -13%;
    }
}


</style>

<div class="loja-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <fieldset>
        <Legend><h1> Loja</h1> </legend>
    
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'razao_social')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'bairro_id')
                            ->dropDownList(
                                ArrayHelper::map(Bairro::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[bairro_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'estado_id')->textInput() ?>
                        <?= $form->field($model, 'estado_id')
                            ->dropDownList(
                                ArrayHelper::map(Estado::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[estado_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                    
                </div>
                <div class="row">
                    <?php echo $form->field($model, 'telefone')->textInput(['maxlength' => true,'class'=>'form-control telefone','disabled'=>true]) ?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'inscricao_municipal')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'nome_fantasia')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'cnpj')->textInput(['maxlength' => true,'class'=>'form-control cnpj','disabled'=>true]) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'endereco_numero')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'cep')->textInput(['maxlength' => true,'disabled'=>true,'class'=>'form-control cep']) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?php //echo $form->field($model, 'cidade_id')->textInput() ?>
                        <?= $form->field($model, 'cidade_id')
                            ->dropDownList(
                                ArrayHelper::map(Cidade::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[cidade_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'complemento')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?php //echo $form->field($model, 'grupo_id')->textInput() ?>
                        <?= $form->field($model, 'grupo_id')
                            ->dropDownList(
                                ArrayHelper::map(Grupo::find()->all(), 'id', 'grupo_nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[grupo_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '','disabled'=>true]) ?>
                    </div>

                </div>

            </div>
        </div>
        <?= Html::a('Ativar/Desativar', ['alterarstatus', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Alterar Loja', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </fieldset>


    <fieldset class="fieldset-resp">
        <Legend><h2>Responsável</h2></legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($responsavel, 'nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
                <div class="row">

                    <div class="metade">
                        <?php echo $form->field($responsavel, 'nascimento')->textInput(['maxlength' => true,'class'=>'form-control data','disabled'=>true]) ?>
                        <!-- <div class="form-group field-loja-responsavel_nascimento ">
                            <label class="control-label" for="loja-responsavel_nascimento">Responsavel Nascimento</label>
                            <input type="text" id="loja-responsavel_nascimento" class="form-control data" name="Responsavel[responsavel_nascimento]" maxlength="45" aria-invalid="false">

                            <div class="help-block"></div>
                        </div> -->
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'sexo_id')->textInput() ?>
                        <?= $form->field($responsavel, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '','disabled'=>true]) ?>
                    </div>
                    
                </div>
                <div class="row">
                    <?= $form->field($responsavel, 'email')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>
            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="metade">
                        <?= $form->field($responsavel, 'rg')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                    <?= $form->field($responsavel, 'cpf')->textInput(['maxlength' => true,'class'=>'form-control cpf','disabled'=>true]) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?= $form->field($responsavel, 'cargo')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($responsavel, 'telefone')->textInput(['maxlength' => true,'class'=>'form-control telefone','disabled'=>true]) ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <br>
    <fieldset>
        <Legend><h2>Contratos</h2></legend>
        <div class="btn btn-verContratos"><a href="<?php echo Url::to(['contrato/viewallcontratos','lid'=>$model->id]);?>"><div class="btn btn-info">Visualizar Todos os contratos</div></a> </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <?php echo $form->field($contrato, 'contrato_numero')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="row">

                        <div class="metade">
                            <?= $form->field($contrato, 'data_adesao')->input('date') ?>
                            <?php //echo $form->field($contrato, 'data_adesao')->textInput(['maxlength' => true,'class'=>'form-control data']) ?>
                        </div>
                        <div class="metade">
                            <?= $form->field($contrato, 'data_vencimento')->input('date') ?>
                            <?php //echo $form->field($contrato, 'data_vencimento')->textInput(['maxlength' => true,'class'=>'form-control data']) ?>
                        </div>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <?php echo $form->field($contrato, 'qtd_inventario')->textInput(['type'=>'number','min'=>0]) ?>
                    </div>
                    <div class="row">
                        <?= $form->field($contrato, 'contrato_arquivo')->fileInput() ?>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="btn btn-info"  onclick="$('#w0').submit()">
                    Salvar
                </div>

            </div> 
        
        <?php ActiveForm::end(); ?>

        <div class="tabela-contratos col-md-12">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Número</th>
                    <th scope="col">Vigência</th>
                    <th scope="col">Status</th>
                    <th scope="col">Contrato</th>
                    <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($contratos as $contrato):?>
                        <?php if($contrato->contrato_status != Contrato::STATUS_VENCIDO && $contrato->contrato_status != Contrato::STATUS_CANCELADO):?>
                            <tr>
                                <td> <?php echo $contrato->contrato_numero; ?> </td>
                                <td> <?php echo $contrato->data_adesao. " à " . $contrato->data_vencimento;?> </td>
                                <td> <?php echo $contrato->contrato_status; ?> </td>
                                <td> 
                                    <a target="_blank" href="<?php echo "../../".$contrato->contrato_arquivo;?>">
                                    <!-- <a target="_blank" href="<?php //echo Yii::$app->basePath;?>"> -->
                                        Contrato
                                    </a>
                                </td>
                                <td> 
                                    <a href="<?php echo Url::to(['contrato/update','id'=>$contrato->id]);?>"><div class="btn btn-warning">alterar</div></a> 
                                    <div class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter-<?php echo $contrato->id;?>">agendamento</div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter-<?php echo $contrato->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLongTitle">Agendamentos do Contrato: <?php echo $contrato->contrato_numero;?></h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php echo Yii::$app->controller->renderPartial('/agendamento/view-agendamentos-contrato', ['agendamentos'=>Agendamento::find()->where(['contrato_id'=>$contrato->id])->all()]); ?>
                                    </div>
                                    <div class="modal-footer">
                                        <?php echo Html::a('<button type="button" class="btn btn-warning" >Alterar</button>', Url::to(['agendamento/alteraragendamentosporcontrato','coid'=>$contrato->id])); ?>
                                        
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        
    </fieldset>

</div>




<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.cep').mask("99999-999");
        $('.telefone').mask("(99)99999-9999");
        // $('.data  ').mask("99/99/9999");
        $('.cnpj').mask('99.999.999/9999-99', {reverse: false});
        if(mensagem != ""){
            if(mensagem == "sucesso"){
                sweetAlert('success','Salvo','Contrato salvo com sucesso!');
            }else{
                sweetAlert('info','Erro',mensagem);
            }
        }
        
    })
</script>