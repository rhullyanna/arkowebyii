<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Responsavel;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LojaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lojas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loja-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastrar Loja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
        //  GridView::widget([
        //     'dataProvider' => $dataProvider,
        //     'filterModel' => $searchModel,
        //     'columns' => [
        //         ['class' => 'yii\grid\SerialColumn'],

        //         'id',
        //         'razao_social',
        //         'nome_fantasia',
        //         'cnpj',
        //         'inscricao_municipal',
        //         //'endereco',
        //         //'endereco_numero',
        //         //'complemento',
        //         //'bairro_id',
        //         //'estado_id',
        //         //'cidade_id',
        //         //'cep',
        //         //'telefone',
        //         //'grupo_id',
        //         //'status',
        //         //'responsavel_id',
        //         //'latitude',
        //         //'longitude',

        //         ['class' => 'yii\grid\ActionColumn'],
        //     ],
        // ]); 
    ?>
    <?= 
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
                ],
                [
                    'attribute' => 'razao_social',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'nome_fantasia',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'cnpj',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'responsavel_id',
                    'headerOptions' => ['class' => 'text-center'],
                    'value'  => function ($data) {
                        return Responsavel::findOne($data->responsavel_id)->nome;
                    },
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'status',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
    
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Ações', 
                'headerOptions' => ['width' => '80','style'=>'width: 10%; color:#337ab7;', 'class'=>'text-center'],
                'template' => '{view} {update} {mudastatus} {impressao}',
                'buttons' => [
                        'view' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>', Url::to(['loja/view','id'=>$key]));
                        },
                        'update' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-pencil" title="Alterar"></span>', Url::to(['loja/update','id'=>$key]));
                        },
                        'mudastatus' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-off" title="Ativar/Desativar"></span>', Url::to(['loja/alterarstatus','id'=>$key]));
                        },
                        'impressao' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-file" title="Imprimir"></span>', Url::to(['#']));
                        },
                    ],
                ],
            ],
        ]); 
    ?>
</div>
