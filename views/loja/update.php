<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loja */

$this->title = 'Alterar Loja: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lojas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loja-update">

    <h1><?php //echo Html::encode($this->title) ?></h1>

    <?= $this->render('_form-alterar', [
        'model' => $model,
        'responsavel'=>$responsavel,
        'mensagem' => $mensagem
    ]) ?>

</div>
