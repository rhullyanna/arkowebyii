
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Estado;
use app\models\Cidade;
use app\models\Bairro;
use app\models\Sexo;
use app\models\Grupo;
use app\models\Responsavel;

/* @var $this yii\web\View */
/* @var $model app\models\Loja */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="loja-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <fieldset>
        <Legend><h3>Cadastrar Loja</h3></legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'razao_social')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'bairro_id')
                            ->dropDownList(
                                ArrayHelper::map(Bairro::find()->orderBy(['descricao'=>SORT_ASC])->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[bairro_id]']    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'estado_id')->textInput() ?>
                        <?= $form->field($model, 'estado_id')
                            ->dropDownList(
                                ArrayHelper::map(Estado::find()->orderBy(['nome'=>SORT_ASC])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[estado_id]']    // options
                            ); ?>
                    </div>
                    
                </div>
                <div class="row">
                    <?php echo $form->field($model, 'telefone')->textInput(['maxlength' => true,'class'=>'form-control telefone']) ?>
                    <!-- <div class="form-group field-loja-telefone ">
                        <label class="control-label" for="loja-telefone">Telefone</label>
                        <input type="text" id="loja-telefone" class="form-control telefone" name="Loja[telefone]" maxlength="14" aria-invalid="false">

                        <div class="help-block"></div>
                    </div> -->
                </div>

                <div class="row">
                    <?= $form->field($model, 'inscricao_municipal')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'nome_fantasia')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'cnpj')->textInput(['maxlength' => true,'class'=>"form-control cnpj"]) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?= $form->field($model, 'endereco_numero')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'cep')->textInput(['maxlength' => true,'class'=>'form-control cep']) ?>
                    </div>
                    <div class="metade hide">
                        <?php echo $form->field($model, 'latitude')->textInput(['maxlength' => true,'class'=>'form-control ']) ?>
                    </div>
                    
                    <div class="metade hide">
                        <?php echo $form->field($model, 'longitude')->textInput(['maxlength' => true,'class'=>'form-control ']) ?>
                    </div>
                    
                </div>

                <div class="row">

                    <div class="metade">
                        <?php //echo $form->field($model, 'cidade_id')->textInput() ?>
                        <?= $form->field($model, 'cidade_id')
                            ->dropDownList(
                                ArrayHelper::map(Cidade::find()->orderBy(['nome'=>SORT_ASC])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[cidade_id]']    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'complemento')->textInput(['maxlength' => true]) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?php //echo $form->field($model, 'grupo_id')->textInput() ?>
                        <?= $form->field($model, 'grupo_id')
                            ->dropDownList(
                                ArrayHelper::map(Grupo::find()->where(['grupo_status'=>Grupo::STATUS_ATIVO])->orderBy(['grupo_nome'=>SORT_ASC])->all(), 'id', 'grupo_nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Loja[grupo_id]']    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'status')->dropDownList([ 'ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO', ], ['prompt' => '']) ?>
                    </div>

                </div>

            </div>
        </div>
    </fieldset>

    <fieldset>
        <Legend>Responsável</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($responsavel, 'nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="row">

                    <div class="metade">
                        <?php echo $form->field($responsavel, 'nascimento')->Input('date') ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'sexo_id')->textInput() ?>
                        <?= $form->field($responsavel, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '']) ?>
                    </div>
                    
                </div>
                <div class="row">
                    <?= $form->field($responsavel, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                
                
            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="metade">
                        <?= $form->field($responsavel, 'rg')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($responsavel, 'cpf')->textInput(['maxlength' => true,'class'=>'form-control cpf']) ?>
                    </div>

                </div>

                <div class="row">

                    <div class="metade">
                        <?= $form->field($responsavel, 'cargo')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($responsavel, 'telefone')->textInput(['maxlength' => true,'class'=>'form-control telefone']) ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <Legend>Contrato</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <?php echo $form->field($contrato, 'contrato_numero')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="row">

                    <div class="metade">
                        <?php echo $form->field($contrato, 'data_adesao')->textInput(['type'=>'date','maxlength' => true,'class'=>'form-control data']) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($contrato, 'data_vencimento')->textInput(['type'=>'date','maxlength' => true,'class'=>'form-control data']) ?>
                    </div>

                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <?php echo $form->field($contrato, 'qtd_inventario')->textInput(['type'=>'number','min'=>0]) ?>
                </div>
                <div class="row">
                    <?= $form->field($contrato, 'contrato_arquivo')->fileInput() ?>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- <div class="form-group">
        <?php //echo Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

    <div class="row" >
        <!-- <div class="btn btn-info" style="margin: 0px 0px 0px 46%;" onclick="$('#w0').submit()"> -->
        <div class="btn btn-info" style="margin: 0px 0px 0px 46%;" onclick="submit()">
            Salvar
        </div>
    </div>   

</div>


<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>
    var mensagem = "<?php echo $mensagem; ?>";

    function submit(){
        lat = document.getElementById("loja-latitude").value;
        lng = document.getElementById("loja-longitude").value;
        if((lat == "") || (lng == "") ){
            localizacao();
        }else{
            $('#w0').submit();
        }
        
    }

    function localizacao(){

        loading();
        let rua = document.getElementById("loja-endereco").value;
        let bairro = $("#loja-bairro_id option:selected").text();
        let num = document.getElementById("loja-endereco_numero").value;
        let cidade = $("#loja-cidade_id option:selected").text();
        let estado = $("#loja-estado_id option:selected").text();

        let myRequest = "https://maps.googleapis.com/maps/api/geocode/json?address="
        +num+","
        +rua+","
        +bairro+","
        +cidade+","
        +estado+
        "&key=AIzaSyDfrATuTFCBcf1sbKD-QY0x4UHSS_lB20A";

        fetch(myRequest)
        .then( function(resposta){ 
            resposta.json().then(function (json){
                document.getElementById('loja-latitude').value = json.results[0].geometry.location.lat;
                document.getElementById('loja-longitude').value = json.results[0].geometry.location.lng;
            }).then(function(){
                $('#w0').submit();
                encerraLoading();
            })
        });
    }

    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.cep').mask("99999-999");
        $('.telefone').mask("(99)99999-9999");
        // $('.data  ').mask("99/99/9999");
        $('.cnpj').mask('99.999.999/9999-99', {reverse: false});
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
        
    })
</script>