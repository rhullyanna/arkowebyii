<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarioenderecamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarioenderecamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inventario_id')->textInput() ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <?= $form->field($model, 'enderecamento_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
