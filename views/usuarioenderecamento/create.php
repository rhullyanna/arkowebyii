<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarioenderecamento */

$this->title = 'Montar Equipe';
$this->params['breadcrumbs'][] = ['label' => 'Inventário', 'url' => ['inventario/view','id'=>$model->inventario_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarioenderecamento-create">

    <h1><?php //echo Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
        'inventarios' => $inventarios,
        'inventariantes' => $inventariantes,
        'usuarios_enderecamentos' => $usuarios_enderecamentos,
        'all_enderecamentos'=>$all_enderecamentos,
        'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
        'mensagem'=>$mensagem
    ]) ?>

</div>
