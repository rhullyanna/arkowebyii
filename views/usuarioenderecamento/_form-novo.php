<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Usuario;
use app\models\Inventario;
use app\models\Enderecamento;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarioenderecamento */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .inventariante-plus{
        margin: 0px 0px 17px 0px;
        /* position: absolute; */
        width: 51px;
    }
</style>

<div id="cadastrar-equipe" class="usuarioenderecamento-form" >

    <h3><b>Inventário</b></h3>
    <hr>

    <?php $form = ActiveForm::begin(); ?>
    <div class=" row">
        <div class=" col-md-3">
            <?= $form->field($model, 'inventario_id')
                ->dropDownList(
                    ArrayHelper::map($inventarios, 'id', 'numero_inventario'),         // Flat array ('id'=>'label')
                    ['prompt'=>'Selecione ','disabled'=>true]    // options
                )->label('<h4><b>Número</b></h4>'); 
            ?>
        </div>
        <?php if($intervalo_de_enderecamentos):?>

            <div class=" col-md-3">
                <?= $form->field($intervalo_de_enderecamentos[0], 'id')
                    ->dropDownList(
                        ArrayHelper::map($all_enderecamentos, 'id', 'descricao'),         // Flat array ('id'=>'label')
                        ['prompt'=>'Selecione ','disabled'=>true]    // options
                    )->label('<h4><b>Inicio</b></h4>'); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($intervalo_de_enderecamentos[1], 'id')
                    ->dropDownList(
                        ArrayHelper::map($all_enderecamentos, 'id', 'descricao'),         // Flat array ('id'=>'label')
                        ['prompt'=>'Selecione ','disabled'=>true]    // options
                    )->label('<h4><b>Fim</b></h4>'); ?>
            </div>
        <?php endif;?>
    </div>
    
    

    <div class="row col-md-12">
        <h3><b>Equipe</b></h3>
        <hr>
        <div class="col-md-3">
            <h4><b>Inventariante</b></h4>
        </div>
        
        <div class="col-md-3">
            <h4><b>End. Inicial</b></h4>
        </div>

        <div class="col-md-3">
            <h4><b>End. Final</b></h4>
        </div>

        <div class="col-md-3">
            <h4><b></b></h4>
        </div>
    </div>

    <?php if($usuarios_enderecamentos):?>
            <?php foreach($usuarios_enderecamentos as $key => $usu_end):?>

                <div class="row col-md-12">
                    <div class="col-md-3 ">
                        <?= $form->field($usu_end, 'usuario_id')
                            ->dropDownList(
                                ArrayHelper::map($inventariantes, 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','disabled'=>true]    // options
                            )->label(False); ?>
                    </div>
                    
                    <div class="col-md-3 ">
                        <?= $form->field($usu_end, 'inicio')
                            ->dropDownList(
                                ArrayHelper::map($all_enderecamentos, 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','disabled'=>true]    // options
                            )->label(false); ?>
                    </div>

                    <div class="col-md-3 ">
                    <?= $form->field($usu_end, 'fim')
                            ->dropDownList(
                                ArrayHelper::map($all_enderecamentos, 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','disabled'=>true]    // options
                            )->label(false); ?>
                    </div>

                    <div class="col-md-3">
                        <?= Html::a('<b>-</b>', ['delete', 'i' => $usu_end->inicio,'f'=>$usu_end->fim], [
                            'class' => 'btn btn-danger inventariante-plus',
                            'data' => [
                                'confirm' => 'Deseja mesmo excluir esta atribuição de endereçamentos?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php endforeach;?>
    <?php endif;?>

    
    
    <div class="row col-md-12">
        <div class="col-md-3">
            <?php //echo $form->field($model, 'usuario_id')->textInput(['id'=>'usuario-1','name'=>'Usuarioenderecamento[usuario_id_1]'])->label(false) ?>
            <?= $form->field($model, 'usuario_id')
                ->dropDownList(
                    ArrayHelper::map($inventariantes, 'id', 'nome'),         // Flat array ('id'=>'label')
                    ['focus'=>true,'prompt'=>'Selecione ','id'=>'usuario-1','name'=>'Usuarioenderecamento[inventariantes][0][usuario_id]']    // options
                )->label(false); ?>
        </div>
        
        <div class="col-md-3">
            <?= $form->field($model, 'inicio')->textInput(['type'=>'number','id'=>'enderecamento-1','name'=>'Usuarioenderecamento[inventariantes][0][inicio]'])->label(false) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'fim')->textInput(['type'=>'number','id'=>'enderecamento-1','name'=>'Usuarioenderecamento[inventariantes][0][fim]'])->label(false) ?>
        </div>

        <div class="col-md-3">
            <!-- <div class="btn btn-success inventariante-plus">+</div> -->
            <?php echo Html::submitButton('<b>+</b>', ['class' => 'btn btn-success inventariante-plus']) ?>
        </div>
    </div>
    <div id="lista" class="hide">
        <?php for($i = 1; $i < 15; $i++):?>
            <div class=" row col-md-12">
                <div class="col-md-3">
                    <?= $form->field($model, 'usuario_id')
                        ->dropDownList(
                            ArrayHelper::map($inventariantes, 'id', 'nome'),         // Flat array ('id'=>'label')
                            ['prompt'=>'Selecione ','id'=>'usuario-'.$i,'name'=>'Usuarioenderecamento[inventariantes]['.$i.'][usuario_id]']    // options
                        )->label(false); ?>
                </div>
                
                <div class="col-md-3">
                    <?= $form->field($model, 'inicio')->textInput(['id'=>'enderecamento-'.$i,'name'=>'Usuarioenderecamento[inventariantes]['.$i.'][inicio]'])->label(false) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'fim')->textInput(['id'=>'enderecamento-'.$i,'name'=>'Usuarioenderecamento[inventariantes]['.$i.'][fim]'])->label(false) ?>
                </div>

                <div class="col-md-3">
                    <div class="btn btn-success inventariante-plus">+</div>
                </div>
            </div>
        <?php endfor;?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>

<script>

    var mensagem = "<?php echo $mensagem; ?>"
    $(document).ready(function($){
        if(mensagem != ""){
            sweetAlert('info','Erro',mensagem);
        }
    })
</script>