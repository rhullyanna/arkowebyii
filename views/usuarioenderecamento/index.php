<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioenderecamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarioenderecamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarioenderecamento-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Usuarioenderecamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'inventario_id',
            'usuario_id',
            'enderecamento_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
