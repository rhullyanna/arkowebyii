<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cnpj')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nascimento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pcd')->dropDownList([ 'SIM' => 'SIM', 'NAO' => 'NAO', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endereco_numero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bairro_id')->textInput() ?>

    <?= $form->field($model, 'cep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_id')->textInput() ?>

    <?= $form->field($model, 'cidade_id')->textInput() ?>

    <?= $form->field($model, 'complemento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cargo')->dropDownList([ 'GESTOR' => 'GESTOR', 'GERENTE' => 'GERENTE', 'COORDENADOR' => 'COORDENADOR', 'INVENTARIANTE' => 'INVENTARIANTE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'regime_id')->textInput() ?>

    <?= $form->field($model, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'foto')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
