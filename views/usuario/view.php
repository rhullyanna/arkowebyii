<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Visualizar Usuário';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'nome',
            'cpf',
            'cnpj',
            'rg',
            'nascimento',
            'pcd',
            'telefone',
            'endereco',
            'endereco_numero',
            'bairro_id',
            'cep',
            'estado_id',
            'cidade_id',
            'complemento',
            'email:email',
            'cargo',
            'regime_id',
            'sexo',
            'foto:ntext',
        ],
    ]) ?>

</div>
