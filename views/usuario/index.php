<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Usuários';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="usuario-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    

    <p>
        <?= Html::a('Cadastrar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= 
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
                ],
                // [
                //     'attribute' => 'id',
                //     'headerOptions' => ['class' => 'text-center'],
                //     'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                // ],
                [
                    'attribute' => 'nome',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'cargo',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'cpf',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'cnpj',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                    'attribute' => 'telefone',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Ações', 
                'headerOptions' => ['width' => '80','style'=>'width: 10%; color:#337ab7;', 'class'=>'text-center'],
                'template' => '{view} {update} {mudastatus} {impressao}',
                'buttons' => [
                        'view' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>', Url::to(['usuario/view','id'=>$key]));
                        },
                        'update' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-pencil" title="Alterar"></span>', Url::to(['usuario/update','id'=>$key]));
                        },
                        'impressaoficha' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-file" title="Imprimir"></span>', Url::to(['#']));
                        },
                        'produtividade' => function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-signal" title="Ver Produtividade"></span>', Url::to(['#']));
                        },
                    ],
                ],
            ],
        ]); 
    ?>
</div>
