<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Alterar Usuario: ' . $model->username;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-update">

  

    <?= $this->render('_form-alterar', [
        'model' => $model,
    ]) ?>

</div>
