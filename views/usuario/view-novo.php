<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sexo;
use app\models\Bairro;
use app\models\Estado;
use app\models\Cidade;
use app\models\Regime;

$this->title = 'Visualizar Usuário';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .metade{        
        width: 49%;
        display: inline-grid;
        margin: 0 0 0 3px;
    }
</style>



<div id="createUsuario" class="vue">

<fieldset>
    <legend><h3><?= Html::encode($this->title) ?></h3></legend>  
    
    <?php $form = ActiveForm::begin(); ?>
        <div class='col-md-12'>
            <div class="row">
                <?php 
                    if($model->foto){
                        echo Html::img('@web/'.$model->foto, ['alt'=>"ArkoWeb", "class"=>"img-profile"]);
                    }else{
                        echo Html::img('@web/images/profile-default.jpg', ['alt'=>"ArkoWeb", "class"=>"img-profile"]);
                    }
                ?>
                <?php  ?>
            </div>
            <div class='col-md-6'>
                <div class="row">
                    <?= $form->field($model, 'nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                </div>

                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'nascimento')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'sexo')->textInput(['disabled'=>true]) ?>
                        <?php /*echo $form->field($model, 'sexo_id')
                            ->dropDownList(
                                ArrayHelper::map(Sexo::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione um sexo','name'=>'Usuario[sexo_id]','disabled'=>true]    // options
                            ); */?>
                    </div>
                </div>

                <div class="row">
                    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true,'disabled'=>true]) ?>    
                </div>

                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'bairro_id')->textInput() ?>   
                        <?= $form->field($model, 'bairro_id')
                            ->dropDownList(
                                ArrayHelper::map(Bairro::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[bairro_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'estado_id')->textInput() ?>    
                        <?= $form->field($model, 'estado_id')
                            ->dropDownList(
                                ArrayHelper::map(Estado::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[estado_id]','disabled'=>true]    // options
                            ); ?>
                    </div>
                </div>

                <div class="row">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'disabled'=>true]) ?>     
                </div>
            </div>
            <div class='col-md-6'>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'rg')->textInput(['maxlength' => true,'disabled'=>true]) ?>    
                    </div> 
                    <?php if($model->cnpj == null) { ?>
                    <div class="metade">
                        <?= $form->field($model, 'cpf')->textInput(['maxlength' => true,'disabled'=>true])?>   
                    </div>
                    <?php } else{ ?>
                    <div class="metade">
                        <?= $form->field($model, 'cnpj')->textInput(['maxlength' => true,'disabled'=>true])?>  
                    </div>
                <?php } ?>
                </div>
                <div class="row">
                    <div class="metade">
                        <?php echo $form->field($model, 'pcd')->textInput(['disabled'=>true]) ?>    
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'telefone')->textInput(['maxlength' => true,'disabled'=>true]) ?>   
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'endereco_numero')->textInput(['maxlength' => true,'disabled'=>true]) ?>   
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'cep')->textInput(['maxlength' => true,'disabled'=>true]) ?>    
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'cidade_id')->textInput() ?>  
                        <?= $form->field($model, 'cidade_id')
                            ->dropDownList(
                                ArrayHelper::map(Cidade::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[cidade_id]','disabled'=>true]    // options
                            ); ?>  
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'complemento')->textInput(['maxlength' => true,'disabled'=>true]) ?>   
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?php echo $form->field($model, 'cargo')->textInput(['disabled'=>true]) ?>  
                        <?php /*echo $form->field($model, 'cargo')
                            ->dropDownList(
                                ArrayHelper::map(Cargo::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[cargo_id]','disabled'=>true]    // options
                            ); */?> 
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'regime_id')->textInput() ?>    
                        <?= $form->field($model, 'regime_id')
                            ->dropDownList(
                                ArrayHelper::map(Regime::find()->orderBy(['descricao'=>SORT_ASC])->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[regime_id]' ,'disabled'=>true]    // options
                            ); ?>
                    </div>
                </div>
            </div>
        </div>
    
        
    
    <?php ActiveForm::end(); ?>
        <div class="row" >
            
                <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            
            <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary']) ?>
        </div>        


    
</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script>

    $(document).ready(function(){
        let location_teste = location.search.split('&');
        if(location_teste.indexOf('mensagem=sucesso') > 0){
            sweetAlert('success','Salvo','Salvo com sucesso');
        }      
    })

</script>