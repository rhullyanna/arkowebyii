<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sexo;
use app\models\Pcd;
use app\models\Bairro;
use app\models\Estado;
use app\models\Cidade;
use app\models\Cargo;
use app\models\Regime;
// use antkaz\vue\VueAsset;
// VueAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .metade{        
        width: 49%;
        display: inline-grid;
        margin: 0 0 0 3px;
    }
</style>



<div id="createUsuario" class="vue">

    <fieldset>
    <legend><h3>Cadastrar de Usuário</h3></legend>    
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <!-- <form id="usuario-form" action="/usuario/create" method="post"> -->
        <div class='col-md-12'>
            <div class='col-md-6'>
                <div class="row">
                    <?= $form->field($model, 'nome')->textInput(['maxlength' => true, 'placeholder'=>'Nome completo']) ?>
                </div>

                <div class="row">
                    <div class="metade">
                        <?php echo $form->field($model, 'nascimento')->textInput(['type'=>'date','maxlength' => true,'class'=>'form-control data']) ?>
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '']) ?>
                    </div>
                </div>

                <div class="row">
                    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true, 'placeholder'=>'Rua,av,etc']) ?>    
                </div>

                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'bairro_id')->textInput() ?>   
                        <?= $form->field($model, 'bairro_id')
                            ->dropDownList(
                                ArrayHelper::map(Bairro::find()->orderBy(['descricao'=>SORT_ASC])->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[bairro_id]']    // options
                            ); ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'estado_id')->textInput() ?>    
                        <?= $form->field($model, 'estado_id')
                            ->dropDownList(
                                ArrayHelper::map(Estado::find()->orderBy(['nome'=>SORT_ASC])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[estado_id]']    // options
                            ); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="metade">
                        <?php //echo $form->field($model, 'cidade_id')->textInput() ?>  
                        <?= $form->field($model, 'cidade_id')
                            ->dropDownList(
                                ArrayHelper::map(Cidade::find()->orderBy(['nome'=>SORT_ASC])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[cidade_id]']    // options
                            ); ?>  
                    </div>
                    <div class="metade">
                        <?= $form->field($model, 'complemento')->textInput(['maxlength' => true, 'placeholder'=>'Informações adicionais']) ?>   
                    </div>
                </div>
                <div class="row">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder'=>'email@email.com']) ?>     
                </div>
                <div class="row">
                    <div id="campo_email_confirmacao" class="form-group">
                        <label  for="exampleInputEmail1">Confirmação de Email<span>*</span></label>
                        <input id="usuario-email-confirmacao" type="email" class="form-control" aria-describedby="" placeholder=" " name="Usuario[conf_email]" placeholder="Confirme o email">
                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>      
                </div>

            </div>
            <div class='col-md-6'>
                <div class="row">
                <div class="form-group field-usuario-pcd required ">
                    <label class="control-label" for="usuario-pcd">Tipo Usuário</label>
                    <select onchange="validaCampo(this);" id="usuario-tipo-pessoa" class="form-control" aria-required="true" aria-invalid="true">
                        <option value="fisica">Pessoa Física</option>
                        <option value="juridica">Pessoa Jurídica</option>
                    </select>
                </div>
                    
                </div>
                <div class="row">
                    <div class="metade">
                        <?php echo $form->field($model, 'rg')->textInput(['maxlength' => 15,]) ?>    
                    </div>
                    <div class="metade">  
                        <div class="form-group field-usuario-cpf ">
                            <?php echo $form->field($model, 'cpf')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999.999.999-99',]) ?>
                        </div>
                        <div class="form-group field-usuario-cnpj hide">
                            <?php echo $form->field($model, 'cnpj')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99.999.999/9999-99',]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'pcd')->dropDownList([ 'SIM' => 'SIM', 'NAO' => 'NAO', ], ['prompt' => '']) ?>
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'telefone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '(99)99999-9999',]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'endereco_numero')->textInput(['maxlength' => true, 'placeholder'=>'Número da casa']) ?>   
                    </div>
                    <div class="metade">
                        <?php echo $form->field($model, 'cep')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99999-999',]) ?>    
                    </div>
                </div>

                <div class="row">
                    <div class="metade">
                        <?= $form->field($model, 'cargo')->dropDownList([ 'GESTOR' => 'GESTOR', 'GERENTE' => 'GERENTE', 'COORDENADOR' => 'COORDENADOR', 'INVENTARIANTE' => 'INVENTARIANTE', ], ['prompt' => '']) ?>
                    </div>
                    <div class="metade">
                        <?php //echo $form->field($model, 'regime_id')->textInput() ?>    
                        <?= $form->field($model, 'regime_id')
                            ->dropDownList(
                                ArrayHelper::map(Regime::find()->all(), 'id', 'descricao'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Usuario[regime_id]']    // options
                            ); ?>
                    </div>
                </div>
                <div class="row">
                    <?= $form->field($model, 'foto')->fileInput() ?>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    <div class="row" >
        <div class="btn btn-info" style="margin: 0px 0px 0px 46%;" onclick="validaCpfCnpj();">
            Salvar
        </div>
        <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-primary','onclick'=>'loading()']) ?>
    </div>        

    <!-- </form> -->
    
</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>
<script>

    function validaCpfCnpj(){
        
        let preenchido = false;
        if($("#usuario-cpf").val()){
            preenchido = true;
            $(".field-usuario-cpf").removeClass('has-error')
        }else{
            $(".field-usuario-cpf").addClass('has-error')
        }
        if($("#usuario-cnpj").val()){
            preenchido = true;
            $(".field-usuario-cnpj").removeClass('has-error')
        }else{
            $(".field-usuario-cnpj").addClass('has-error')
        }

        if(preenchido == true){
            validaCampos();
        }
    }

    function validaCampo(e){
        console.log(e);
        let id = e.id;
        verificaPessoa($("#"+id));
    }
    
    function verificaPessoa(e){
        if(e.val() == 'fisica'){
            $(".field-usuario-cnpj").addClass('hide');
            $("#usuario-cnpj").removeAttr('required');

            $("#usuario-cpf").attr('required','required');
            $(".field-usuario-cpf").removeClass('hide');
        }else if(e.val() == 'juridica'){
            $(".field-usuario-cpf").addClass('hide');
            $("#usuario-cpf").removeAttr('required','required');

            $(".field-usuario-cnpj").removeClass('hide');
            $("#usuario-cnpj").attr('required','required');

            
            
        }
    }

</script>
