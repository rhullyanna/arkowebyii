<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Responsavel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="responsavel-form">

    <fieldset>
        <legend><h1>Cadastrar Responsável</h1></legend>
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'rg')->textInput(['maxlength' => 8]) ?>

                <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>

                <div class="form-group field-usuario-cpf ">
                    <label class="control-label" for="responsavel-cpf">Cpf</label>
                    <input type="text" id="responsavel-cpf" required class="form-control cpf" name="Responsavel[cpf]" maxlength="14" placeholder="000.000.000-00" aria-invalid="false">
                </div>

                <?php //echo $form->field($model, 'nascimento')->textInput(['maxlength' => true]) ?>
                <div class="form-group field-responsavel-nascimento">
                    <label class="control-label" for="responsavel-nascimento">Nascimento</label>
                    <input type="text" id="responsavel-nascimento" class="form-control nascimento" name="Responsavel[nascimento]" maxlength="10" placeholder="00/00/0000" aria-invalid="false">

                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'sexo')->dropDownList([ 'MASCULINO' => 'MASCULINO', 'FEMININO' => 'FEMININO', 'OUTROS' => 'OUTROS', ], ['prompt' => '']) ?>

                <?php //echo $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>
                <div class="form-group field-usuario-telefone ">
                    <label class="control-label" for="usuario-telefone">Telefone </label>
                    <input type="text" id="responsavel-telefone" class="form-control telefone" name="Responsavel[telefone]" maxlength="14" placeholder="(00)00000-0000" aria-invalid="false">

                    <div class="help-block"></div>
                </div>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'cargo')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        
    </fieldset>
</div>

<script src="../js/js-arko/jquery-3.3.1.min.js" ></script>
<script src="../js/js-arko/jquery.maskedinput.min.js" ></script>

<script>
    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.telefone').mask("(99)99999-9999");
        $('.nascimento  ').mask("99/99/9999");        
    })
</script>
