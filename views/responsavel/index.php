<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResponsavelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Responsáveis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="responsavel-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Responsavel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'rg',
            'cpf',
            'nascimento',
            //'sexo',
            //'telefone',
            //'email:email',
            //'cargo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
