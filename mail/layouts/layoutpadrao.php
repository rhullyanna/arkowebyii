<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="http://arko.devemandamento.com/arkowebyii/web/css/layoutemail.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title>Recuperar Senha</title>
    <?php $this->head() ?>
</head>
<body>
    <table  border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td width="260" valign="top">
                <table  border="1" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td style="background-color: #222; " align="center">
                    <img src="http://arko.devemandamento.com/arkowebyii/web/images/logo-login-copia.png" alt="" width="200px" height="140" style="display: block;" />
                    </td>
                    </tr>
                    <tr>
                    <td style="padding: 25px 0 0 0;">
                        <?= $content ?>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php //$this->beginBody() ?>
    
    <?php //$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
