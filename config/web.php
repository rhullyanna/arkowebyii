<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3cgPPBo2DR180Dz8l_KhOvK25vPeNyoQ',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<action>'=>'site/<action>',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            // 'enableSession'=>false,
            // 'loginUtl'=>null,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'devemandamento.com',
                // 'username' => 'sistemainventario@arkosolucoes.com.br',
                // 'password' => 'arko1234',
                'username' => 'arko-homologacao@devemandamento.com',
                'password' => 'arko-homologacao',
                'port' => '465',
                'encryption' => 'ssl',
            ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            // 'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'api/default'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/auditoriadivergencia'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/base'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/coleta'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/confronto'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/divergencia'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/enderecamento'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/inventario'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/login'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/loja'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/regime'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/usuario'
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller'=>'server/usuarioenderecamento'
                ],
            ],
        ],
        
    ],
    'params' => $params,
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\ApiModules',
        ],
        'server' => [
            'class' => 'app\modules\server\SeverModule',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = [
        // 'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    // ];

    // $config['bootstrap'][] = 'gii';
    // $config['modules']['gii'] = [
    //     'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    // ];
}

return $config;
