<?php

namespace app\modules\api\controllers;
use app\models\User;

// use yii\web\Controller;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpHeaderAuth;

/**
 * Default controller for the `api` module
 */
class DefaultController extends ActiveController
{

    public $modelClass = '\app\models\Usuario';

    public function behaviors(){
        $behabiors = parent::behaviors();
        $behabiors['authenticator'] = [
            'class' => HttpHeaderAuth::className(),
            'auth' => [$this, 'loginByAccessToken']
        ];
        return $behabiors;
    }

    // public function behaviors(){
    //     $behabiors = parent::behaviors();
    //     $behabiors['authenticator'] = [
    //         'class' => HttpBasicAuth::className(),
    //         'auth' => [$this, 'auth']
    //     ];
    //     return $behabiors;
    // }

    public function auth($username, $password){
        return User::findOne(['email'=>$username,'password'=>$password]);
    }

    public function loginByAccessToken($token, $type=null){

        return User::findOne(['accessToken'=>$token]);
    }

    public function actionCreate(){

    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
