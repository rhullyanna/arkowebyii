<?php

namespace app\modules\api;

use yii\filters\auth\HttpBasicAuth;
/**
 * api module definition class
 */
class ApiModules extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    // public function behavior(){
    //     $behabiors = parent::behavior();
    //     $behabiors['authenticator'] = [
    //         'class' => HttpBasicAuth::className()
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession =false;
        // custom initialization code goes here
    }
}
