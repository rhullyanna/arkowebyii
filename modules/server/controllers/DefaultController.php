<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;

class DefaultController extends ActiveController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
