<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Divergencia;

class DivergenciaController extends ActiveController
{
    public $modelClass = 'app\models\Divergencia';

}
