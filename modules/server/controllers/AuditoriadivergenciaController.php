<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\AuditoriaDivergencia;

class AuditoriadivergenciaController extends ActiveController
{
    public $modelClass = 'app\models\AuditoriaDivergencia';

}
