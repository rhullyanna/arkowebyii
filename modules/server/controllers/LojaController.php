<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Loja;

class LojaController extends ActiveController
{
    public $modelClass = 'app\models\Loja';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}