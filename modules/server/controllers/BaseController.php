<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Base;

class BaseController extends ActiveController
{
    public $modelClass = 'app\models\Base';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}

