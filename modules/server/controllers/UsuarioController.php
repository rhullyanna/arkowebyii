<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Usuario;

class UsuarioController extends ActiveController
{
    public $modelClass = 'app\models\Usuario';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}
