<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Inventario;

class InventarioController extends ActiveController
{
    public $modelClass = 'app\models\Inventario';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}
