<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Enderecamento;

class EnderecamentoController extends ActiveController
{
    public $modelClass = 'app\models\Enderecamento';

}
