<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Regime;

class RegimeController extends ActiveController
{
    public $modelClass = 'app\models\Regime';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}