<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Coleta;

class ColetaController extends ActiveController
{
    public $modelClass = 'app\models\Coleta';

}
