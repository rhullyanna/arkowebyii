<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Usuarioenderecamento;

class UsuarioenderecamentoController extends ActiveController
{
    public $modelClass = 'app\models\Usuarioenderecamento';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}
