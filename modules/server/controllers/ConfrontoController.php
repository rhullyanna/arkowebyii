<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Confronto;

class ConfrontoController extends ActiveController
{
    public $modelClass = 'app\models\Confronto';

}
