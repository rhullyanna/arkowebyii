<?php

namespace app\modules\server\controllers;

use yii\rest\ActiveController;
use app\models\Loginarko;

class LoginController extends ActiveController
{
    public $modelClass = 'app\models\Loginarko';

    public function actions()
    {
    	$actions=parent::actions();
    	unset($actions['delete'],$actions['create']);

    	return $actions;
    }
}