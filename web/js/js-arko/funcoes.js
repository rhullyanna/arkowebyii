
// let email = document.getElementById('usuario-email');
// let email_confirmacao = document.getElementById('usuario-email-confirmacao');
// let nome          = document.getElementById('usuario_nome');
// let dt_nascimento = document.getElementById('usuario_dt_nascimento');
// let sexo          = document.getElementById('usuario_sexo');
// let endereco      = document.getElementById('usuario_endereco');
// let bairro        = document.getElementById('usuario_bairro');
// let estado        = document.getElementById('usuario_estado');
// let rg            = document.getElementById('usuario_rg');
// let cpf           = document.getElementById('usuario_cpf');
// let pcd           = document.getElementById('usuario_pcd');
// let telefone      = document.getElementById('usuario_telefone');
// let numero        = document.getElementById('usuario_numero');
// let cep           = document.getElementById('usuario_cep');
// let cidade        = document.getElementById('usuario_cidade');
// let complemento   = document.getElementById('usuario_complemento');
// let cargo         = document.getElementById('usuario_cargo');
// let regime        = document.getElementById('usuario_regime');

function enviarEmails(){
    
    loading();
    fetch(window.location.href+"site/notificar")
    .then( function(resposta){ 
        resposta.json().then(function (json){
            console.log(json.message);
            if(json.status == true){
                sweetAlert('success',json.message);
            }else{
                sweetAlert('warning',json.message);
            }
        }).then(function(){
            // encerraLoading();
        })
    });
}

function verificaTipoInventario(){
    let valor = $("#inventario-tipo_inventario").val();
    console.log(valor);
    if(valor == 'VARREDURA'){
        $(".filtro-coleta").hide(500);
    }else{
        $(".filtro-coleta").show(500);
    }
    
}

function validaPassword(){

    let temcampovazio = false;

    let password = $("#loginarko-password");
    let password_confirmacao = $("#login-password-validation");

    if(password.val() != ""){
        $(".field-loginarko-password").removeClass('has-error');
        if(password_confirmacao.val() != ""){
            $(".field-login-password-validation").removeClass('has-error');
            if(password.val() != password_confirmacao.val()){
                $(".field-login-password-validation").addClass('has-error');
                sweetAlert('warning','Campos errados', 'Senhas diferentes, por favor verifique os campos.');
            }
            
        }else{
            $(".field-login-password-validation").addClass('has-error');
            temcampovazio = true;
        }
    }else{
        $(".field-loginarko-password").addClass('has-error');
        temcampovazio = true;
    }

    return temcampovazio;

}

function validaEmail(){

    let temcampovazio = false;

    let email = $("#usuario-email");
    let email_confirmacao = $("#usuario-email-confirmacao");

    console.log(email.val());
    console.log(email_confirmacao.val());

    if(email.val() != ""){
        $(".field-usuario-email").removeClass('has-error');
        if(email_confirmacao.val() != ""){
            $("#campo_email_confirmacao").removeClass('has-error');
            if(email.val() != email_confirmacao.val()){
                // temcampovazio = true;
                email_confirmacao.addClass('has-error');
                sweetAlert('warning','Campos errados', 'Emails diferentes, por favor verifique os campos referentes a email.');
            }
            
        }else{
            document.getElementById('campo_email_confirmacao').classList.add('has-error');
            temcampovazio = true;
        }
    }else{
        $(".field-usuario-email").addClass('has-error');
        temcampovazio = true;
    }

    return temcampovazio;
    
}

function validaCampos(){

    let temcampovazio = false;

    if(validaEmail() == true){
        temcampovazio = true;
    }
    // if( validacaoVazio(nome) == true){ temcampovazio = true }
    // if( validacaoVazio(sexo) == true){ temcampovazio = true }
    // if( validacaoVazio(cpf) == true){ temcampovazio = true }
    // if( validacaoVazio(pcd) == true){ temcampovazio = true }
    // if( validacaoVazio(telefone) == true){ temcampovazio = true }
    // if( validacaoVazio(cargo) == true){ temcampovazio = true }
    // if( validacaoVazio(regime) == true){ temcampovazio = true }

    // validacaoVazio(dt_nascimento);
    // validacaoVazio(endereco);
    // validacaoVazio(bairro);
    // validacaoVazio(estado);
    // validacaoVazio(rg);
    // validacaoVazio(numero);
    // validacaoVazio(cep);
    // validacaoVazio(cidade);
    // validacaoVazio(complemento);
    
    if(temcampovazio == true){
        
        sweetAlert('warning','Campos Vazios','Por favor, preencha os campos obrigatorios');
        console.log('não tem campo vazio');
    }else{
        console.log('não tem campo vazio');
        // $("#usuario-form").submit();
        $("#w0").submit();
    }

}

function validacaoVazio(e){
    let id_campo = e.id.split('usuario_')[1];
    if(e.value ==""){
        console.log(e.value);
        $("#campo_"+id_campo).addClass('has-error');
        return true;
    }else{
        $("#campo_"+id_campo).removeClass('has-error');
    }
    return false;
}

function sweetAlert(tipo, titulo ,texto){
    swal({
        title:  titulo,
        type:   tipo,
        html:   texto,
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: true,
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i> Ok!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        preConfirm: function () {
          
        }
    });
}

function loading(){
    swal({
        title: "Loading...",
        id:'load-modal',
        onOpen: () => {
            swal.showLoading()
            timerInterval = setInterval(() => {
            },100)
        },
    });
}

function encerraLoading(){
    swal.close();
}