<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado".
 *
 * @property int $id
 * @property int $codigouf
 * @property string $nome
 * @property string $uf
 * @property int $regiao
 *
 * @property Loja[] $lojas
 * @property Usuario[] $usuarios
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigouf', 'regiao'], 'integer'],
            [['nome'], 'string', 'max' => 100],
            [['uf'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigouf' => 'Codigouf',
            'nome' => 'Nome',
            'uf' => 'Uf',
            'regiao' => 'Regiao',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLojas()
    {
        return $this->hasMany(Loja::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['estado_id' => 'id']);
    }
}
