<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventario;

/**
 * InventarioSearch represents the model behind the search form of `app\models\Inventario`.
 */
class InventarioSearch extends Inventario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'agendamento_id', 'usuario_resp_cadastro_inventario_id', ], 'integer'],
            [['previsao_duracao', 'tipo_inventario', 'validade', 'fabricacao', 'usuario_coordenador_id','lote', 'itens_embalagem', 'marca', 'fornecedor', 'descricao', 'validade_por_meses', 'ignorar_zero_esq', 'ignorar_zero_direita', 'data_inicio', 'hora_inicio', 'data_fim', 'hora_fim', 'inventario_status', 'numero_inventario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventario::find()->joinWith(['usuarioCoordenador','agendamento']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'usuario_resp_cadastro_inventario_id' => $this->usuario_resp_cadastro_inventario_id,
        ]);

        $query->andFilterWhere(
            ['like','agendamento.data_agendamento',$this->agendamento_id],
            ['like','usuarioCoordenador.nome',$this->usuario_coordenador_id]
        );

        $query
            ->andFilterWhere(['like', 'previsao_duracao', $this->previsao_duracao])
            ->andFilterWhere(['like', 'tipo_inventario', $this->tipo_inventario])
            ->andFilterWhere(['like', 'validade', $this->validade])
            ->andFilterWhere(['like', 'fabricacao', $this->fabricacao])
            ->andFilterWhere(['like', 'lote', $this->lote])
            ->andFilterWhere(['like', 'itens_embalagem', $this->itens_embalagem])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'descricao', $this->validade_por_meses])
            ->andFilterWhere(['like', 'fornecedor', $this->fornecedor])
            ->andFilterWhere(['like', 'ignorar_zero_esq', $this->ignorar_zero_esq])
            ->andFilterWhere(['like', 'ignorar_zero_direita', $this->ignorar_zero_direita])
            ->andFilterWhere(['like', 'data_inicio', $this->data_inicio])
            ->andFilterWhere(['like', 'hora_inicio', $this->hora_inicio])
            ->andFilterWhere(['like', 'data_fim', $this->data_fim])
            ->andFilterWhere(['like', 'hora_fim', $this->hora_fim])
            ->andFilterWhere(['like', 'inventario_status', $this->inventario_status])
            ->andFilterWhere(['like', 'numero_inventario', $this->numero_inventario]);

        return $dataProvider;
    }
}
