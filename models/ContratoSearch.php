<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contrato;

/**
 * ContratoSearch represents the model behind the search form of `app\models\Contrato`.
 */
class ContratoSearch extends Contrato
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'qtd_inventario', 'loja_id'], 'integer'],
            [['contrato_numero', 'data_adesao', 'data_vencimento', 'contrato_arquivo', 'contrato_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contrato::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qtd_inventario' => $this->qtd_inventario,
            'loja_id' => $this->loja_id,
        ]);

        $query->andFilterWhere(['like', 'contrato_numero', $this->contrato_numero])
            ->andFilterWhere(['like', 'data_adesao', $this->data_adesao])
            ->andFilterWhere(['like', 'data_vencimento', $this->data_vencimento])
            ->andFilterWhere(['like', 'contrato_arquivo', $this->contrato_arquivo])
            ->andFilterWhere(['like', 'contrato_status', $this->contrato_status]);

        return $dataProvider;
    }
}
