<?php

namespace app\models;
use app\models\Agendamento;
use app\models\Contrato;
use app\models\Loja;

use Yii;

/**
 * This is the model class for table "inventario".
 *
 * @property int $id
 * @property int $agendamento_id
 * @property int $usuario_resp_cadastro_inventario_id
 * @property int $usuario_coordenador_id
 * @property string $previsao_duracao
 * @property string $tipo_inventario
 * @property string $validade
 * @property string $fabricacao
 * @property string $lote
 * @property string $itens_embalagem
 * @property string $marca
 * @property string $fornecedor
 * @property string $descricao
 * @property string $validade_por_meses
 * @property string $ignorar_zero_esq
 * @property string $ignorar_zero_direita
 * @property string $data_inicio
 * @property string $hora_inicio
 * @property string $data_fim
 * @property string $hora_fim
 * @property string $inventario_status
 * @property string $numero_inventario
 *
 * @property Base[] $bases
 * @property Confronto[] $confrontos
 * @property Divergencia[] $divergencias
 * @property Enderecamento[] $enderecamentos
 * @property Equipe[] $equipes
 * @property Agendamento $agendamento
 * @property Usuario $usuarioRespCadastroInventario
 * @property Usuario $usuarioCoordenador
 * @property UsuarioEnderecamento[] $usuarioEnderecamentos
 */
class Inventario extends \yii\db\ActiveRecord
{
    CONST STATUS_ATIVO = 'ATIVO';
    CONST STATUS_INATIVO = 'INATIVO';
    CONST STATUS_EM_ANDAMENTO = 'EM ANDAMENTO';
    CONST STATUS_EM_DIVERGENCIA = 'EM DIVERGENCIA';
    CONST STATUS_CONCLUIDO = 'CONCLUIDO';

    CONST TIPO_VARREDURA = 'VARREDURA';
    CONST TIPO_SEMI_VARREDURA = 'SEMI VARREDURA';

    public $filtros;
    const ARRAY_FILTROS_INVENTARIO = [
        'ignorar_zero_esq',
        'validade',
        'fabricacao',
        'lote',
        'itens_embalagem',
        'marca',
        'fornecedor',
        'ignorar_zero_direita',
        'descricao',
        'validade_por_meses'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agendamento_id', 'numero_inventario' , 'previsao_duracao' ,'usuario_resp_cadastro_inventario_id', 'usuario_coordenador_id'], 'required'],
            [['agendamento_id', 'usuario_resp_cadastro_inventario_id', 'usuario_coordenador_id'], 'integer'],
            [['tipo_inventario', 'itens_embalagem', 'ignorar_zero_esq', 'ignorar_zero_direita', 'inventario_status'], 'string'],
            [['previsao_duracao'], 'string', 'max' => 5],
            [['filtros'],'safe'],
            [['validade', 'fabricacao', 'lote', 'marca', 'fornecedor', 'descricao', 'validade_por_meses', 'data_inicio', 'hora_inicio', 'data_fim', 'hora_fim'], 'string', 'max' => 45],
            [['numero_inventario'], 'string', 'max' => 100],
            [['agendamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agendamento::className(), 'targetAttribute' => ['agendamento_id' => 'id']],
            [['usuario_resp_cadastro_inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_resp_cadastro_inventario_id' => 'id']],
            [['usuario_coordenador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_coordenador_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agendamento_id' => 'Agendamento',
            'usuario_resp_cadastro_inventario_id' => 'Usuario que Cadastrou o Inventario ',
            'usuario_coordenador_id' => 'Coordenador',
            'previsao_duracao' => 'Previsão Duração',
            'tipo_inventario' => 'Tipo de Inventario',
            'validade' => 'Validade',
            'fabricacao' => 'Fabricação',
            'lote' => 'Lote',
            'itens_embalagem' => 'Itens Embalagem',
            'marca' => 'Marca',
            'fornecedor' => 'Fornecedor',
            'ignorar_zero_esq' => 'Ignorar Zeros a Esquerda',
            'ignorar_zero_direita' => 'Ignorar Zeros a Direita',
            'data_inicio' => 'Data Inicio',
            'hora_inicio' => 'Hora Inicio',
            'data_fim' => 'Data Fim',
            'hora_fim' => 'Hora Fim',
            'inventario_status' => 'Status',
            'numero_inventario' => 'Número Inventário',
            'filtros' => 'Configurações de coleta',
            'descricao' => 'Descrição',
            'validade_por_meses'=>'Validade em meses'
        ];
    }


    public static function getFiltrosMarcados($inventario){

        $filtros = [];

        if($inventario->ignorar_zero_esq == 'SIM'){
            array_push($filtros,'ignorar_zero_esq');
        }
        if($inventario->validade == 'SIM'){
            array_push($filtros,'validade');
        }
        if($inventario->fabricacao == 'SIM'){
            array_push($filtros,'fabricacao');
        }
        if($inventario->lote == 'SIM'){
            array_push($filtros,'lote');
        }
        if($inventario->itens_embalagem == 'SIM'){
            array_push($filtros,'itens_embalagem');
        }
        if($inventario->marca == 'SIM'){
            array_push($filtros,'marca');
        }
        if($inventario->fornecedor == 'SIM'){
            array_push($filtros,'fornecedor');
        }
        if($inventario->ignorar_zero_direita == 'SIM'){
            array_push($filtros,'ignorar_zero_direita');
        }
        if($inventario->descricao == 'SIM'){
            array_push($filtros,'descricao');
        }
        if($inventario->validade_por_meses == 'SIM'){
            array_push($filtros,'validade_por_meses');
        }

        $inventario->filtros = $filtros;

        return $inventario;
        
    }

    // gera a numeração do inventario com a concatenação de identificadores unicos de loja e agendamento.
    public static function gerarNumero_inventario($agendamento, $loja){
        return "L".$loja->id."-A".$agendamento->id;
    }

    public static function getLoja($agid){
        $agendamento = Agendamento::findOne($agid);
        $contrato = Contrato::findOne($agendamento->contrato_id);
        $loja = Loja::findOne($contrato->loja_id);
        return $loja;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBases()
    {
        return $this->hasMany(Base::className(), ['inventario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfrontos()
    {
        return $this->hasMany(Confronto::className(), ['inventario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivergencias()
    {
        return $this->hasMany(Divergencia::className(), ['inventario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnderecamentos()
    {
        return $this->hasMany(Enderecamento::className(), ['inventario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipes()
    {
        return $this->hasMany(Equipe::className(), ['inventario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendamento()
    {
        return $this->hasOne(Agendamento::className(), ['id' => 'agendamento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioRespCadastroInventario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_resp_cadastro_inventario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioCoordenador()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_coordenador_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioEnderecamentos()
    {
        return $this->hasMany(UsuarioEnderecamento::className(), ['inventario_id' => 'id']);
    }
}
