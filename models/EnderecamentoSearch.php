<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Enderecamento;

/**
 * EnderecamentoSearch represents the model behind the search form of `app\models\Enderecamento`.
 */
class EnderecamentoSearch extends Enderecamento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'inventario_id'], 'integer'],
            [['descricao','descricao_proprietario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Enderecamento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inventario_id' => $this->inventario_id,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao])
        ->andFilterWhere(['like', 'descricao', $this->descricao_proprietario]);

        return $dataProvider;
    }
}
