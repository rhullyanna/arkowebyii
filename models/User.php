<?php

namespace app\models;
use yii\db\ActiveRecord;
use app\models\Loginarko;

// class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    CONST GESTOR = 'GESTOR';
    CONST GERENTE = 'GERENTE';
    CONST COORDENADOR = 'COORDENADOR';
    CONST INVENTARIANTE = 'INVENTARIANTE';

    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    public $cpf;
    public $login_status;
    public $cargo;
    public $cargo_gestor = 'GESTOR';
    public $cargo_gerente = 'GERENTE';
    public $cargo_coordenador = 'COORDENADOR';
    public $cargo_inventariante = 'INVENTARIANTE';
    // public $email;
    // public $nome;
    // public $rg;
    // public $nascimento;
    // public $telefone;
    // public $endereco;
    // public $endereco_numero;
    // public $cep;
    // public $complemento;
    // public $sexo_id;
    // public $pcd_id;
    // public $bairro_id;
    // public $estado_id;
    // public $cidade_id;
    // public $cargo_id;
    // public $regime_id;

    // private static $users = [
    //     '100' => [
    //         'id' => '100',
    //         'username' => 'admin',
    //         'password' => 'admin',
    //         'authKey' => 'test100key',
    //         'accessToken' => '100-token',
    //     ],
    //     '101' => [
    //         'id' => '101',
    //         'username' => 'demo',
    //         'password' => 'demo',
    //         'authKey' => 'test101key',
    //         'accessToken' => '101-token',
    //     ],
    // ];

    public static function tableName()
    {
        return 'login';
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user = Loginarko::find()->where(['id'=>$id])->one();

        if ($user) {
            return new static($user);
        }

        return null;
        // return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    public static function findByEmail($email){
        $user = Loginarko::find()->where(['email'=>$email])->one();

        if ($user) {
            return new static($user);
        }

        return null;
    }

    public static function findByUsercpf($cpf){

        $users = Loginarko::find()->All();
        foreach ($users as $user) {
            if (strcasecmp($user['cpf'], $cpf) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        // echo "<pre>"; print_r("asfdasfsf");die;
        return static::findOne(['accessToken' => $token]);

        // foreach (self::$users as $user) {
        //     if ($user['accessToken'] === $token) {
        //         return new static($user);
        //     }
        // }
        

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public static function is_gestor($user){
        $usuario_aux = new User();
        if($user->cargo == $usuario_aux->cargo_gestor){
            return true;
        }
        return false;
    }

    public static function is_gerente($user){
        $usuario_aux = new User();
        if($user->cargo == $usuario_aux->cargo_gerente){
            return true;
        }
        return false;
    }

    public static function is_coordenador($user){
        $usuario_aux = new User();
        if($user->cargo == $usuario_aux->cargo_coordenador){
            return true;
        }
        return false;
    }

    public static function is_inventariante($user){
        $usuario_aux = new User();
        if($user->cargo == $usuario_aux->cargo_inventariante){
            return true;
        }
        return false;
    }
}
