<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coleta".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $data
 * @property string $hora
 * @property string $enderecamento
 * @property string $cod_barra
 * @property string $validade
 * @property string $fabricacao
 * @property string $lote
 * @property int $itens_embalagem
 * @property string $marca
 * @property string $fornecedor
 *
 * @property Usuario $usuario
 * @property Confronto[] $confrontos
 * @property Divergencia[] $divergencias
 */
class Coleta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coleta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id'], 'required'],
            [['usuario_id', 'itens_embalagem'], 'integer'],
            [['data', 'hora', 'enderecamento', 'cod_barra', 'validade', 'fabricacao', 'lote', 'marca', 'fornecedor'], 'string', 'max' => 45],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'data' => 'Data',
            'hora' => 'Hora',
            'enderecamento' => 'Enderecamento',
            'cod_barra' => 'Cod Barra',
            'validade' => 'Validade',
            'fabricacao' => 'Fabricacao',
            'lote' => 'Lote',
            'itens_embalagem' => 'Itens Embalagem',
            'marca' => 'Marca',
            'fornecedor' => 'Fornecedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfrontos()
    {
        return $this->hasMany(Confronto::className(), ['coleta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivergencias()
    {
        return $this->hasMany(Divergencia::className(), ['coleta_id' => 'id']);
    }
}
