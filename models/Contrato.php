<?php

namespace app\models;

use Yii;
use DateTime;
use app\models\Loja;

/**
 * This is the model class for table "contrato".
 *
 * @property int $id
 * @property string $contrato_numero
 * @property string $data_adesao
 * @property string $data_vencimento
 * @property int $qtd_inventario
 * @property string $contrato_arquivo
 * @property int $loja_id
 * @property string $contrato_status
 *
 * @property Agendamento[] $agendamentos
 * @property Loja $loja
 */
class Contrato extends \yii\db\ActiveRecord
{

    const DATA_ACEITAVEL = "2000-01-01";

    const STATUS_ABERTO = "ABERTO";
    CONST STATUS_EM_ANDAMENTO = "EM ANDAMENTO";
    CONST STATUS_VENCIDO = "VENCIDO";
    CONST STATUS_CANCELADO = "CANCELADO";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contrato';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qtd_inventario', 'loja_id'], 'integer'],
            [['contrato_status'], 'string'],
            [['contrato_numero'], 'unique'],
            [['contrato_arquivo'], 'file', 'extensions' => 'pdf'],
            [['loja_id','qtd_inventario','contrato_numero','data_adesao', 'data_vencimento'], 'required'],
            [['contrato_numero'],'string', 'max' => 1000],
            [['data_adesao', 'data_vencimento'], 'string', 'max' => 45],
            [['loja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Loja::className(), 'targetAttribute' => ['loja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contrato_numero' => 'Número Contrato',
            'data_adesao' => 'Data de Adesão',
            'data_vencimento' => 'Data do Vencimento',
            'qtd_inventario' => 'Quantidade de Inventários',
            'contrato_arquivo' => 'Contrato',
            'loja_id' => 'Loja',
            'contrato_status' => 'Contrato Status',
        ];
    }


    public static function verificaSobreposicaoDeContrato($contrato){
        
        // converte as datas pra timestamp
        $dt_vencimento_timestamp = strtotime($contrato->data_vencimento);
        $dt_adesao_timestamp = strtotime($contrato->data_adesao);

        $contratos_ativos_loja = Contrato::find()->where(['loja_id'=>$contrato->loja_id,'contrato_status'=>[Contrato::STATUS_ABERTO,Contrato::STATUS_EM_ANDAMENTO] ])->all();

        $contratos_sobrepostos = [];

        foreach($contratos_ativos_loja as $contrat){

            $contrat->data_adesao = Contrato::dateBrParaDateString($contrat->data_adesao);
            $contrat->data_vencimento = Contrato::dateBrParaDateString($contrat->data_vencimento);

            $dt_vencimento_timestamp_aux = strtotime($contrat->data_vencimento);
            $dt_adesao_timestamp_aux = strtotime($contrat->data_adesao);

            if( $dt_adesao_timestamp >= $dt_adesao_timestamp_aux ){
                if($dt_adesao_timestamp <= $dt_vencimento_timestamp_aux){
                    array_push($contratos_sobrepostos,$contrat->contrato_numero);
                }

            }else if($dt_vencimento_timestamp >= $dt_adesao_timestamp_aux){
                if($dt_vencimento_timestamp <= $dt_vencimento_timestamp_aux){
                    array_push($contratos_sobrepostos,$contrat->contrato_numero);
                }
            }

            
        }
        // echo "<pre>"; print_r($contratos_sobrepostos);
        // die;
        // pega todos os contratos ativos
        // echo "<pre>"; print_r($contrato->attributes);
        // echo "<pre>"; print_r($dt_vencimento_timestamp);
        // echo "<pre>"; print_r($dt_adesao_timestamp);
        // die;
        
        return $contratos_sobrepostos;


    }

    public static function dividirOtempoDeContratoPeloNumeroDeInventarios($contrato){
        // transforma as datas pra formato americano
        $data_vencimento = explode('/',$contrato->data_vencimento);
        $data_vencimento = $data_vencimento[2]."-".$data_vencimento[1]."-".$data_vencimento[0];

        $data_adesao = explode('/',$contrato->data_adesao);
        $data_adesao = $data_adesao[2]."-".$data_adesao[1]."-".$data_adesao[0];
        
        // converte as datas pra timestamp
        $dt_vencimento_timestamp = strtotime($data_vencimento);
        $dt_adesao_timestamp = strtotime($data_adesao);

        $datas_para_agendamento = [];
        
        // subtrai as datas para ver se o contrato ja venceu ou não
        if(($dt_vencimento_timestamp - $dt_adesao_timestamp) >= 0){
            // subtrair a data de adesao da data de vencimento, para saver o intervalo entre elas
            $dias_para_vencer = $dt_vencimento_timestamp - $dt_adesao_timestamp;
            // verifica se a quantidade de inventarios é menor ou igual aos dias para vencer, partindo do pre-suposto que é feito 1 inventario por dia, no máximo
            if(($dias_para_vencer/86400) >= $contrato->qtd_inventario){
                // contabiliza quantos dias será o intervalo entre os inventarios contratados pela loja
                $intervalo_entre_inventarios = $dias_para_vencer/$contrato->qtd_inventario;
                // echo "<pre>"; print_r("adesao: {$contrato->data_adesao} | vencimento: {$contrato->data_vencimento}");
                // echo "<pre>"; print_r("dias de contrato: ".($dias_para_vencer/86400));
                // echo "<pre>"; print_r("intervalo em dias entre os {$contrato->qtd_inventario} intervalos: ".intval($intervalo_entre_inventarios/86400));
                // echo "<pre>"; print_r("datas dos inventarios: ");
                $data_aux = $dt_adesao_timestamp;
                for($i = 1; $i <= $contrato->qtd_inventario; $i++){
                    if($i != 1){
                        $data_aux = $data_aux + $intervalo_entre_inventarios;
                    }
                    // array_push($datas_para_agendamento, date('d/m/Y',$data_aux));
                    array_push($datas_para_agendamento, date('Y-m-d',$data_aux));
                }
            }
            return $datas_para_agendamento;
        }
    }

    public static function verificaContratosPrestesAVencer(){

        // array que vai guardar todos os contratos prestes a vencer de todas as lojas cadastradas e ativas
        $contratos_prestes_a_vencer = [];

        $lojas = Loja::find()->where(['status'=>Loja::STATUS_ATIVO])->all();

        foreach($lojas as $loja){
            $contratos = Contrato::find()->where(['loja_id'=>$loja->id])->all();
            foreach($contratos as $contrato){
                // transforma as datas pra formato americano
                $data = explode('/',$contrato->data_vencimento);
                $data = $data[2]."-".$data[1]."-".$data[0];
                // converte as datas pra timestamp
                $dt_vencimento_timestamp = strtotime($data);
                $dt_atual_timestamp = strtotime(date('Y-m-d'));
                
                // subtrai as datas para ver se o contrato ja venceu ou não
                if(($dt_vencimento_timestamp - $dt_atual_timestamp) > 0){
                    // divide o numero de milisegundo restantes pelo numero de milisegundo de um dia, para saber o intervalo entre a data atual e o vencimento
                    $dias_para_vencer = ($dt_vencimento_timestamp - $dt_atual_timestamp)/86400;
                    // verifica se o intervalo entre a data atual e o vencimento é menor que 30
                    if($dias_para_vencer < 30){
                        // manda um array com informações do contrato para o array principal
                        array_push($contratos_prestes_a_vencer,[
                            'numero'=>$contrato->contrato_numero,
                            'vencimento'=>$contrato->data_vencimento,
                            'loja'=>Loja::findOne($contrato->loja_id)->nome_fantasia
                        ]);
                    }
                }
            }
        }
        return $contratos_prestes_a_vencer;

    }

    public static function verificar_criar_diretorio($loja){
        if(is_dir(Yii::$app->basePath."/web/uploads/contratos/{$loja->id}/")){
            return true;
        }else{
            if(mkdir(Yii::$app->basePath."/web/uploads/contratos/{$loja->id}/", 0777, true)){
                return true;
            }
        }
        return false;
    }

    public static function salvarContrato($loja,$contrato){
        if($contrato->contrato_arquivo){
            if(Contrato::verificar_criar_diretorio($loja)){
                $caminho_arquivo = "web/uploads/contratos/{$loja->id}/{$loja->id}_{$contrato->contrato_numero}_".date('dmY-his').".".$contrato->contrato_arquivo->extension;
                if($contrato->contrato_arquivo->saveAs(Yii::$app->basePath."/".$caminho_arquivo)){
                    $contrato->contrato_arquivo = $caminho_arquivo;
                    return $contrato;
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    public static function dateBrParaDateString($data){
        $data = explode("/",$data);
        $data = $data[2]."-".$data[1]."-".$data[0];
        return $data;
    }

    public static function dateTodateBR($data){
        $data = explode("-",$data);
        $data = $data[2]."/".$data[1]."/".$data[0];
        return $data;
    }

    // "dd/mm/YYYY -> "YYYY-mm-dd"
    public static function dateBrToDateMysql($data){
        $data = explode("/",$data);
        // echo "<pre>"; print_r($data);die;
        $data = $data[2]."-".$data[1]."-".$data[0];
        $data = new DateTime($data);
        return $data;
    }

    public static function verificaAdesaoVencimento($contrato){
        // $data_adesao = Contrato::dateBrToDateMysql($contrato->data_adesao);
        $data_adesao = $contrato->data_adesao;
        // $data_vencimento = Contrato::dateBrToDateMysql($contrato->data_vencimento);
        $data_vencimento = $contrato->data_vencimento;
        // echo "<pre>"; print_r($contrato->attributes);die;
        if($data_adesao > Contrato::DATA_ACEITAVEL){
            if($data_vencimento > Contrato::DATA_ACEITAVEL){
                if($data_adesao <= $data_vencimento){
                    return true;
                }
            }
        }
        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->data_adesao = Contrato::dateTodateBR($this->data_adesao);
            $this->data_vencimento = Contrato::dateTodateBR($this->data_vencimento);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendamentos()
    {
        return $this->hasMany(Agendamento::className(), ['contrato_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoja()
    {
        return $this->hasOne(Loja::className(), ['id' => 'loja_id']);
    }
}
