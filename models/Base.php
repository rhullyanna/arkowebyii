<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "base".
 *
 * @property int $id
 * @property int $inventario_id
 * @property string $cod_barra
 * @property string $cod_interno
 * @property string $setor_secao
 * @property string $descricao_setor_secao
 * @property string $grupo
 * @property string $familia
 * @property string $subfamilia
 * @property string $referencia
 * @property double $saldo_estoque
 * @property double $valor_custo
 * @property double $valor_venda
 * @property string $descricao_item
 *
 * @property Inventario $inventario
 * @property Confronto[] $confrontos
 * @property Divergencia[] $divergencias
 */
class Base extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inventario_id'], 'required'],
            [['inventario_id'], 'integer'],
            [['saldo_estoque', 'valor_custo', 'valor_venda'], 'number'],
            [['cod_barra', 'cod_interno', 'setor_secao', 'descricao_setor_secao', 'grupo', 'familia', 'subfamilia', 'referencia', 'descricao_item'], 'string', 'max' => 45],
            [['inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventario::className(), 'targetAttribute' => ['inventario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventario_id' => 'Inventario ID',
            'cod_barra' => 'Cod Barra',
            'cod_interno' => 'Cod Interno',
            'setor_secao' => 'Setor Secao',
            'descricao_setor_secao' => 'Descricao Setor Secao',
            'grupo' => 'Grupo',
            'familia' => 'Familia',
            'subfamilia' => 'Subfamilia',
            'referencia' => 'Referencia',
            'saldo_estoque' => 'Saldo Estoque',
            'valor_custo' => 'Valor Custo',
            'valor_venda' => 'Valor Venda',
            'descricao_item' => 'Descricao Item',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return $this->hasOne(Inventario::className(), ['id' => 'inventario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfrontos()
    {
        return $this->hasMany(Confronto::className(), ['base_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivergencias()
    {
        return $this->hasMany(Divergencia::className(), ['base_id' => 'id']);
    }
}
