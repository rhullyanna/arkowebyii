<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "login".
 *
 * @property int $id
 * @property string $username
 * @property string $cpf
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property int $usuario_id
 * @property string $login_status
 *
 * @property Usuario $usuario
 */
class Loginarko extends \yii\db\ActiveRecord
{

    public $status_ativo = "ATIVO";
    public $status_inativo = "INATIVO";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['authKey', 'accessToken', 'login_status'], 'string'],
            [['cpf','username','password','login_status'], 'required'],
            [['usuario_id'], 'integer'],
            [['username', 'cpf', 'password'], 'string', 'max' => 45],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Nome de Usuário',
            'cpf' => 'CPF',
            'password' => 'Senha',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'usuario_id' => 'Usuário',
            'login_status' => 'Status do Login',
        ];
    }

    public static function getUsuarioPorCpf($cpf){
        $usuario = Loginarko::find()->where(['cpf'=>$cpf])->one();
        return $usuario;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
