<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario_enderecamento".
 *
 * @property int $id
 * @property int $inventario_id
 * @property int $usuario_id
 * @property int $enderecamento_id
 *
 * @property Enderecamento $enderecamento
 * @property Inventario $inventario
 * @property Usuario $usuario
 */
class Usuarioenderecamento extends \yii\db\ActiveRecord
{

    public $inicio;
    public $fim;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_enderecamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inventario_id'], 'required'],
            [['inventario_id', 'usuario_id', 'enderecamento_id','inicio','fim'], 'integer'],
            [['enderecamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Enderecamento::className(), 'targetAttribute' => ['enderecamento_id' => 'id']],
            [['inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventario::className(), 'targetAttribute' => ['inventario_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventario_id' => 'Inventario ID',
            'usuario_id' => 'Usuario ID',
            'enderecamento_id' => 'Enderecamento ID',
            'inicio' => 'End. Inicial',
            'fim'=>'End. Final'
        ];
    }

    public static function getUsuariosEnderecamentoInicioFim($inventario_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
        select distinct 
        inv.numero_inventario as inventario, 
        usu.nome as usuario, 
        endereco.descricao as enderecamento,
        usu_end.id as usuarioenderecamento_id,
        usu_end.inventario_id as inventario_id,
        usu_end.usuario_id as usuario_id,
        usu_end.enderecamento_id as enderecamento_id
        from usuario_enderecamento as usu_end
        inner join inventario as inv on usu_end.inventario_id = inv.id
        inner join usuario as usu on usu_end.usuario_id = usu.id
        inner join enderecamento as endereco on usu_end.enderecamento_id = endereco.id
        where usu_end.inventario_id = {$inventario_id}
        order by usu_end.enderecamento_id"
        );

        $dados = $sql->queryAll();

        $enderecamentos_por_usuario = [];
        $enderecamentos = [];

        // percorre todo o resultado e separa em arrays por sequencia de inventariante
        foreach($dados as $usu_end){
            
            $end = new Usuarioenderecamento();
            $end->attributes = $usu_end;
            $end->id = $usu_end['enderecamento_id'];
            if($enderecamentos){
                if($end->usuario_id == $enderecamentos[count($enderecamentos)-1]->usuario_id){     
                    $sequencial_anterior = Enderecamento::findOne($enderecamentos[ count($enderecamentos)-1 ]->enderecamento_id);
                    $sequencial_anterior = intval(explode('-',$sequencial_anterior->descricao)[1]);
                    $sequencial_atual = intval(explode('-',$usu_end['enderecamento'])[1]);
                    if($sequencial_atual != $sequencial_anterior+1){
                        array_push($enderecamentos_por_usuario,$enderecamentos);      
                        $enderecamentos = [];       
                        array_push($enderecamentos,$end);     
                    }
                    // echo "<pre>"; print_r($usu_end); die;
                    array_push($enderecamentos,$end);
                }else{      
                    array_push($enderecamentos_por_usuario,$enderecamentos);      
                    $enderecamentos = [];       
                    array_push($enderecamentos,$end);       
                }
            }else{       
                array_push($enderecamentos,$end);     
            }
        }    
        array_push($enderecamentos_por_usuario,$enderecamentos);    
        $enderecamentos = [];

        // percorre os arrays de sequencia de inventariante e guarda o enderecamento inicial e final de cada sequencia de inventariante
        if($enderecamentos_por_usuario){
            foreach($enderecamentos_por_usuario as $enderecos){
                if($enderecos){
                    $end = $enderecos[0];
                    $end->inicio = $end->enderecamento_id;
                    $end->fim = $enderecos[count($enderecos)-1]->enderecamento_id;
                    array_push($enderecamentos,$end);
                }
                
            }
        }
        
        // foreach($enderecamentos as $end){
        //     echo "<pre>"; print_r($end->attributes); 
        //     echo "<pre>"; print_r($end->inicio); 
        //     echo "<pre>"; print_r($end->fim); 
        // }
        // die;

        return $enderecamentos;
    }

    // Se retornar "false" é porque um dos endereçamentos ja está associado a um usuario da equipe
    public static function associacaoValida($inventario, $valor_inicio, $valor_fim){

        $valores_intervalo_enderecamentos = [];
        for($i = intval($valor_inicio); $i <= intval($valor_fim); $i++){
            array_push($valores_intervalo_enderecamentos,$i);
        }

        $all_usu_ends = Usuarioenderecamento::find()->where(['inventario_id'=>$inventario->id])->all();

        if($all_usu_ends){
            foreach($all_usu_ends as $usu_end){

                $enderecamento = Enderecamento::find()->where(['id'=>$usu_end->enderecamento_id])->one();
                $valor_enderecamento_numero = intval(explode('-',$enderecamento->descricao)[1]);

                if(in_array($valor_enderecamento_numero,$valores_intervalo_enderecamentos)){
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnderecamento()
    {
        return $this->hasOne(Enderecamento::className(), ['id' => 'enderecamento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return $this->hasOne(Inventario::className(), ['id' => 'inventario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
