<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enderecamento".
 *
 * @property int $id
 * @property int $inventario_id
 * @property string $descricao
 * @property string $descricao_proprietario
 * @property string $excecao
 *
 * @property Inventario $inventario
 * @property UsuarioEnderecamento[] $usuarioEnderecamentos
 */
class Enderecamento extends \yii\db\ActiveRecord
{

    public $prefixo;
    public $inicio;
    public $fim;
    public $arquivo;
    public $nome_coluna_enderecamento;

    const EXCECAO_SIM = 'SIM';
    const EXCECAO_NAO = 'NAO';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enderecamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['inventario_id','prefixo','inicio','fim'], 'required'],
            [['inventario_id','descricao'], 'required'],
            [['inicio','fim'],'integer'],
            [['inventario_id'], 'integer'],
            [['arquivo'], 'file'],
            [['descricao','excecao','prefixo','nome_coluna_enderecamento','descricao_proprietario'], 'string', 'max' => 100],
            [['inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventario::className(), 'targetAttribute' => ['inventario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventario_id' => 'Inventário',
            'descricao' => 'Descrição',
            'descricao_proprietario'=>'Descrição do Proprietário',
            'nome_coluna_enderecamento' => 'Nome da coluna do Endereçamento',
            'arquivo'=>'Arquivo a ser importado (*.csv)',
            'excecao' => 'Exceção'
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if( ($this->excecao == "") || ($this->excecao == null)){
                $this->excecao = Enderecamento::EXCECAO_NAO;
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getListaSequenciaisPorInventario($inventario_id){
        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$inventario_id])->all();
        $sequenciais = [];
        if($enderecamentos){
            foreach($enderecamentos as $end){
                array_push($sequenciais, ['descricao'=>$end->descricao, 'descricao_proprietario'=>$end->descricao_proprietario]);
            }
        }
        
        return $sequenciais;
    }

    public static function getPrefixo($end){
        $prefixo = explode("-",$end->descricao)[0];
        return $prefixo;
    }

    public static function getEnderecamentos($inventario_id){
        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$inventario_id])->all();

        return $enderecamentos;
    }

    public static function getEnderecamentosPrimeiroEUltimo($inventario_id){
        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$inventario_id])->all();
        $end_aux = [];
        if($enderecamentos){
            array_push($end_aux, $enderecamentos[0]);
            array_push($end_aux, $enderecamentos[ count($enderecamentos)-1 ]);
        }
    
        return $end_aux;
    }

    public static function getUltimoSequencial($enderecamento){
        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$enderecamento->inventario_id])->all();
        $end = new Enderecamento();
        $sequencial = 1;
        if($enderecamentos){
            foreach($enderecamentos as $enderecamento){
                $end = $enderecamento;    
            }

            $seq_aux = explode('-',$end->descricao)[1];
            $sequencial = intval($seq_aux)+1;
        }
        return $sequencial;
    }

    public static function montaListaEnderecamentosSequencial($enderecamento){
        $array_descricoes = [];

        $enderecamento->prefixo = strtoupper($enderecamento->prefixo);
        for($i = $enderecamento->inicio; $i <= $enderecamento->fim; $i++){
            if($i < 10){
                $descricao = $enderecamento->prefixo.'-'.'0'.$i;    
            }else{
                $descricao = $enderecamento->prefixo.'-'.$i;
            }
            array_push($array_descricoes,$descricao);
        }
        return $array_descricoes;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return $this->hasOne(Inventario::className(), ['id' => 'inventario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioEnderecamentos()
    {
        return $this->hasMany(UsuarioEnderecamento::className(), ['enderecamento_id' => 'id']);
    }

    public static function verificaUltimoEnderecamento($inventario){
        $all_enderecamentos = Enderecamento::find()->where(['inventario_id'=>$inventario->id])->all();

        if($all_enderecamentos){
            
            $ultimo_end = $all_enderecamentos[count($all_enderecamentos) - 1];
            $ultimo_end_numerico = explode('-',$ultimo_end->descricao)[1];
            
            return intval($ultimo_end_numerico);
        }
        return 0;

    }

}
