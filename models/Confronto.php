<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "confronto".
 *
 * @property int $id
 * @property int $inventario_id
 * @property int $base_id
 * @property int $coleta_id
 * @property string $cod_barra
 * @property string $cod_interno
 * @property string $descricao_item
 * @property double $valor_custo
 * @property int $saldo_estoque
 * @property int $qtd_inventario
 * @property int $qtd_divergencia
 * @property double $valor_inventario
 * @property double $valor_saldo_estoque
 * @property double $valor_divergente
 *
 * @property Base $base
 * @property Coleta $coleta
 * @property Inventario $inventario
 * @property Divergencia[] $divergencias
 */
class Confronto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'confronto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inventario_id', 'base_id', 'coleta_id'], 'required'],
            [['inventario_id', 'base_id', 'coleta_id', 'saldo_estoque', 'qtd_inventario', 'qtd_divergencia'], 'integer'],
            [['valor_custo', 'valor_inventario', 'valor_saldo_estoque', 'valor_divergente'], 'number'],
            [['cod_barra', 'cod_interno', 'descricao_item'], 'string', 'max' => 45],
            [['base_id'], 'exist', 'skipOnError' => true, 'targetClass' => Base::className(), 'targetAttribute' => ['base_id' => 'id']],
            [['coleta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coleta::className(), 'targetAttribute' => ['coleta_id' => 'id']],
            [['inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventario::className(), 'targetAttribute' => ['inventario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventario_id' => 'Inventario ID',
            'base_id' => 'Base ID',
            'coleta_id' => 'Coleta ID',
            'cod_barra' => 'Cod Barra',
            'cod_interno' => 'Cod Interno',
            'descricao_item' => 'Descricao Item',
            'valor_custo' => 'Valor Custo',
            'saldo_estoque' => 'Saldo Estoque',
            'qtd_inventario' => 'Qtd Inventario',
            'qtd_divergencia' => 'Qtd Divergencia',
            'valor_inventario' => 'Valor Inventario',
            'valor_saldo_estoque' => 'Valor Saldo Estoque',
            'valor_divergente' => 'Valor Divergente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBase()
    {
        return $this->hasOne(Base::className(), ['id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColeta()
    {
        return $this->hasOne(Coleta::className(), ['id' => 'coleta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return $this->hasOne(Inventario::className(), ['id' => 'inventario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivergencias()
    {
        return $this->hasMany(Divergencia::className(), ['confronto_id' => 'id']);
    }
}
