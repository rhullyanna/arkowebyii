<?php

namespace app\models;

use Yii;
use app\models\Usuario;

/**
 * This is the model class for table "usuario_inventario".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $inventario_id
 */
class Usuarioinventario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_inventario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'inventario_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'inventario_id' => 'Inventario ID',
        ];
    }

    public function equipeDoInventario($invid){
        $equipe_aux = Usuarioinventario::find()->where(['inventario_id'=>$invid])->all();
        $equipe_de_inventariantes = [];

        foreach($equipe_aux as $inventariante){
            $usuario = Usuario::find()->where(['id'=>$inventariante->usuario_id])->one();
            array_push($equipe_de_inventariantes, $usuario);
        }

        return $equipe_de_inventariantes;
    }
}
