<?php

namespace app\models;

use Yii;
use yii\helpers\Time;

/**
 * This is the model class for table "log_historico".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $acao_realizada
 * @property string $modulo
 * @property string $data
 * @property string $observacao
 *
 * @property Usuario $usuario
 */
class Loghistorico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_historico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id'], 'required'],
            [['usuario_id'], 'integer'],
            [['observacao'], 'string'],
            [['acao_realizada', 'modulo', 'data'], 'string', 'max' => 45],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'acao_realizada' => 'Acao Realizada',
            'modulo' => 'Modulo',
            'data' => 'Data',
            'observacao' => 'Observacao',
        ];
    }

    public static function getTime(){
        $time = new \DateTime('now', new \DateTimeZone('Brazil/West'));
        return $time->format('d/m/Y H:i:s');
    }

    public static function salvarLogHistorico(
        $id_usuario,
        $acao_realizada,
        $modulo,
        $observacao
    ){
        $log = new Loghistorico();
        $log->usuario_id = $id_usuario;
        $log->acao_realizada = strtoupper($acao_realizada);
        $log->modulo = strtoupper($modulo);
        // $log->data = date('d/m/Y h:i:s');
        $log->data = Loghistorico::getTime();
        $log->observacao = strtoupper($observacao);
        
        if($log->save()){
            return true;
        }
        return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
