<?php

namespace app\models;

use Yii;
use yiibr\brvalidator\CpfValidator;

/**
 * This is the model class for table "responsavel".
 *
 * @property int $id
 * @property string $nome
 * @property string $rg
 * @property string $cpf
 * @property string $nascimento
 * @property string $sexo
 * @property string $telefone
 * @property string $email
 * @property string $cargo
 *
 * @property Grupo[] $grupos
 * @property Loja[] $lojas
 */
class Responsavel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'responsavel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome','cpf','telefone','cargo','email','sexo'],'required','message'=>'campo não pode ser vazio'],
            [['sexo'], 'string'],
            [['cpf'],'unique','message'=>'Documento ja cadastrado'],
            ['cpf', CpfValidator::className(),'message'=>'Documento inválido'],
            [['nome', 'email'], 'string', 'max' => 200],
            [['rg', 'cpf', 'nascimento', 'telefone', 'cargo'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'rg' => 'Rg',
            'cpf' => 'Cpf',
            'nascimento' => 'Nascimento',
            'sexo' => 'Sexo',
            'telefone' => 'Telefone',
            'email' => 'Email',
            'cargo' => 'Cargo',
        ];
    }

    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->nascimento = Usuario::dateTodateBR($this->nascimento);

            return true;
        } else {
            return false;
        }
    }

    public function afterFind(){

        parent::afterFind();
    
        if($this->nascimento){
            $this->nascimento = Usuario::dateBrParaDateString($this->nascimento);
        }
    }

    public static function getResponsavelPorCpf($cpf){
        $result = Responsavel::find()->where(['cpf'=>$cpf])->one();
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupo::className(), ['responsavel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLojas()
    {
        return $this->hasMany(Loja::className(), ['responsavel_id' => 'id']);
    }
}
