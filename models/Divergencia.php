<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "divergencia".
 *
 * @property int $id
 * @property int $confronto_id
 * @property int $inventario_id
 * @property int $base_id
 * @property int $coleta_id
 * @property string $enderecamento
 * @property string $cod_barra
 * @property string $cod_interno
 * @property string $descricao_item
 * @property double $confronto_valor_custo
 * @property double $confronto_saldo_estoque
 * @property int $confronto_qtd_inventario
 * @property int $confronto_qtd_divergencia
 * @property double $confronto_valor_inventario
 * @property double $confronto_valor_saldo_estoque
 * @property double $confronto_valor_divergente
 *
 * @property AuditoriaDivergencia[] $auditoriaDivergencias
 * @property Base $base
 * @property Coleta $coleta
 * @property Confronto $confronto
 * @property Inventario $inventario
 */
class Divergencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'divergencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['confronto_id', 'inventario_id', 'base_id', 'coleta_id'], 'required'],
            [['confronto_id', 'inventario_id', 'base_id', 'coleta_id', 'confronto_qtd_inventario', 'confronto_qtd_divergencia'], 'integer'],
            [['confronto_valor_custo', 'confronto_saldo_estoque', 'confronto_valor_inventario', 'confronto_valor_saldo_estoque', 'confronto_valor_divergente'], 'number'],
            [['enderecamento', 'cod_barra', 'cod_interno', 'descricao_item'], 'string', 'max' => 45],
            [['base_id'], 'exist', 'skipOnError' => true, 'targetClass' => Base::className(), 'targetAttribute' => ['base_id' => 'id']],
            [['coleta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coleta::className(), 'targetAttribute' => ['coleta_id' => 'id']],
            [['confronto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Confronto::className(), 'targetAttribute' => ['confronto_id' => 'id']],
            [['inventario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventario::className(), 'targetAttribute' => ['inventario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'confronto_id' => 'Confronto ID',
            'inventario_id' => 'Inventario ID',
            'base_id' => 'Base ID',
            'coleta_id' => 'Coleta ID',
            'enderecamento' => 'Enderecamento',
            'cod_barra' => 'Cod Barra',
            'cod_interno' => 'Cod Interno',
            'descricao_item' => 'Descricao Item',
            'confronto_valor_custo' => 'Confronto Valor Custo',
            'confronto_saldo_estoque' => 'Confronto Saldo Estoque',
            'confronto_qtd_inventario' => 'Confronto Qtd Inventario',
            'confronto_qtd_divergencia' => 'Confronto Qtd Divergencia',
            'confronto_valor_inventario' => 'Confronto Valor Inventario',
            'confronto_valor_saldo_estoque' => 'Confronto Valor Saldo Estoque',
            'confronto_valor_divergente' => 'Confronto Valor Divergente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaDivergencias()
    {
        return $this->hasMany(AuditoriaDivergencia::className(), ['divergencia_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBase()
    {
        return $this->hasOne(Base::className(), ['id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColeta()
    {
        return $this->hasOne(Coleta::className(), ['id' => 'coleta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfronto()
    {
        return $this->hasOne(Confronto::className(), ['id' => 'confronto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return $this->hasOne(Inventario::className(), ['id' => 'inventario_id']);
    }
}
