<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Agendamento;

/**
 * AgendamentoSearch represents the model behind the search form of `app\models\Agendamento`.
 */
class AgendamentoSearch extends Agendamento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'contrato_id', 'usuario_id'], 'integer'],
            [['data_agendamento', 'hora_agendamento', 'agendamento_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agendamento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contrato_id' => $this->contrato_id,
            'usuario_id' => $this->usuario_id,
        ]);

        $query->andFilterWhere(['like', 'data_agendamento', $this->data_agendamento])
            ->andFilterWhere(['like', 'hora_agendamento', $this->hora_agendamento])
            ->andFilterWhere(['like', 'agendamento_status', $this->agendamento_status]);

        return $dataProvider;
    }
}
