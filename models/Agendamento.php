<?php

namespace app\models;

use Yii;
use app\models\Contrato;

/**
 * This is the model class for table "agendamento".
 *
 * @property int $id
 * @property int $contrato_id
 * @property string $data_agendamento
 * @property string $hora_agendamento
 * @property int $usuario_id
 * @property string $agendamento_status
 *
 * @property Contrato $contrato
 * @property Usuario $usuario
 * @property Inventario[] $inventarios
 */
class Agendamento extends \yii\db\ActiveRecord
{

    CONST STATUS_PRE_AGENDADO   = "PRE AGENDADO";
    CONST STATUS_AGUARDANDO     = "AGUARDANDO";
    CONST STATUS_EM_ANDAMENTO   = "EM ANDAMENTO";
    CONST STATUS_CONCLUIDO      = "CONCLUIDO";
    CONST STATUS_ATRASADO       = "ATRASADO";
    CONST STATUS_CANCELADO      = "CANCELADO";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agendamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contrato_id', 'usuario_id'], 'required'],
            [['contrato_id', 'usuario_id'], 'integer'],
            [['agendamento_status'], 'string'],
            [['data_agendamento', 'hora_agendamento'], 'string', 'max' => 45],
            [['contrato_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contrato::className(), 'targetAttribute' => ['contrato_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contrato_id' => 'Contrato',
            'data_agendamento' => 'Data Agendamento',
            'hora_agendamento' => 'Hora Agendamento',
            'usuario_id' => 'Coordenador',
            'agendamento_status' => 'Status',
        ];
    }

    public static function preAgendamentoAutomaticoDeInventario($contrato_id, $data_agendamento){
        $agendamento = new Agendamento();
        $agendamento->contrato_id = $contrato_id;
        $agendamento->data_agendamento = $data_agendamento;
        $agendamento->hora_agendamento = "09:00";
        $agendamento->agendamento_status = Agendamento::STATUS_PRE_AGENDADO;
        // no pre agendamento, o campo coordenador fica associado ao usuário que cadastrou o contrato, independente do seu cargo.
        // no agendamento, ele ira fazer a alteração para o coordenador de sua preferencia
        $agendamento->usuario_id = Yii::$app->user->identity->usuario_id;
        return $agendamento;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->data_agendamento = Contrato::dateTodateBR($this->data_agendamento);

            return true;
        } else {
            return false;
        }
    }

    public static function agendamentos_do_mes()
    {
        
        $dt_atual = date('d/m/Y');
        $dt_consulta = explode('/',$dt_atual);
        $mes = $dt_consulta[1];
        $ano = $dt_consulta[2];
        $dt_consulta = $mes.'/'.$ano;

        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            SELECT l.nome_fantasia as loja ,a.data_agendamento as data, a.agendamento_status as status
            from agendamento as a
            inner join contrato as c
            inner join loja l
            where 
            a.data_agendamento like '%{$dt_consulta}' and
            a.agendamento_status <> 'CONCLUIDO' and
            c.id = a.contrato_id and
            l.id = c.loja_id
            order by a.data_agendamento
            limit 10
        ");

        $dados = $sql->queryAll();

        $agendamentos_a_mostrar = [];
        foreach ($dados as $value) {
            if($value['data'] >= $dt_atual){
                array_push($agendamentos_a_mostrar, $value);
            }
        }

        if(count($agendamentos_a_mostrar) < 10){
            $mes_aux = $mes + 1;
            $ano_aux = $ano;
            $connection = Yii::$app->getDb();
            if($mes == '12'){
                $mes_aux = '1';
                $ano_aux = $ano + 1;
            }
            if($mes_aux < 10){
                $mes_aux = '0'.$mes_aux;
            }

            $dt_consulta_aux = $mes_aux.'/'.$ano_aux;
            $limite = 10 - count($agendamentos_a_mostrar);
            $sql = $connection->createCommand("
                SELECT l.nome_fantasia as loja ,a.data_agendamento as data, a.agendamento_status as status
                from agendamento as a
                inner join contrato as c
                inner join loja l
                where 
                a.data_agendamento like '%{$dt_consulta_aux}' and
                a.agendamento_status <> 'CONCLUIDO' and
                c.id = a.contrato_id and
                l.id = c.loja_id
                order by a.data_agendamento
                limit {$limite}
            ");

            $dados = $sql->queryAll();
            
            foreach ($dados as $value) {
                array_push($agendamentos_a_mostrar, $value);
            }
        }

        return $agendamentos_a_mostrar;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContrato()
    {
        return $this->hasOne(Contrato::className(), ['id' => 'contrato_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventarios()
    {
        return $this->hasMany(Inventario::className(), ['agendamento_id' => 'id']);
    }
}
