<?php

namespace app\models;

use Yii;
use app\models\Agendamento;
use yiibr\brvalidator\CnpjValidator;


/**
 * This is the model class for table "loja".
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj
 * @property string $inscricao_municipal
 * @property string $endereco
 * @property string $endereco_numero
 * @property string $complemento
 * @property int $bairro_id
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $cep
 * @property string $telefone
 * @property int $grupo_id
 * @property string $status
 * @property int $responsavel_id
 * @property string $latitude
 * @property string $longitude
 *
 * @property Contrato[] $contratos
 * @property Bairro $bairro
 * @property Cidade $cidade
 * @property Estado $estado
 * @property Grupo $grupo
 * @property Responsavel $responsavel
 */
class Loja extends \yii\db\ActiveRecord
{
    CONST STATUS_ATIVO = 'ATIVO';
    CONST STATUS_INATIVO = 'INATIVO';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bairro_id', 'estado_id', 'cidade_id', 'grupo_id', 'responsavel_id','status','cnpj','inscricao_municipal'], 'required', 'message'=>'Campo não pode ser vazio'],
            [['bairro_id', 'estado_id', 'cidade_id', 'grupo_id', 'responsavel_id'], 'integer'],
            [['status'], 'string'],
            [['cnpj'],'unique','message'=>'Documento ja cadastrado'],
            ['cnpj', CnpjValidator::className(),'message'=>'Documento inválido'],
            [['razao_social'], 'string', 'max' => 200],
            [['nome_fantasia', 'inscricao_municipal'], 'string', 'max' => 100],
            [['cnpj', 'endereco_numero', 'complemento', 'cep', 'telefone', 'latitude', 'longitude'], 'string', 'max' => 45],
            [['endereco'], 'string', 'max' => 150],
            [['bairro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bairro::className(), 'targetAttribute' => ['bairro_id' => 'id']],
            [['cidade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cidade::className(), 'targetAttribute' => ['cidade_id' => 'id']],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['grupo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grupo::className(), 'targetAttribute' => ['grupo_id' => 'id']],
            [['responsavel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Responsavel::className(), 'targetAttribute' => ['responsavel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'razao_social' => 'Razao Social',
            'nome_fantasia' => 'Nome Fantasia',
            'cnpj' => 'Cnpj',
            'inscricao_municipal' => 'Inscricao Municipal',
            'endereco' => 'Endereco',
            'endereco_numero' => 'Endereco Numero',
            'complemento' => 'Complemento',
            'bairro_id' => 'Bairro',
            'estado_id' => 'Estado',
            'cidade_id' => 'Cidade',
            'cep' => 'Cep',
            'telefone' => 'Telefone',
            'grupo_id' => 'Grupo',
            'status' => 'Status',
            'responsavel_id' => 'Responsavel',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    public static function getInventarios($id){
        $inventarios = [];
        $inventarios_aux = Inventario::find()->where(['LIKE','numero_inventario','L'.$id.'-'])->all();
        foreach( $inventarios_aux as $inventario){
            array_push($inventarios, ["id" => $inventario->id , "numero_inventario" => $inventario->numero_inventario]);
        }
        return $inventarios;
    }

    public static function getLojasComContratosConcluidos(){
        $resultado = [];
        $lojas = Loja::find()->where(['status'=>Loja::STATUS_ATIVO])->all();
        
        foreach ($lojas as $loja) {
            $tem_contrato_ativo = false;
            $contratos_concluidos = 
                Contrato::find()
                ->where(['loja_id'=>$loja->id])
                ->andWhere(['not in','contrato_status',[
                    Contrato::STATUS_ABERTO,
                    Contrato::STATUS_EM_ANDAMENTO,
                    // Contrato::STATUS_VENCIDO,
                    Contrato::STATUS_CANCELADO,
                ]])
                ->all();
            if($contratos_concluidos){
                foreach ($contratos_concluidos as $contrato) {
                    $agendamentos = 
                        Agendamento::find()
                        ->where(['contrato_id'=>$contrato->id])
                        ->andWhere(['agendamento_status'=>Agendamento::STATUS_CONCLUIDO])
                        ->all();
                }
            }
            echo "<pre>"; print_r($agendamentos); die;
        }
        
    }

    public static function getPositionLojas(){
        $lojas = [];
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select 
                nome_fantasia,latitude,longitude from loja
            where 
                latitude != ''
            and 
                longitude != ''
            ");
        $dados = $sql->queryAll();
        foreach($dados as $value){
            array_push($lojas,[$value['nome_fantasia'],$value['latitude'],$value['longitude']]);
            
        }
        
        return $lojas;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contrato::className(), ['loja_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBairro()
    {
        return $this->hasOne(Bairro::className(), ['id' => 'bairro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCidade()
    {
        return $this->hasOne(Cidade::className(), ['id' => 'cidade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupo()
    {
        return $this->hasOne(Grupo::className(), ['id' => 'grupo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsavel()
    {
        return $this->hasOne(Responsavel::className(), ['id' => 'responsavel_id']);
    }
}
