<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria_divergencia".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $divergencia_id
 * @property string $data
 * @property string $hora
 * @property string $enderecamento
 * @property string $cod_barra
 * @property string $validade
 * @property string $fabricacao
 * @property string $lote
 * @property int $itens_embalagem
 * @property string $marca
 * @property string $fornecedor
 * @property string $interna
 * @property string $externa
 *
 * @property Divergencia $divergencia
 * @property Usuario $usuario
 * @property EquipeAuditoria[] $equipeAuditorias
 */
class AuditoriaDivergencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auditoria_divergencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'divergencia_id'], 'required'],
            [['usuario_id', 'divergencia_id', 'itens_embalagem'], 'integer'],
            [['interna', 'externa'], 'string'],
            [['data', 'hora', 'enderecamento', 'cod_barra', 'validade', 'fabricacao', 'lote', 'marca', 'fornecedor'], 'string', 'max' => 45],
            [['divergencia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Divergencia::className(), 'targetAttribute' => ['divergencia_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'divergencia_id' => 'Divergencia ID',
            'data' => 'Data',
            'hora' => 'Hora',
            'enderecamento' => 'Enderecamento',
            'cod_barra' => 'Cod Barra',
            'validade' => 'Validade',
            'fabricacao' => 'Fabricacao',
            'lote' => 'Lote',
            'itens_embalagem' => 'Itens Embalagem',
            'marca' => 'Marca',
            'fornecedor' => 'Fornecedor',
            'interna' => 'Interna',
            'externa' => 'Externa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivergencia()
    {
        return $this->hasOne(Divergencia::className(), ['id' => 'divergencia_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipeAuditorias()
    {
        return $this->hasMany(EquipeAuditoria::className(), ['auditoria_divergencia_id' => 'id']);
    }
}
