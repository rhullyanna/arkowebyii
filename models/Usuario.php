<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yiibr\brvalidator\CpfValidator;
use yiibr\brvalidator\CnpjValidator;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $username
 * @property string $nome
 * @property string $cpf
 * @property string $cnpj
 * @property string $rg
 * @property string $nascimento
 * @property string $pcd
 * @property string $telefone
 * @property string $endereco
 * @property string $endereco_numero
 * @property int $bairro_id
 * @property string $cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $complemento
 * @property string $email
 * @property string $cargo
 * @property int $regime_id
 * @property string $sexo
 * @property string $foto
 *
 * @property Agendamento[] $agendamentos
 * @property AuditoriaDivergencia[] $auditoriaDivergencias
 * @property Coleta[] $coletas
 * @property Equipe[] $equipes
 * @property EquipeAuditoria[] $equipeAuditorias
 * @property Inventario[] $inventarios
 * @property Inventario[] $inventarios0
 * @property Login[] $logins
 * @property Bairro $bairro
 * @property Cidade $cidade
 * @property Estado $estado
 * @property Regime $regime
 * @property UsuarioEnderecamento[] $usuarioEnderecamentos
 */
class Usuario extends \yii\db\ActiveRecord
{

    const COORDENADOR = 'COORDENADOR';
    CONST GESTOR = 'GESTOR';
    CONST GERENTE = 'GERENTE';
    CONST INVENTARIANTE = 'INVENTARIANTE';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'nome','rg','sexo','bairro_id','cidade_id','estado_id','nascimento','pcd','telefone','regime_id','cargo'], 'required' , 'message'=>'Campo não pode ser vazio.'],
            [[ 'cpf','cnpj','rg'], 'unique' , 'message'=>'Documento já cadastrado'],
            ['cpf', CpfValidator::className(),'message'=>'Documento inválido'],
            // cnpj validator
            ['cnpj', CnpjValidator::className(),'message'=>'Documento inválido'],
            [['pcd', 'cargo', 'sexo'], 'string'],
            [['bairro_id', 'estado_id', 'cidade_id', 'regime_id'], 'integer'],
            [['username', 'cpf', 'cnpj', 'rg', 'nascimento', 'telefone', 'cep'], 'string', 'max' => 45],
            [['nome', 'endereco'], 'string', 'max' => 200],
            [['endereco_numero'], 'string', 'max' => 20],
            [['complemento'], 'string', 'max' => 300],
            [['email'], 'string', 'max' => 150],
            [['foto'],'file'],
            [['bairro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bairro::className(), 'targetAttribute' => ['bairro_id' => 'id']],
            [['cidade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cidade::className(), 'targetAttribute' => ['cidade_id' => 'id']],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['regime_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regime::className(), 'targetAttribute' => ['regime_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            //'username' => 'Username',
            'nome' => 'Nome',
            'cpf' => 'CPF',
            'cnpj' => 'CNPJ',
            'rg' => 'RG',
            'nascimento' => 'Data de Nascimento',
            'pcd' => 'PCD',
            'telefone' => 'Telefone',
            'endereco' => 'Endereço',
            'endereco_numero' => 'Número Residência',
            'bairro_id' => 'Bairro',
            'cep' => 'CEP',
            'estado_id' => 'Estado',
            'cidade_id' => 'Cidade',
            'complemento' => 'Complemento',
            'email' => 'E-mail',
            'cargo' => 'Cargo',
            'regime_id' => 'Regime',
            'sexo' => 'Sexo',
            'foto' => 'Foto',
        ];
    }

    public static function dateTodateBR($data){
        $data = explode("-",$data);
        $data = $data[2]."/".$data[1]."/".$data[0];
        return $data;
    }

    public static function dateBrParaDateString($data){
        $data = explode("/",$data);
        $data = $data[2]."-".$data[1]."-".$data[0];
        return $data;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->nascimento = Usuario::dateTodateBR($this->nascimento);

            return true;
        } else {
            return false;
        }
    }

    // public static function emailUsuario($type = "teste", $data = null){
    //     Yii::$app->mailer->compose($type, ['data' => $data])
    //         ->setFrom($this->from)
    //         ->setTo($this->to)
    //         ->setSubject($this->subjects[$type])
    //         ->send();
    // }

    public static function emailUsuario($arquivo,$titulo,$mensagem,$remetente,$destinatario,$assunto){
        if(!$destinatario){
            $destinatario = "sistemainventario@arkosolucoes.com.br";
        }
        
        // Yii::$app->mailer->compose("@app/mail/layouts/".$arquivo, ["content" => $mensagem,"titulo"=>$titulo])
        //  ->setFrom($remetente)
        //  ->setTo($destinatario)
        //  ->setSubject($assunto)
        //  ->send();
        return null;
    }

    public function afterFind(){

        parent::afterFind();
    
        if($this->nascimento){
            $this->nascimento = Usuario::dateBrParaDateString($this->nascimento);
        }
    }

    public static function getUsuarioPorCpf($cpf){
        $usuario = Usuario::find()->where(['cpf'=>$cpf])->one();
        return $usuario;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendamentos()
    {
        return $this->hasMany(Agendamento::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaDivergencias()
    {
        return $this->hasMany(AuditoriaDivergencia::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColetas()
    {
        return $this->hasMany(Coleta::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipes()
    {
        return $this->hasMany(Equipe::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipeAuditorias()
    {
        return $this->hasMany(EquipeAuditoria::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventarios()
    {
        return $this->hasMany(Inventario::className(), ['usuario_inventariante_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventarios0()
    {
        return $this->hasMany(Inventario::className(), ['usuario_coordenador_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogins()
    {
        return $this->hasMany(Login::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBairro()
    {
        return $this->hasOne(Bairro::className(), ['id' => 'bairro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCidade()
    {
        return $this->hasOne(Cidade::className(), ['id' => 'cidade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegime()
    {
        return $this->hasOne(Regime::className(), ['id' => 'regime_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioEnderecamentos()
    {
        return $this->hasMany(UsuarioEnderecamento::className(), ['usuario_id' => 'id']);
    }
}
