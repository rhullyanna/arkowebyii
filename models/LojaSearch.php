<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loja;

/**
 * LojaSearch represents the model behind the search form of `app\models\Loja`.
 */
class LojaSearch extends Loja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bairro_id', 'estado_id', 'cidade_id', 'grupo_id', 'responsavel_id'], 'integer'],
            [['razao_social', 'nome_fantasia', 'cnpj', 'inscricao_municipal', 'endereco', 'endereco_numero', 'complemento', 'cep', 'telefone', 'status', 'latitude', 'longitude'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bairro_id' => $this->bairro_id,
            'estado_id' => $this->estado_id,
            'cidade_id' => $this->cidade_id,
            'grupo_id' => $this->grupo_id,
            'responsavel_id' => $this->responsavel_id,
        ]);

        $query->andFilterWhere(['like', 'razao_social', $this->razao_social])
            ->andFilterWhere(['like', 'nome_fantasia', $this->nome_fantasia])
            ->andFilterWhere(['like', 'cnpj', $this->cnpj])
            ->andFilterWhere(['like', 'inscricao_municipal', $this->inscricao_municipal])
            ->andFilterWhere(['like', 'endereco', $this->endereco])
            ->andFilterWhere(['like', 'endereco_numero', $this->endereco_numero])
            ->andFilterWhere(['like', 'complemento', $this->complemento])
            ->andFilterWhere(['like', 'cep', $this->cep])
            ->andFilterWhere(['like', 'telefone', $this->telefone])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude]);

        return $dataProvider;
    }
}
