<?php

namespace app\models;

use Yii;
use app\models\Usuario;

/**
 * This is the model class for table "grupo".
 *
 * @property int $id
 * @property string $grupo_nome
 * @property int $responsavel_id
 * @property string $grupo_status
 *
 * @property Responsavel $responsavel
 * @property Loja[] $lojas
 */
class Grupo extends \yii\db\ActiveRecord
{

    const STATUS_ATIVO = "ATIVO";
    const STATUS_INATIVO = "INATIVO";
    // public $responsavel_cpf;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grupo_status','grupo_nome','responsavel_id'], 'required','message'=>'Campo não pode ser vazio'],
            [['responsavel_id'], 'integer'],
            [['grupo_status'], 'string'],
            [['grupo_nome'], 'string', 'max' => 100],
            [['responsavel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Responsavel::className(), 'targetAttribute' => ['responsavel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grupo_nome' => 'Nome do Grupo',
            'responsavel_cpf' => 'CPF do Responsavel',
            'responsavel_id' => 'Responsável',
            'grupo_status' => 'Status do Grupo',
            
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsavel()
    {
        return $this->hasOne(Responsavel::className(), ['id' => 'responsavel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLojas()
    {
        return $this->hasMany(Loja::className(), ['grupo_id' => 'id']);
    }
}
