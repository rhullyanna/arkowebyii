<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Loghistorico;
/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        

        return $this->render('view-novo', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();

        if($model->load(Yii::$app->request->post())){
            $foto = UploadedFile::getInstance($model, 'foto');
            if($foto){
                $model->foto = 'uploads/perfil/' . date("dmY-his") . '.' . $foto->extension;
            }
            if($model->validate()){
                
                if ($model->save()) {
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'cadastrar usuario',
                        'Usuario',
                        "Cadastrou usuario no sistema: ({$model->id}) {$model->nome}"
                    );
                    $corpoEmail = 
                    "
                        <h1>Confirmação de cadastro</h1>
                        <p>Usuario: {$model->nome}</p>
                        <p>CPF: {$model->cpf}</p>
                        <p>Você foi devidamente cadastrado no sistema da Arko Solutions.</p><br>
                    ";
                    // CASO SEJA NECESSÁRIO NOTIFICAÇÃO DE CADASTRO DO USUÁRIO
                    Usuario::emailUsuario(
                        "layoutpadrao",
                        "Confirmação de Cadastro",
                        $corpoEmail,
                        "sistemainventario@arkosolucoes.com.br",
                        $model->email,
                        "Arko Solutions- Confirmação de Cadastro");

                    if(!$foto){
                        return $this->redirect(['view', 'id' => $model->id,'mensagem'=>'sucesso']);
                    }else{
                        if($foto->saveAs($model->foto)){
                            return $this->redirect(['view', 'id' => $model->id,'mensagem'=>'sucesso']);
                        }
                    }
                    
                }
            }
        }
        

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    // function actionEmail(){
    //     $corpoEmail = 
    //     "
    //         <h1>Confirmação de cadastro</h1>
    //         <p>Usuario: cpf </p>
    //         <p>Você foi devidamente cadastrado no sistema da Arko Solutions.</p><br>
    //     ";
    //     Usuario::emailUsuario(
    //         "layoutpadrao",
    //         "Confirmação de Cadastro",
    //         $corpoEmail,
    //         "sistemainventario@arkosolucoes.com.br",
    //         "eduardo.luiz92@gmail.com",
    //         "Arko Solutions- Confirmação de Cadastro");
    // }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $foto_aux = $model->foto;

        if($model->load(Yii::$app->request->post())){
            $foto = UploadedFile::getInstance($model, 'foto');
            if($foto){
                $model->foto = 'uploads/perfil/' . date("dmY-his") . '.' . $foto->extension;
            }else{
                $model->foto = $foto_aux;
            }
            if($model->validate()){
                if ($model->save()) {
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'alterar usuario',
                        'Usuario',
                        "alterou usuario : ({$model->id}) {$model->nome}"
                    );
                    if($foto){
                        if($foto->saveAs($model->foto)){
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }else{
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                    
                }   
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
