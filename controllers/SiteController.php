<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Loghistorico;
use app\models\Loja;
use app\models\Contrato;
use app\models\Usuario;
use app\models\Agendamento;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        if(Yii::$app->user->isGuest){
            return $this->redirect(['login']);
        }

        $contratos_a_vencer = Contrato::verificaContratosPrestesAVencer();
        $proximos_inventarios = Agendamento::agendamentos_do_mes();

        $corpoEmail = 
        "<h1>Contratos prestes a vencer</h1>";
        
        foreach( $contratos_a_vencer as $key => $value){
            $corpoEmail = $corpoEmail
            ."<hr><p>Loja: <b>{$value['loja']}</b></p>"
            ."<p>Num.contrato: <b>{$value['numero']}</b></p>"
            ."<p>Data de vencimento:<b> {$value['vencimento']}</b></p><br>";
        }

        $corpoEmail = $corpoEmail."<p>Os contratos listados acima estão próximos da sua data de vencimento.</p><br>";

        $lojas = Loja::getPositionLojas();
        
        // $recepcionista = Loja::getLojasComContratosConcluidos();

        return $this->render('index',[
            'proximos_inventarios'=>$proximos_inventarios,
            'contratos_a_vencer'=>$contratos_a_vencer,
            'lojas' => json_encode($lojas)
        ]);
    }

    public function actionAplicativo(){
        return $this->redirect(['uploads/aplicativo/app-release.apk']);
    }

    public function actionNotificar(){

        $status = false;
        $mensagem = "Não foi possivel emitir notificações";

        $contratos_a_vencer = Contrato::verificaContratosPrestesAVencer();
        $corpoEmail = 
        "<h1>Contratos prestes a vencer</h1>";
        
        foreach( $contratos_a_vencer as $key => $value){
            $corpoEmail = $corpoEmail
            ."<hr><p>Loja: <b>{$value['loja']}</b></p>"
            ."<p>Num.contrato: <b>{$value['numero']}</b></p>"
            ."<p>Data de vencimento:<b> {$value['vencimento']}</b></p><br>";
        }

        $corpoEmail = $corpoEmail."<p>Os contratos listados acima estão próximos da sua data de vencimento.</p><br>";

        try {

            // DESCOMENTAR PARA ENVIAR EMAIL SEMPRE QUE ACESSAR E TIVER CONTRATOS A VENCER OU BOTÃO PARA NOTIFICAR POR EMAIL
            Usuario::emailUsuario(
                "layoutpadrao",
                "Contratos a vencer",
                $corpoEmail,
                "sistemainventario@arkosolucoes.com.br",
                "sistemainventario@arkosolucoes.com.br",
                "Arko Solutions- Contratos a vencer");
            $status = true;
            $mensagem = "Notificações Enviadas";

            return json_encode(['status'=>$status,'message'=>$mensagem]);

        } catch (\Throwable $th) {
            return json_encode(['status'=>$status,'message'=>$mensagem]);
        }

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $log = Loghistorico::salvarLogHistorico(
                intval(Yii::$app->user->identity->usuario_id),
                'Logar sistema',
                'Login sistema',
                "logou no sistema"
            );
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login-novo', [
            'model' => $model,
        ]);
    }

    public function actionLogindesktop()
    {

        $model = new LoginForm();
        if(Yii::$app->request->get()){
            $model->load(Yii::$app->request->get());
            $model->cpf = Yii::$app->request->get('cpf');
            $model->password = Yii::$app->request->get('password');
            if($model->login()){
                return json_encode(['status'=>'ok','username'=>Yii::$app->user->identity->username]);
            }else{
                return json_encode(['status'=>'acesso negado!']);
            }
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $log = Loghistorico::salvarLogHistorico(
            intval(Yii::$app->user->identity->usuario_id),
            'Deslogar sistema',
            'Login sistema',
            "Deslogou no sistema"
        );
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
