<?php

namespace app\controllers;

use Yii;
use app\models\Agendamento;
use app\models\Contrato;
use app\models\Loghistorico;
use app\models\AgendamentoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgendamentoController implements the CRUD actions for Agendamento model.
 */
class AgendamentoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Agendamento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgendamentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agendamento model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agendamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agendamento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agendamento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionAlteraragendamentosporcontrato($coid){
        $contrato = Contrato::findOne($coid);
        $agendamentos = Agendamento::find()->where(['contrato_id'=>$contrato->id])->all();
        $mensagem = "";

        foreach($agendamentos as $agend){
            $agend->data_agendamento = Contrato::dateBrParaDateString($agend->data_agendamento);
        }

        if(Yii::$app->request->post()){
            $agendamento = $this->findModel(Yii::$app->request->post()['Agendamento']['id']);
            $agendamento->load(Yii::$app->request->post());
            if($agendamento->validate()){
                if($agendamento->save()){
                    $agendamentos = Agendamento::find()->where(['contrato_id'=>$contrato->id])->all();
                    $mensagem = "sucesso";
                    foreach($agendamentos as $agend){
                        $agend->data_agendamento = Contrato::dateBrParaDateString($agend->data_agendamento);
                    }
                }else{
                    echo "<pre>"; print_r(Yii::$app->request->post());die;
                }
            }
        }

        return $this->render('alterar-agendamentos', [
            'contrato' => $contrato,
            'agendamentos' => $agendamentos,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Deletes an existing Agendamento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $agendamento = $this->findModel($id);
        $contrato = Contrato::findOne($agendamento->contrato_id);
        $agendamento_aux = new Agendamento();
        $agendamento_aux->attributes = $agendamento->attributes;
        if($agendamento->delete()){
            $log = Loghistorico::salvarLogHistorico(
                intval(Yii::$app->user->identity->usuario_id),
                'Deletar Agendamento',
                'Agendamento',
                "Deletou agendamento: 
                ({$agendamento->id}) | 
                data: {$agendamento->data_agendamento} | 
                contrato COD: {$contrato->id} |
                contrato NUM: {$contrato->contrato_numero} |"
            );
            return $this->redirect(['alteraragendamentosporcontrato','coid'=>$agendamento_aux->contrato_id]);
        }

        
    }

    /**
     * Finds the Agendamento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agendamento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agendamento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
