<?php

namespace app\controllers;

use Yii;
use app\models\Usuarioenderecamento;
use app\models\Inventario;
use app\models\Loghistorico;
use app\models\Usuario;
use app\models\Enderecamento;
use app\models\UsuarioenderecamentoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioenderecamentoController implements the CRUD actions for Usuarioenderecamento model.
 */
class UsuarioenderecamentoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuarioenderecamento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioenderecamentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTestejson()
    {
        $request = Yii::$app->request;

        $post    = $request->post();

        echo "<pre>"; print_r($post);die;

        if ($request->isAjax && !empty($post['postParameter1'])) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['status' => 'success'];

        }
    }

    /**
     * Displays a single Usuarioenderecamento model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuarioenderecamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($invid)
    {
        $model = new Usuarioenderecamento();
        $inventario = Inventario::findOne($invid);
        $intervalo_de_enderecamentos = Enderecamento::getEnderecamentosPrimeiroEUltimo($inventario->id);
        $prefixo_end = Enderecamento::getPrefixo(Enderecamento::find()->where(['inventario_id'=>$inventario->id])->one());
        $model->inventario_id = $inventario->id;
        $inventarios = Inventario::find()->where(['inventario_status'=>Inventario::STATUS_ATIVO])->orderBy(['numero_inventario'=>SORT_ASC])->all();
        $inventariantes = Usuario::find()->where(['cargo'=>Usuario::INVENTARIANTE])->orderBy(['nome'=>SORT_ASC])->all();
        $all_enderecamentos = Enderecamento::getEnderecamentos($inventario->id);
        $ultimo_enderecamento_numerico = Enderecamento::verificaUltimoEnderecamento($inventario);   
        $mensagem = "";

        $log_inicio = '';
        $log_fim = '';
        $log_usuario_id = '';

        $usuarios_enderecamentos = Usuarioenderecamento::getUsuariosEnderecamentoInicioFim($inventario->id);


        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $lista_usu_enderecamentos = [];

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if(Yii::$app->request->post()['Usuarioenderecamento']['inventariantes']){
                    foreach(Yii::$app->request->post()['Usuarioenderecamento']['inventariantes'] as $key => $value){
                        if($value['usuario_id']){
                            $referencia = new Usuarioenderecamento();
                            $referencia->inventario_id = $inventario->id;
                            $referencia->attributes = $value;

                            if(intval($referencia->inicio) > $ultimo_enderecamento_numerico){
                                $transaction->rollBack();
                                $mensagem = "endereçamento inicial maior que o tamanho do inventário";
                                return $this->render('create', [
                                    'model' => $model,
                                    'inventarios' => $inventarios,
                                    'inventariantes' => $inventariantes,
                                    'usuarios_enderecamentos' => $usuarios_enderecamentos,
                                    'all_enderecamentos'=>$all_enderecamentos,
                                    'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
                                    'mensagem'=>$mensagem
                                ]);
                            }

                            if(intval($referencia->fim) > $ultimo_enderecamento_numerico){
                                $transaction->rollBack();
                                $mensagem = "endereçamento final maior que o tamanho do inventário";
                                return $this->render('create', [
                                    'model' => $model,
                                    'inventarios' => $inventarios,
                                    'inventariantes' => $inventariantes,
                                    'usuarios_enderecamentos' => $usuarios_enderecamentos,
                                    'all_enderecamentos'=>$all_enderecamentos,
                                    'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
                                    'mensagem'=>$mensagem
                                ]);
                            }

                            if( Usuarioenderecamento::associacaoValida($inventario,$referencia->inicio,$referencia->fim) == false ){
                                $transaction->rollBack();
                                $mensagem = "Conflito nos endereçamentos! Por favor verifique se selecionou apenas endereçamentos livres.";
                                return $this->render('create', [
                                    'model' => $model,
                                    'inventarios' => $inventarios,
                                    'inventariantes' => $inventariantes,
                                    'usuarios_enderecamentos' => $usuarios_enderecamentos,
                                    'all_enderecamentos'=>$all_enderecamentos,
                                    'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
                                    'mensagem'=>$mensagem
                                ]);
                            }


                            $log_inicio = $referencia->inicio;
                            $log_fim = $referencia->fim;
                            $log_usuario_id = $referencia->usuario_id;

                            for($i = intval($referencia->inicio); $i <= intval($referencia->fim); $i++){
                                $aux = new Usuarioenderecamento();
                                $aux->inventario_id = $inventario->id;
                                $aux->attributes = $value;

                                if($i < 10){
                                    $end = Enderecamento::find()->where(['inventario_id'=>$inventario->id, 'descricao'=>$prefixo_end.'-0'.$i])->one();
                                    if($end){
                                        $aux->enderecamento_id = $end->id;
                                    }
                                }else{
                                    $end = Enderecamento::find()->where(['inventario_id'=>$inventario->id, 'descricao'=>$prefixo_end.'-'.$i])->one();
                                    if($end){
                                        $aux->enderecamento_id = $end->id;
                                    }
                                }
                                array_push($lista_usu_enderecamentos, $aux);
                            }
                        }
                        
                    }
                }

                foreach($lista_usu_enderecamentos as $usu){
                    if(!$usu->save()){
                        $transaction->rollBack();
                        $mensagem = "erro ao salvar equipe";
                        return $this->render('create', [
                            'model' => $model,
                            'inventarios' => $inventarios,
                            'inventariantes' => $inventariantes,
                            'usuarios_enderecamentos' => $usuarios_enderecamentos,
                            'all_enderecamentos'=>$all_enderecamentos,
                            'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
                            'mensagem'=>$mensagem
                        ]);
                    }
                }
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'atribuir enderecamentos',
                    'Equipe',
                    "Atribuiu enderecamentos : ({$model->usuario_id}) inventariante: ".Usuario::findOne($log_usuario_id)->nome." | inicio: {$prefixo_end}-{$log_inicio} Fim: {$prefixo_end}-{$log_fim}"
                );
                $transaction->commit();
                $mensagem = "sucesso";

                // return $this->redirect(['/inventario/view','id'=>$inventario->id,'message'=>$mensagem]);
                return $this->redirect(['create','invid'=>$inventario->id,'message'=>$mensagem]);

            } catch (\Throwable $th) {
                //throw $th;
                $transaction->rollBack();
                $mensagem = "erro ao salvar equipe - t";
                return $this->render('create', [
                    'model' => $model,
                    'inventarios' => $inventarios,
                    'inventariantes' => $inventariantes,
                    'usuarios_enderecamentos' => $usuarios_enderecamentos,
                    'all_enderecamentos'=>$all_enderecamentos,
                    'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
                    'mensagem'=>$mensagem
                ]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'inventarios' => $inventarios,
            'inventariantes' => $inventariantes,
            'usuarios_enderecamentos' => $usuarios_enderecamentos,
            'all_enderecamentos'=>$all_enderecamentos,
            'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Updates an existing Usuarioenderecamento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuarioenderecamento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($i,$f)
    {
        $inventario_id = null;
        $log_inicio = Enderecamento::findOne($i);
        $log_fim    = Enderecamento::findOne($f);
        $log_usuario_id = '';

        $transaction = Yii::$app->db->beginTransaction();
        try {
            //code...
            for($cont = intval($i); $cont <= intval($f); $cont++){
                $usu_end = Usuarioenderecamento::find()->where(['enderecamento_id'=>$cont])->one();
                $inventario_id = $usu_end->inventario_id;
                $log_usuario_id = $usu_end->usuario_id;
                $usu_end->delete();
            }
    
            $log = Loghistorico::salvarLogHistorico(
                intval(Yii::$app->user->identity->usuario_id),
                'excluir atribuitcao de enderecamentos',
                'Equipe',
                "excluiu enderecamentos : inventariante: ".Usuario::findOne($log_usuario_id)->nome." | inventario: ".Inventario::findOne($inventario_id)->numero_inventario." | inicio: {$log_inicio->descricao} Fim: {$log_fim->descricao}"
            );
            $transaction->commit();

        } catch (\Throwable $th) {
            $transaction->rollBack();
            return $this->redirect(['create','invid'=>$inventario_id,'message'=>'erro ao deletar']);
        }
        

        return $this->redirect(['create','invid'=>$inventario_id]);
    }

    /**
     * Finds the Usuarioenderecamento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuarioenderecamento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuarioenderecamento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
