<?php

namespace app\controllers;

use Yii;
use app\models\Inventario;
use app\models\Contrato;
use app\models\Agendamento;
use app\models\Enderecamento;
use app\models\Loja;
use app\models\Loghistorico;
use app\models\InventarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InventarioController implements the CRUD actions for Inventario model.
 */
class InventarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Inventario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InventarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inventario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);
        $model = Inventario::getFiltrosMarcados($model);
        $agendamento = Agendamento::findOne($model->agendamento_id);
        $agendamento->data_agendamento = Contrato::dateBrParaDateString($agendamento->data_agendamento);
        $loja = Inventario::getLoja($model->agendamento_id);
        $mensagem="";
        $enderecamento = new Enderecamento();
        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$model->id])->all();
        return $this->render('view-novo', [
            'model' => $model,
            'loja'=>$loja,
            'agendamento'=>$agendamento,
            'mensagem'=>$mensagem,
            'enderecamento'=>$enderecamento,
            'enderecamentos'=>$enderecamentos
        ]);
    }


    // Verifica a lista de filtros que foi passada pelo atributo filtros, e atribui a valorização "sim" para as que estavam marcadas
    // para os casos de remoção, as que não estao marcadas recebem valor vazio
    public function verificaConfiguracaoInventario($inventario){
        $opcoes = Inventario::ARRAY_FILTROS_INVENTARIO;
        $filtros = $inventario->filtros;

        if(!$filtros){
            $filtros = [];
        }

        foreach($opcoes as $key => $opcao){
            switch ($opcao) {
                case 'ignorar_zero_esq':
                    if(in_array($opcao, $filtros)){
                        $inventario->ignorar_zero_esq = 'SIM';
                    }else{
                        $inventario->ignorar_zero_esq = 'NAO';
                    }
                    break;
                case 'validade':
                    if(in_array($opcao, $filtros)){
                        $inventario->validade = 'SIM';
                    }else{
                        $inventario->validade = 'NAO';
                    }
                    break;
                case 'fabricacao':
                    if(in_array($opcao, $filtros)){
                        $inventario->fabricacao = 'SIM';
                    }else{
                        $inventario->fabricacao = 'NAO';
                    }
                    break;
                case 'lote':
                    if(in_array($opcao, $filtros)){
                        $inventario->lote = 'SIM';
                    }else{
                        $inventario->lote = 'NAO';
                    }
                    break;
                case 'itens_embalagem':
                    if(in_array($opcao, $filtros)){
                        $inventario->itens_embalagem = 'SIM';
                    }else{
                        $inventario->itens_embalagem = 'NAO';
                    }
                    break;
                case 'marca':
                    if(in_array($opcao, $filtros)){
                        $inventario->marca = 'SIM';
                    }else{
                        $inventario->marca = 'NAO';
                    }
                    break;
                case 'fornecedor':
                        if(in_array($opcao, $filtros)){
                        $inventario->fornecedor = 'SIM';
                    }else{
                        $inventario->fornecedor = 'NAO';
                    }
                    break;
                case 'descricao':
                    if(in_array($opcao, $filtros)){
                        $inventario->descricao = 'SIM';
                    }else{
                        $inventario->descricao = 'NAO';
                    }
                    break;
                case 'validade_por_meses':
                    if(in_array($opcao, $filtros)){
                        $inventario->validade_por_meses = 'SIM';
                    }else{
                        $inventario->validade_por_meses = 'NAO';
                    }
                    break;
                case 'ignorar_zero_direita':
                    if(in_array($opcao, $filtros)){
                        $inventario->ignorar_zero_direita = 'SIM';
                    }else{
                        $inventario->ignorar_zero_direita = 'NAO';
                    }
                    break;                    
                default:
                    # code...
                    break;
            }
            
        }

        return $inventario;
        
    }

    /**
     * Creates a new Inventario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($agid)
    {
        $model = new Inventario();
        $agendamento = Agendamento::findOne($agid);
        if($agendamento){
            $model->agendamento_id = $agid;
            $data_agendamento_aux = $agendamento->data_agendamento;
            $agendamento->data_agendamento = Contrato::dateBrParaDateString($agendamento->data_agendamento);
        }
        $loja = Inventario::getLoja($agid);
        $model->numero_inventario = Inventario::gerarNumero_inventario($agendamento, $loja);
        $mensagem = "";

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            // echo "<pre>"; print_r(Yii::$app->request->post());
            if($model->tipo_inventario == Inventario::TIPO_VARREDURA){
                $model->filtros = [];
            }else{
                if(!$model->filtros){
                    $model->filtros = ["itens_embalagem"];
                }else{
                    if(in_array("itens_embalagem", $model->filtros)){
                        array_push($model->filtros, "itens_embalagem");
                    }
                }
            }

            $model->attributes = $this->verificaConfiguracaoInventario($model);

            $model->usuario_resp_cadastro_inventario_id = Yii::$app->user->identity->usuario_id;
            $model->inventario_status = Inventario::STATUS_ATIVO;
            $agendamento->load(Yii::$app->request->post());
            
            $transaction = Yii::$app->db->beginTransaction();
            if($model->save()){
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'CADASTRAR inventario',
                    'inventario',
                    "CADASTROU inventario: ({$model->id}) {$model->numero_inventario}"
                );
                $agendamento->usuario_id = $model->usuario_coordenador_id;
                if($agendamento->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'alterar agendamento',
                        'inventario',
                        "alterou agendamento: ({$agendamento->id}) data antiga: {$data_agendamento_aux} | nova: {$agendamento->data_agendamento}"
                    );
                    $transaction->commit();
                    return $this->redirect(['view','id'=>$model->id]);
                }else{
                    $mensagem="Erro ao salvar data agendamento.";
                    $transaction->rollback();    
                }
            }else{
                $mensagem="Erro ao salvar configuração de inventario.";
                $transaction->rollback();
            }

            // echo "<pre>"; print_r(Yii::$app->request->post());
            // echo "<pre>"; print_r($agendamento->attributes);
            // echo "<pre>"; print_r($model->attributes);
            // die;
        }

        return $this->render('create', [
            'model' => $model,
            'loja'=>$loja,
            'agendamento'=>$agendamento,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Updates an existing Inventario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model = Inventario::getFiltrosMarcados($model);
        $agendamento = Agendamento::findOne($model->agendamento_id);
        if($agendamento){
            $data_agendamento_aux = $agendamento->data_agendamento;
            $agendamento->data_agendamento = Contrato::dateBrParaDateString($agendamento->data_agendamento);
        }
        $loja = Inventario::getLoja($model->agendamento_id);
        $mensagem="";

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            if($model->tipo_inventario == Inventario::TIPO_VARREDURA){
                $model->filtros = [];
            }else{
                if(!$model->filtros){
                    $model->filtros = ["itens_embalagem"];
                }else{
                    if(in_array("itens_embalagem", $model->filtros)){
                        array_push($model->filtros, "itens_embalagem");
                    }
                }
            }
            $model = $this->verificaConfiguracaoInventario($model);
            $agendamento->load(Yii::$app->request->post());

            $transaction = Yii::$app->db->beginTransaction();
            if($model->save()){
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'alterar inventario',
                    'inventario',
                    "alterou inventario: ({$model->id}) {$model->numero_inventario}"
                );
                $agendamento->usuario_id = $model->usuario_coordenador_id;
                if($agendamento->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'alterar agendamento',
                        'inventario',
                        "alterou agendamento: ({$agendamento->id}) data antiga: {$data_agendamento_aux} | nova: {$agendamento->data_agendamento}"
                    );
                    $transaction->commit();
                    return $this->redirect(['view','id'=>$model->id]);
                }else{
                    $mensagem="Erro ao salvar data agendamento.";
                    $transaction->rollback();    
                }
            }else{
                $mensagem="Erro ao salvar configuração de inventario.";
                $transaction->rollback();
            }
            // echo "<pre>"; print_r(Yii::$app->request->post());
            // echo "<pre>"; print_r($agendamento->data_agendamento);
            // echo "<pre>"; print_r($model->attributes);
            // die;
        }

        return $this->render('update', [
            'model' => $model,
            'loja'=>$loja,
            'agendamento'=>$agendamento,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Deletes an existing Inventario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Throwable $th) {
            return $this->redirect(['/site/error',[
                'message'=>'erro ao deletar'
            ]]);
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Inventario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inventario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inventario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
