<?php

namespace app\controllers;

use Yii;
use app\models\Enderecamento;
use app\models\Inventario;
use app\models\Loghistorico;
use app\models\EnderecamentoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Usuario;
use Mpdf\Mpdf;
use Mpdf\Barcode;
use Mpdf\Tag;

/**
 * EnderecamentoController implements the CRUD actions for Enderecamento model.
 */
class EnderecamentoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Enderecamento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EnderecamentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Enderecamento model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionConfigimpressora(){
        return $this->render('configuracoes-impressoa');
    }

    public function actionGerarimpressao($invid=''){

        $prefixo = "";
        $tem_prefixo = false;
        $enderecamento = new Enderecamento();
        $mensagem = "";
        if($invid){
            $enderecamento_aux = Enderecamento::find()->where(['inventario_id'=>$invid])->one();
            if($enderecamento_aux){
                $prefixo = Enderecamento::getPrefixo($enderecamento_aux);
                $enderecamento->inventario_id = $enderecamento_aux->inventario_id;
                $tem_prefixo = true;
            }
        }

        if(Yii::$app->request->post()){
            $enderecamento->load(Yii::$app->request->post());
            $this->redirect([
                'impressao','p'=>$enderecamento->prefixo,
                'i'=>$enderecamento->inicio, 
                'f'=>$enderecamento->fim
                ]);
        }

        return $this->render('gerar_impressao', [
            'model' => $enderecamento,
            'tem_prefixo' => $tem_prefixo,
            'prefixo' => $prefixo,
            'mensagem'=>$mensagem
        ]);

    }

    // $p = prefixo
    // $i = inicio sequencial
    // $f = fim sequencial
    public function actionImpressao($p,$i,$f){

        $enderecamentos = [];
        $tamanho = 20;
        for($cont = $i; $cont <= $f; $cont++){
            $cont_aux = $cont;
            if($cont_aux < 10){
                $cont_aux = "0".$cont_aux;
            }
            $enderecamento = strtoupper($p)."-".$cont_aux;
            array_push($enderecamentos, $enderecamento);
        }

        if(count($p)<=2){
            $tamanho = 5;
            if(count($p)<2){
                $tamanho = 0;
            }
        }

    $inicio_pagina = "
    <html>
        <head>
            
            <link href='../web/css/css-arko/paper.css' rel='stylesheet' />
            <style>

                body {
                    font-family: sans-serif;
                    font-size: 10pt;
                    align: center;
                }
                h5, p {	
                    margin: 0pt;
                }
                table.items {
                    
                    /*font-size: 9pt;*/
                    border-collapse: collapse;
                    /*border: 3px solid #880000;*/
                }
                td { 
                    margin-top: 10px;
                    vertical-align: top;
                }
                table thead td { 
                    background-color: #EEEEEE;
                    text-align: center;
                }
                table tfoot td { 
                    background-color: #AAFFEE;
                    text-align: center;
                }
                .barcode {
                    padding: 1.5mm;
                    margin: 0;
                    vertical-align: top;
                    color: #000000;
                }
                .barcodecell {
                    text-align: center;
                    vertical-align: middle;
                    padding: 0;
                }

                table {
                    /*background: tomato;*/
                }

                img {
                    margin: {$tamanho}% 0% 0 0 ;
                }

                // @page print{
                //     margin-top: 30mm;
                //     size: 40mm 40mm;
                    
                // }

            </style>
        </head>
        <body>   
            <div>
    ";

    $corpo_pagina = "";

    foreach ($enderecamentos as $key => $value) {
        // $corpo_pagina .= "
        //     <table>
        //         <tbody>
        //             <tr>
        //                 <td class='img' align='center' >
        //                     <img src='../web/images/logo-login.jpg' width='100px' height='60px' />
        //                 </td>
        //             </tr>
        //             <hr>
        //             <tr>
        //                 <td class='barcodecell'>
        //                     <barcode code='{$value}' type='C128A' class='barcode' />
        //                 </td>
        //             </tr>
        //             <tr>
        //                 <td align='center'>{$value}</td>
        //             </tr>
        //         </tbody>
        //     </table>
        //     <pagebreak>
        // ";
        $corpo_pagina .= "
            <table>
                <tbody>
                    <tr>
                        <td class='img' align='center' >
                            <img src='../web/images/logo-login.jpg' width='100px' height='60px' />
                        </td>
                    </tr>
                    <hr>
                    <tr>
                        <td class='barcodecell'>
                            <barcode code='{$value}' type='C128A' class='barcode' />
                        </td>
                    </tr>
                    <tr>
                        <td align='center'>{$value}</td>
                    </tr>
                </tbody>
            </table>
        ";
        if($key != (count($enderecamentos)  - 1)){
            $corpo_pagina .= "<pagebreak>";
        }
    }
    
    $fim_pagina = "
            </div>
        </body>
    </html>
    ";

    $pagina = $inicio_pagina.$corpo_pagina.$fim_pagina;

    $mpdf = new Mpdf(['format' => [40, 40]]);
    $mpdf->showImageErrors = true;
    $mpdf->AddPage('P','','','','',0,0,3,0);
    $mpdf->WriteHTML(
        $pagina
    );
    
    $mpdf->Output();

        
    }

    // a importação dos endereçamentos montam um sequencial proprio da arko equivalente a queantidade de items no arquivo
    // adicionando a descrição do proprietario do atributo equivalente
    // o hífem mostrado no visualizar, é adicionado na view, contatenando os 2 atributos do objeto trazido do banco
    public function actionListarenderecamentos($invid){

        $inventario = Inventario::findOne($invid);
        $listaSequenciais = Enderecamento::getListaSequenciaisPorInventario($inventario->id);

        // echo "<pre>";print_r($listaSequenciais);die;

        return $this->render('view-listasequenciais', [
            'inventario'=>$inventario,
            'listasequenciais'=>$listaSequenciais
        ]);
    }

    // a importação dos endereçamentos montam um sequencial proprio da arko equivalente a queantidade de items no arquivo
    // adicionando a descrição do proprietario do atributo equivalente
    // o hífem mostrado no visualizar, é adicionado na view, contatenando os 2 atributos do objeto trazido do banco
    public function actionImportarenderecamentos($invid){

        $model = new Enderecamento();
        $model->inventario_id = $invid;
        $ultimo_sequencial = Enderecamento::getUltimoSequencial($model);
        // echo "<pre>";print_r($ultimo_sequencial);die;
        $enderecamento_aux = Enderecamento::find()->where(['inventario_id'=>$model->inventario_id])->one();
        $tem_prefixo = false;
        if($enderecamento_aux){
            $model->prefixo = explode('-',$enderecamento_aux->descricao)[0];
            $tem_prefixo = true;
        }
        $mensagem = "";
        $arquivo = '';
        if(Yii::$app->request->post()){

            $arquivo = UploadedFile::getInstance($model, 'arquivo'); 

            $model->load(Yii::$app->request->post());
            $model->arquivo = $arquivo;
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($arquivo){
                    if(explode('.',$model->arquivo->name)[1] == 'csv'){
                        $tem_coluna = false;
                        $localizacao_da_coluna = 0;
                        $nome_da_coluna = $model->nome_coluna_enderecamento;
                        $array_para_insert = [];
                        // o "$ultimo_sequencial" sempre retorna 1 a mais, assim, se não houver nenhum, ele parte do 1, se tiver, ele parte do proximo sequencial 
                        $contador = $ultimo_sequencial; 
                        $tem_valor_vazio = false;
                        $arquivo = fopen ($model->arquivo->tempName, 'r');
                        while (!feof($arquivo)) {
                            $linha = fgets($arquivo, 1024);
                            $dados = explode(';', $linha);
                            if($tem_coluna == false){
        
                                foreach($dados as $key=>$value){
                                    if(trim($value)  == trim($nome_da_coluna)){
                                        $localizacao_da_coluna = $key;
                                        $tem_coluna = true;
                                    }
                                }
        
                                if($tem_coluna == false){
                                    $mensagem="O arquivo selecionado não contem a coluna informada";
                                    $transaction->rollBack();
                                    break;
                                }
        
                            }else{
                                if($dados[0] != trim($nome_da_coluna) && !empty($linha)){
                                    foreach($dados as $key=>$value){
                                        
                                        if($key == $localizacao_da_coluna){
                                            if($tem_valor_vazio == false){
                                                if(trim($value) != ""){
                                                    $sequencial_para_concatenar = "";
                                                    if($contador < 10){
                                                        $sequencial_para_concatenar = 0;
                                                    }
                                                    array_push(
                                                        $array_para_insert,
                                                        [
                                                            // $dados[0], //sequencial teste da planilha medical (não é persistido)
                                                            $model->inventario_id, // numero do inventario
                                                            strtoupper($model->prefixo)."-".$sequencial_para_concatenar.$contador, //endereçamento sequencial referencia arko
                                                            $value //endereçamento referencia proprietario
                                                        ]
                                                    );
                                                    $contador++;
                                                }else{
                                                    $tem_valor_vazio = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if($tem_valor_vazio == true){
                                $mensagem = "Coluna selecionada contem valores vazio, por favor verifique.";
                                $transaction->rollBack();
                                break;
                            }
                        }
                        
                        fclose($arquivo);
                        if(Yii::$app->db->createCommand()->batchInsert(
                            'enderecamento', 
                            ['inventario_id', 'descricao', 'descricao_proprietario'], 
                            $array_para_insert
                            )->execute()
                        ){
                            $log = Loghistorico::salvarLogHistorico(
                                intval(Yii::$app->user->identity->usuario_id),
                                'Importar Enderecamento',
                                'Enderecamento',
                                "importou enderecamento: prefixo: {$model->prefixo} | inicio: ".$array_para_insert[0][1]." | fim: ".$array_para_insert[count($array_para_insert)-1][1]
                            );
                            $transaction->commit();
                            $mensagem = "sucesso";
                            return $this->redirect(['inventario/view', 'id' => $model->inventario_id,'mensagem'=>'sucesso']);
                        }else{
                            $mensagem = "erro ao salvar registros";
                            $transaction->rollBack();    
                        }
                        
    
                        // die;
                    }else{
                        $mensagem = "arquivo não é *.csv";
                        $transaction->rollBack();
                    }
                }else{
                    $mensagem = "arquivo não selecionado";
                    $transaction->rollBack();
                }
            } catch (\Throwable $th) {
                //throw $th;
                $mensagem = "Erro ao tentar extrair os dados";
                $transaction->rollBack();
            }
            

        }

        return $this->render('importar', [
            'model' => $model,
            'ultimo_sequencial'=>$ultimo_sequencial,
            'mensagem'=>$mensagem,
            'tem_prefixo'=>$tem_prefixo
        ]);
    }

    /**
     * Creates a new Enderecamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($invid)
    {
        $model = new Enderecamento();
        $model->inventario_id = $invid;
        $ultimo_sequencial = Enderecamento::getUltimoSequencial($model);
        $enderecamento_aux = Enderecamento::find()->where(['inventario_id'=>$model->inventario_id])->one();
        
        $tem_prefixo = true;
        if($enderecamento_aux){
            $model->prefixo = explode('-',$enderecamento_aux->descricao)[0];
        }else{
            $model->prefixo = 'ARKO';
        }
        $mensagem = "";

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->inventario_id = $invid;

            $array_enderecamentos_sequenciais = Enderecamento::montaListaEnderecamentosSequencial($model);

            $transaction = Yii::$app->db->beginTransaction();


            try {
                foreach($array_enderecamentos_sequenciais as $key=>$value){
                    $enderecamento = new Enderecamento();
                    $enderecamento->inventario_id = $invid;
                    $enderecamento->descricao = $value;
                    if(!$enderecamento->save()){
                        $mensagem = "erro ao salvar enderecamentos!";
                        $transaction->rollback();
                        $this->redirect(['/inventario/view','id'=>$invid,'mensagem'=>$mensagem]);
                    }
                    
                }
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'Cadastrar Enderecamento',
                    'Enderecamento',
                    "cadastrou enderecamento: prefixo: {$model->prefixo} | inicio: {$model->inicio} | fim: {$model->fim} "
                );
                $transaction->commit();
                $mensagem = "sucesso";
                $this->redirect(['/inventario/view','id'=>$invid,'mensagem'=>$mensagem]);
            } catch (\Throwable $th) {
                $mensagem = "erro ao salvar enderecamentos! t";
                $transaction->rollback();
                $this->redirect(['/inventario/view','id'=>$invid,'mensagem'=>$mensagem]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'ultimo_sequencial'=>$ultimo_sequencial,
            'tem_prefixo'=>$tem_prefixo
        ]);
    }

    /**
     * Updates an existing Enderecamento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionExcecao($invid){
        $model = new Enderecamento();
        $inventario = Inventario::findOne($invid);
        $inventarios = Inventario::find()->where(['inventario_status'=>Inventario::STATUS_ATIVO])->orderBy(['numero_inventario'=>SORT_ASC])->all();
        $intervalo_de_enderecamentos = Enderecamento::getEnderecamentosPrimeiroEUltimo($inventario->id);
        $enderecamentos_excecao = Enderecamento::find()->where(['inventario_id' => $inventario->id, 'excecao'=>Enderecamento::EXCECAO_SIM])->all();
        $prefixo_end = Enderecamento::getPrefixo(Enderecamento::find()->where(['inventario_id'=>$inventario->id])->one());
        $model->inventario_id = $inventario->id;
        $all_enderecamentos = Enderecamento::getEnderecamentos($inventario->id);
        $mensagem = "";
        $inventariantes = Usuario::find()->where(['cargo'=>Usuario::INVENTARIANTE])->orderBy(['nome'=>SORT_ASC])->all();

        if(Yii::$app->request->post()){
            $model = Enderecamento::findOne(Yii::$app->request->post('Enderecamento')['id']);
            $model->load(Yii::$app->request->post());            
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'Alterar Enderecamento',
                        'Enderecamento',
                        "Alterou enderecamento: {$model->descricao} de Exceção para Sem Excecão "
                    );

                    // se o inventario de tipo "varredura" tiver exceção, habilitar o campo "qtd_itens"
                    // caso contratio, desabilitar

                    if($inventario->tipo_inventario == Inventario::TIPO_VARREDURA){
                        if($inventario->itens_embalagem == "NAO"){
                            $inventario->itens_embalagem = "SIM";
                            $inventario->save();
                        }
                    }

                    $transaction->commit();
                    $this->redirect(['/enderecamento/excecao','invid'=>$invid]);
                }else{
                    $transaction->rollBack();
                }

            } catch (\Throwable $th) {
                $transaction->rollBack();
                $this->redirect(['/enderecamento/excecao','invid'=>$invid]);
            }
            
        }

        return $this->render('_form-excecao', [
            'model' => $model,
            'inventarios' => $inventarios,
            'all_enderecamentos'=>$all_enderecamentos,
            'intervalo_de_enderecamentos'=>$intervalo_de_enderecamentos,
            'enderecamentos_excecao'=>$enderecamentos_excecao,
            'inventariantes'=>$inventariantes,
            'mensagem'=>$mensagem
        ]);

    }

    public function actionRemoverexcecao($id){
        $model = $this->findModel($id);
        if($model){
            $invid = $model->inventario_id;
            $model->excecao = Enderecamento::EXCECAO_NAO;

            // como o usuário está removendo uma exceção, caracteriza que esse inventario tem exceções
            $tem_excecao = true;

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'Alterar Enderecamento',
                        'Enderecamento',
                        "Alterou enderecamento: {$model->descricao} de Exceção para Sem Excecão "
                    );

                    // caso seja removida a ultima exceção removida, o inventário volta a ter o campo "itens_embalagem" desabilitado
                    $inventario = Inventario::find()->where(['id'=>$invid])->one();
                    if($inventario->tipo_inventario == Inventario::TIPO_VARREDURA){
                        $enderecamentos = Enderecamento::find()->where(['inventario_id'=>$invid, 'excecao'=>Enderecamento::EXCECAO_SIM])->all();

                        if(!$enderecamentos){
                            if($inventario->itens_embalagem == "SIM"){
                                $inventario->itens_embalagem = "NAO";
                                $inventario->save();
                            }
                        }
                    }

                    $transaction->commit();
                }else{
                    $transaction->rollBack();
                }
                $this->redirect(['/enderecamento/excecao','invid'=>$invid]);
            } catch (\Throwable $th) {
                $this->redirect(['/enderecamento/excecao','invid'=>$invid]);
            }
        }else{
            $this->redirect(['/enderecamento/excecao','invid'=>$invid]);
        }
    }

    /**
     * Deletes an existing Enderecamento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionZerarenderecamentos($invid)
    {
        $inventario = Inventario::findOne($invid);
        $enderecamentos = Enderecamento::getEnderecamentos($inventario->id);
        $inicio = null;
        $fim = null;

        foreach($enderecamentos as $key => $enderecamento){
            if($key == 0){
                $inicio = $enderecamento->descricao;
            }else{
                $fim = $enderecamento->descricao;
            }
            
            $enderecamento->delete();
        }

        $log = Loghistorico::salvarLogHistorico(
            intval(Yii::$app->user->identity->usuario_id),
            'zerar Enderecamento',
            'Enderecamento',
            "zerou enderecamentos: inicio: {$inicio} | fim: {$fim} "
        );

        return $this->redirect(['listarenderecamentos','invid'=>$inventario->id]);
    }

    /**
     * Finds the Enderecamento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Enderecamento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Enderecamento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
