<?php

namespace app\controllers;

use Yii;
use app\models\Contrato;
use app\models\ContratoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Loja;
use yii\web\UploadedFile;
use app\models\Loghistorico;

/**
 * ContratoController implements the CRUD actions for Contrato model.
 */
class ContratoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contrato models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContratoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contrato model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewallcontratos($lid)
    {
        $loja = Loja::findOne($lid);
        $contratos = Contrato::find()->where(['loja_id'=>$lid])->all();

        return $this->render('view-all-contratos', [
            'loja'=>$loja,
            'contratos' => $contratos,
        ]);
    }

    /**
     * Creates a new Contrato model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contrato();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contrato model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $contrato = $this->findModel($id);
        $loja = Loja::findOne($contrato->loja_id);
        $contrato->data_adesao = Contrato::dateBrParaDateString($contrato->data_adesao);
        $contrato->data_vencimento = Contrato::dateBrParaDateString($contrato->data_vencimento);
        $mensagem = "";

        if(Yii::$app->request->post()){
            $contrato_aux = $contrato->contrato_arquivo;
            $contrato->attributes = Yii::$app->request->post()['Contrato'];
            $arquivo = UploadedFile::getInstance($contrato, 'contrato_arquivo'); 
            if($arquivo == ""){
                $contrato->contrato_arquivo = $contrato_aux;
                if ($contrato->save()) {
                    return $this->redirect(['/loja/view', 'id' => $contrato->loja_id]);
                }
            }else{
                $transaction = Yii::$app->db->beginTransaction();
                $contrato->contrato_arquivo = $arquivo; 
                try {
                    if(Contrato::verificaAdesaoVencimento($contrato)){
                        if($contrato = Contrato::salvarContrato($loja,$contrato)){
                            if($contrato->validate()){
                                if($contrato->save()){
                                    $log = Loghistorico::salvarLogHistorico(
                                        intval(Yii::$app->user->identity->usuario_id),
                                        'Alterar Contrato',
                                        'Contrato',
                                        "alterou contrato: ({$contrato->id}) {$contrato->contrato_numero}"
                                    );
                                    // echo "<pre>"; print_r($contrato->attributes);die;
                                    $transaction->commit();
                                    return $this->redirect(['/loja/view', 'id' => $contrato->loja_id]);
                                }
                            }else{
                                $mensagem = "Revise os campos de contrato";
                                $transaction->rollBack();
                            }
                        }else{
                            $mensagem = "Erro ao salvar contrato";
                            $transaction->rollBack();
                        }
                    }else{
                        $mensagem = "Datas de adesão e vencimento inválidas";
                        $transaction->rollBack();
                    }

                } catch (\Throwable $th) {
                    $transaction->rollBack();
                    $mensagem = "throws";
                }
                // echo "<pre>"; print_r($contrato->attributes);die;
                
            }
            
            
        }
        
        
        // if(Yii::$app->request->post()){

        //     if($model->load(Yii::$app->request->post())){
        //         if ($model->save()) {
        //             return $this->redirect(['view', 'id' => $model->id]);
        //         }
        //     }
        // }
    
        return $this->render('update', [
            'model' => $contrato,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Deletes an existing Contrato model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contrato model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contrato the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contrato::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
