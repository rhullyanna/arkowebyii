<?php

namespace app\controllers;

use Yii;
use app\models\Loja;
use app\models\LojaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Responsavel;
use app\models\Loghistorico;
use yii\web\UploadedFile;
use app\models\Contrato;
use app\models\Agendamento;
use yii\helpers\Json;


/**
 * LojaController implements the CRUD actions for Loja model.
 */
class LojaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Loja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LojaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetinventarios($id){
        $inventarios = Loja::getInventarios($id);
        return Json::encode($inventarios);
    }

    public function actionGetlojas(){
        $lojas = Loja::find()->all();
        return Json::encode($lojas);
    }

    /**
     * Displays a single Loja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $responsavel = Responsavel::findOne($model->responsavel_id);
        $contratos = Contrato::find()->where(['loja_id'=>$model->id])->all();
        $contrato = new Contrato();
        $contrato_aux = new Contrato();
        $mensagem = "";

        if(Yii::$app->request->post()){
            $contrato_aux->attributes = Yii::$app->request->post()['Contrato'];
            $contrato->attributes = Yii::$app->request->post()['Contrato'];
            $contrato->loja_id = $model->id;
            $contrato->contrato_status = Contrato::STATUS_ABERTO;
            // Contrato::verificaSobreposicaoDeContrato($contrato);
            // $contrato->data_adesao = Contrato::dateToDateBR($contrato->data_adesao);
            // $contrato->data_vencimento = Contrato::dateToDateBR($contrato->data_vencimento);
            $contrato->contrato_arquivo = UploadedFile::getInstance($contrato, 'contrato_arquivo'); 
            // echo "<pre>"; print_r($contrato->attributes);die;
            $contratos_sobrepostos = Contrato::verificaSobreposicaoDeContrato($contrato);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if(!$contratos_sobrepostos){
                    if(Contrato::verificaAdesaoVencimento($contrato)){
                        if($contrato = Contrato::salvarContrato($model,$contrato)){
                            $contrato->contrato_status = Contrato::STATUS_ABERTO;   
                            if($contrato->validate()){
                                if($contrato->save()){
                                    $log = Loghistorico::salvarLogHistorico(
                                        intval(Yii::$app->user->identity->usuario_id),
                                        'CADASTRAR Contrato',
                                        'Contrato',
                                        "CADASTROU contrato: ({$contrato->id}) {$contrato->contrato_numero}"
                                    );
    
                                    $datas_pre_agendamento = Contrato::dividirOtempoDeContratoPeloNumeroDeInventarios($contrato);
                                    if($datas_pre_agendamento){
                                        foreach($datas_pre_agendamento as $key => $value){
                                            $agendamento = new Agendamento();
                                            $agendamento = Agendamento::preAgendamentoAutomaticoDeInventario($contrato->id, $value);
                                            // echo "<pre>";print_r($agendamento->attributes);die;
                                            if($agendamento->save()){
                                                $log = Loghistorico::salvarLogHistorico(
                                                    intval(Yii::$app->user->identity->usuario_id),
                                                    'CADASTRO AGENDAMENTO',
                                                    'Agendamento',
                                                    "pre agendou automaticamento contrato : ({$contrato->id}) {$contrato->contrato_numero} | data: {$value}"
                                                );
                                            }else{
                                                $mensagem = "Erro ao salvar pre agendamento";
                                                $transaction->rollBack();
                                                return $this->render('view-com-responsavel', [
                                                    'model' => $model,
                                                    'responsavel'=>$responsavel,
                                                    'contratos' => $contratos,
                                                    'contrato'=>$contrato,
                                                    'mensagem' => $mensagem
                                                ]);
                                            }
                                        }
        
                                        $transaction->commit();
                                        return $this->redirect(['view', 'id' => $model->id]);
                                    }else{
                                        $mensagem = "A quantidade de inventários excedeu a quantidade de dias do contrato.";
                                        $transaction->rollBack();
                                    }
                                }else{
                                    $mensagem = "Erro ao salvar contrato";
                                    $transaction->rollBack();
                                }
                            }else{
                                $mensagem = "Revise os campos de contrato";
                                $transaction->rollBack();
                            }
                        }else{
                            $contrato = $contrato_aux;
                            $mensagem = "Erro ao salvar arquivo contrato";
                            $transaction->rollBack();
                        }                            
                    }else{
                        $mensagem = "Datas de adesão e vencimento inválidas";
                        $transaction->rollBack();
                    }
                }else{
                    $texto = "";
                    foreach($contratos_sobrepostos as $contrato_numero){
                        if($texto == ""){
                            $texto = $contrato_numero."; ";
                        }else{
                            $texto = $texto . $contrato_numero. "; ";
                        }
                    }
                    $mensagem = "Datas invalidas, contratos sobrepostos: <b>".$texto."</b>";
                    $transaction->rollBack();
                }
                
            } catch (\Throwable $th) {
                $mensagem = "Erro ao salvar contrato, verifique se adicionou o arquivo.";
                $transaction->rollBack();
            }
        }
        return $this->render('view-com-responsavel', [
            'model' => $model,
            'responsavel'=>$responsavel,
            'contratos' => $contratos,
            'contrato'=>$contrato,
            'mensagem' => $mensagem
        ]);
    }

    public function actionAlterarstatus($id){
        $model = $this->findModel($id);
        if(Yii::$app->request->get()){

            if($model->status == Loja::STATUS_ATIVO){
                $model->status = Loja::STATUS_INATIVO;
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'ALTERAR STATUS LOJA',
                        'LOJA',
                        "desativou LOJA: ({$model->id}) {$model->razao_social} | nome fantasia: {$model->nome_fantasia}"
                    );
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            if($model->status == Loja::STATUS_INATIVO){
                $model->status = Loja::STATUS_ATIVO;
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'ALTERAR STATUS loja',
                        'loja',
                        "ativou loja: ({$model->id}) {$model->razao_social} | nome fantasia: {$model->nome_fantasia}"
                    );
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Loja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Loja();
        $responsavel = new Responsavel();
        $contrato = new Contrato();
        $contrato_aux = new Contrato();
        $mensagem = "";

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $responsavel->attributes = Yii::$app->request->post()['Responsavel'];
            $contrato_aux->attributes = Yii::$app->request->post()['Contrato'];
            $contrato->attributes = Yii::$app->request->post()['Contrato'];
            $contrato->contrato_arquivo = UploadedFile::getInstance($contrato, 'contrato_arquivo');           

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($responsavel->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'CADASTRAR RESPONSAVEL',
                        'RESPONSAVEL',
                        "CADASTROU RESPONSAVEL: ({$responsavel->id}) {$responsavel->nome} "
                    );
                    $model->responsavel_id = $responsavel->id;
                    if($model->save()){
                        $log = Loghistorico::salvarLogHistorico(
                            intval(Yii::$app->user->identity->usuario_id),
                            'CADASTRAR Loja',
                            'loja',
                            "CADASTROU loja: ({$model->id}) {$model->razao_social} | nome fantasia: {$model->nome_fantasia}"
                        );
                        $contrato->loja_id = $model->id;
                        $contratos_sobrepostos = Contrato::verificaSobreposicaoDeContrato($contrato);
                        if(!$contratos_sobrepostos){
                            if(Contrato::verificaAdesaoVencimento($contrato)){
                                if($contrato = Contrato::salvarContrato($model,$contrato)){
                                    $contrato->contrato_status = Contrato::STATUS_ABERTO;   
                                    if($contrato->validate()){
                                        if($contrato->save()){
                                            $log = Loghistorico::salvarLogHistorico(
                                                intval(Yii::$app->user->identity->usuario_id),
                                                'CADASTRAR Contrato',
                                                'Contrato',
                                                "CADASTROU contrato: ({$contrato->id}) {$contrato->contrato_numero}"
                                            );
    
                                            $datas_pre_agendamento = Contrato::dividirOtempoDeContratoPeloNumeroDeInventarios($contrato);
    
                                            foreach($datas_pre_agendamento as $key => $value){
                                                $agendamento = new Agendamento();
                                                $agendamento = Agendamento::preAgendamentoAutomaticoDeInventario($contrato->id, $value);
                                                // echo "<pre>";print_r($agendamento->attributes);die;
                                                if($agendamento->save()){
                                                    $log = Loghistorico::salvarLogHistorico(
                                                        intval(Yii::$app->user->identity->usuario_id),
                                                        'CADASTRO AGENDAMENTO',
                                                        'Agendamento',
                                                        "pre agendou automaticamento contrato : ({$contrato->id}) {$contrato->contrato_numero} | data: {$value}"
                                                    );
                                                }else{
                                                    $mensagem = "Erro ao salvar pre agendamento";
                                                    $transaction->rollBack();
                                                    return $this->render('create', [
                                                        'model' => $model,
                                                        'responsavel' => $responsavel,
                                                        'contrato' => $contrato,
                                                        'mensagem'=>$mensagem
                                                    ]);
                                                }
                                            }
    
                                            $transaction->commit();
                                            return $this->redirect(['view', 'id' => $model->id]);
                                        }else{
                                            $mensagem = "Erro ao salvar contrato";
                                            $transaction->rollBack();
                                        }
                                    }else{
                                        $mensagem = "Revise os campos de contrato";
                                        $transaction->rollBack();
                                    }
                                }else{
                                    $contrato = $contrato_aux;
                                    $mensagem = "Erro ao salvar arquivo contrato";
                                    $transaction->rollBack();
                                }                            
                            }else{
                                $mensagem = "Datas de adesão e vencimento inválidas";
                                $transaction->rollBack();
                            }
                        }else{
                            $texto = "";
                            foreach($contratos_sobrepostos as $contrato_numero){
                                if($texto == ""){
                                    $texto = $contrato_numero."; ";
                                }else{
                                    $texto = $texto . $contrato_numero. "; ";
                                }
                            }
                            $mensagem = "Datas invalidas, contratos sobrepostos: <b>".$texto."</b>";
                            $transaction->rollBack();
                        }
                        
                    }else{
                        $mensagem = "Revise os campos de Loja";
                        $transaction->rollBack();
                    }
                }else{
                    $mensagem = "Revise os campos de Responsável";
                }
            } catch (\Throwable $th) {
                //throw $th;
                $transaction->rollBack();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'responsavel' => $responsavel,
            'contrato' => $contrato,
            'mensagem'=>$mensagem
        ]);
    }

    /**
     * Updates an existing Loja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $responsavel = Responsavel::findOne($model->responsavel_id);
        $mensagem = "";

        if(Yii::$app->request->post()){
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($model->load(Yii::$app->request->post())){
                    $responsavel->attributes = Yii::$app->request->post()['Responsavel'];
                    // echo "<pre>"; print_r($model->attributes);
                    // echo "<pre>"; print_r($responsavel->attributes);
                    // echo "<pre>"; print_r(Yii::$app->request->post());die;
                    if($responsavel->save()){
                        if($model->save()){
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->id]);
                        }else{
                            $transaction->rollBack();    
                            $mensagem = "Erro ao salvar Loja."; 
                        }
                    }else{
                        $transaction->rollBack();    
                        $mensagem = "Erro ao salvar Responsavel.";
                    }
                }else{
                    $transaction->rollBack();    
                    $mensagem = "Erro ao salvar.";
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
                $mensagem = "Erro ao salvar.";
            }
            
        }

        return $this->render('update', [
            'model' => $model,
            'responsavel'=>$responsavel,
            'mensagem' => $mensagem
        ]);
    }

    /**
     * Deletes an existing Loja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    // determinação do contrantante não ter funcionalidade deletar
    // public function actionDelete($id)
    // {
    //     try {
    //         $this->findModel($id)->delete();
    //     } catch (\Throwable $th) {
    //         return $this->redirect(['/site/error',[
    //             'message'=>'Erro ao Excluir',
    //         ]]);
    //     }
    
    //     return $this->redirect(['index']);
    // }


    /**
     * Finds the Loja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
