<?php

namespace app\controllers;

use Yii;
use app\models\Loginarko;
use app\models\LoginarkoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Usuario;
use app\models\User;
use app\models\Loghistorico;
/**
 * LoginarkoController implements the CRUD actions for Loginarko model.
 */
class LoginarkoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Loginarko models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoginarkoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Loginarko model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Loginarko model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Loginarko();
        if ($model->load(Yii::$app->request->post() ) ){

            $usuario = Usuario::find()->where(['cpf'=>$model->cpf])->one();

            $model->usuario_id = Usuario::find()->where(['cpf'=>$model->cpf])->one()->id;
            
            $model->login_status = "ATIVO";

            if($model->save()){
                
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'CADASTRAR LOGIN',
                    'LOGINARKO',
                    "Cadastrou login para o usuario: ({$model->usuario_id}) ".Usuario::findOne($model->usuario_id)->nome
                );
                $corpoEmail = 
                "
                    <h1>Confirmação de Acesso</h1>
                    <p>Usuario: {$model->cpf}</p>
                    <p>Senha: {$model->password}</p>
                    <p>Você pode acessar o sistemas com as informações acima.</p><br>
                ";
                Usuario::emailUsuario(
                    "layoutpadrao",
                    "Permissão de acesso",
                    $corpoEmail,
                    "sistemainventario@arkosolucoes.com.br",
                    $usuario->email,
                    "Arko Solutions- Permissão de acesso");

                if($log == false){
                    echo "erro no salvar log";die;
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
            die;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Loginarko model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAlterarsenha()
    {
        $model = new Loginarko();

        if (Yii::$app->request->post()){
            // echo "<pre>";print_r(Yii::$app->request->post());die;
            $model = $this->findModelByCpf(Yii::$app->request->post()['Loginarko']['cpf']);
            $usuario = Usuario::findOne($model->usuario_id);
            if($usuario->cargo != User::INVENTARIANTE){
                return json_encode(['STATUS'=>400,'message'=>'Este usuário não é inventariante!']);
            }
            $model->load(Yii::$app->request->post());
            if($model->save()){
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'ALTERAR SENHA INVENTARIANTE',
                    'LOGINARKO',
                    "alterou senha para o usuario: ({$model->usuario_id}) ".Usuario::findOne($model->usuario_id)->nome
                );
                $corpoEmail = 
                "
                    <h1>Confirmação de Acesso</h1>
                    <p>Usuario: {$model->cpf}</p>
                    <p>Senha: {$model->password}</p>
                    <p>Você pode acessar o sistemas com as informações acima.</p><br>
                ";
                Usuario::emailUsuario(
                    "layoutpadrao",
                    "Permissão de acesso",
                    $corpoEmail,
                    "sistemainventario@arkosolucoes.com.br",
                    $usuario->email,
                    "Arko Solutions- Permissão de acesso");

                if($log == false){
                    echo "erro no salvar log";die;
                }
                return json_encode(['message'=>'Salvo com sucesso!']);
            }
        }

        return $this->render('nova-senha-inventariante', [
            'model' => $model,
        ]);
    }

    public function actionAlterarstatus($id){
        $model = $this->findModel($id);
        if(Yii::$app->request->get()){

            if($model->login_status == $model->status_ativo){
                $model->login_status = $model->status_inativo;
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'ALTERAR STATUS LOGIN',
                        'LOGINARKO',
                        "desativou login Do usuario: ({$model->usuario_id}) ".Usuario::findOne($model->usuario_id)->nome
                    );
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            if($model->login_status == $model->status_inativo){
                $model->login_status = $model->status_ativo;
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'ALTERAR STATUS LOGIN',
                    'LOGINARKO',
                    "ativou login Do usuario: ({$model->usuario_id}) ".Usuario::findOne($model->usuario_id)->nome
                );
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            
        }

        

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionConsultalogin($cpf){
        if(Yii::$app->request->get()){
            $usuario = Loginarko::getUsuarioPorCpf(Yii::$app->request->get()['cpf']);
            return json_encode($usuario->attributes);
        }
    }

    public function actionConsultausuario($cpf){
        if(Yii::$app->request->get()){
            $usuario = Usuario::getUsuarioPorCpf(Yii::$app->request->get()['cpf']);
            return json_encode($usuario->attributes);
        }
    }

    /**
     * Updates an existing Loginarko model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $log = Loghistorico::salvarLogHistorico(
                intval(Yii::$app->user->identity->usuario_id),
                'ALTERAR LOGIN',
                'LOGINARKO',
                "alterou login Do usuario: ({$model->usuario_id}) ".Usuario::findOne($model->usuario_id)->nome
            );
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Loginarko model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();
    //     $log = Loghistorico::salvarLogHistorico(
    //         intval(Yii::$app->user->identity->usuario_id),
    //         'deletar LOGIN',
    //         'LOGINARKO',
    //         "deletou login : ({$id}) "
    //     );

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Loginarko model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loginarko the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loginarko::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelByCpf($cpf)
    {
        if (($model = Loginarko::find()->where(['cpf'=>$cpf])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
