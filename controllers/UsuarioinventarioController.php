<?php

namespace app\controllers;

use Yii;
use app\models\Usuarioinventario;
use app\models\UsuarioinventarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Usuario;
use app\models\Inventario;
use app\models\Loghistorico;

/**
 * UsuarioinventarioController implements the CRUD actions for Usuarioinventario model.
 */
class UsuarioinventarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuarioinventario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioinventarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuarioinventario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuarioinventario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($invid)
    {
        $model = new Usuarioinventario();
        $inventario = Inventario::findOne($invid);
        if($inventario){
            $model->inventario_id = $inventario->id;
        }
        // $usuarios_inventario = Usuarioinventario::find()->where(['inventario_id'=>$invid])->all();
        $usuarios_inventario = $model->equipeDoInventario($invid);
        $inventarios = Inventario::find()->where(['inventario_status'=>Inventario::STATUS_ATIVO])->orderBy(['numero_inventario'=>SORT_ASC])->all();
        $all_inventariantes = Usuario::find()->where(['cargo'=>Usuario::INVENTARIANTE])->orderBy(['nome'=>SORT_ASC])->all();
        $inventariantes_a_listar = [];
        

        foreach($all_inventariantes as $valor){
            if(!in_array($valor, $usuarios_inventario)){
                array_push($inventariantes_a_listar, $valor);
            }
        }
        // foreach($inventariantes_a_listar as $valor){
        //     echo "<pre>"; print_r($valor->attributes);
        // }
        // die;

        $mensagem = '';

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if(Yii::$app->request->post()['Usuarioinventario']['inventariantes']){
                    foreach(Yii::$app->request->post()['Usuarioinventario']['inventariantes'] as $key => $value){
                        if($value['usuario_id']){
                            $referencia = new Usuarioinventario();
                            $referencia->inventario_id = $inventario->id;
                            $referencia->attributes = $value;

                            if($referencia->save()){
                                $log = Loghistorico::salvarLogHistorico(
                                    intval(Yii::$app->user->identity->usuario_id),
                                    'atribuir inventario',
                                    'Equipe',
                                    "Atribuiu inventario : ". $inventario->numero_inventario ." | ({$model->usuario_id}) inventariante: ".Usuario::findOne($referencia->usuario_id)->nome
                                );
                                $transaction->commit();
                                $mensagem = "sucesso";
                                return $this->redirect(['create','invid'=>$inventario->id,'message'=>$mensagem]);
                            }else{
                                $transaction->rollBack();
                                $mensagem = "erro ao salvar equipe";
                                return $this->render('create', [
                                    'model' => $model,
                                    'usuarios_inventario' => $usuarios_inventario,                
                                    'inventarios' => $inventarios,                
                                    'inventariantes' => $inventariantes_a_listar,
                                    'mensagem' => $mensagem
                                ]);
                            }
                        }

                    }
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
                $mensagem = "erro ao salvar equipe";
                return $this->render('create', [
                    'model' => $model,
                    'usuarios_inventario' => $usuarios_inventario,                
                    'inventarios' => $inventarios,                
                    'inventariantes' => $inventariantes_a_listar,
                    'mensagem' => $mensagem
                ]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'usuarios_inventario' => $usuarios_inventario,                
            'inventarios' => $inventarios,                
            'inventariantes' => $inventariantes_a_listar,
            'mensagem' => $mensagem
        ]);
    }

    /**
     * Updates an existing Usuarioinventario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuarioinventario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($i,$u)
    {
        $inventario_id = Inventario::findOne($i)->id;
        $usuario_inventario = Usuarioinventario::find()->where(['inventario_id'=>$i,'usuario_id'=>$u])->one();

        $this->findModel($usuario_inventario->id)->delete();

        return $this->redirect(['create','invid'=>$inventario_id]);
    }

    /**
     * Finds the Usuarioinventario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuarioinventario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuarioinventario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
