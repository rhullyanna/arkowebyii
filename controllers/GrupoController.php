<?php

namespace app\controllers;

use Yii;
use app\models\Grupo;
use app\models\GrupoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Responsavel;
use yii\db\Transaction;
use yii\db\Query;
use app\models\Loghistorico;

/**
 * GrupoController implements the CRUD actions for Grupo model.
 */
class GrupoController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Grupo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GrupoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Grupo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $responsavel = Responsavel::findOne($model->responsavel_id);

        return $this->render('view-com-responsavel', [
            'model' => $model,
            'responsavel' => $responsavel,
        ]);
    }

    /**
     * Creates a new Grupo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Grupo();
        $responsavel = new Responsavel();
        $mensagem = "";

        $transaction = Yii::$app->db->beginTransaction();

        if ($model->load(Yii::$app->request->post())){
            // $model->grupo_status = Grupo::STATUS_ATIVO;
            $responsavel->attributes = Yii::$app->request->post()['Responsavel'];
            if($responsavel->validate()){
                try {
                    if($responsavel->save()){
                        $log = Loghistorico::salvarLogHistorico(
                            intval(Yii::$app->user->identity->usuario_id),
                            'CADASTRAR RESPONSAVEL',
                            'RESPONSAVEL',
                            "CADASTROU RESPONSAVEL: ({$responsavel->id}) {$responsavel->nome} "
                        );
                        $model->responsavel_id = $responsavel->id;
                        if($model->save()){
                            $log = Loghistorico::salvarLogHistorico(
                                intval(Yii::$app->user->identity->usuario_id),
                                'CADASTRAR GRUPO',
                                'GRUPO',
                                "CADASTROU GRUPO: ({$model->id}) {$model->grupo_nome} "
                            );
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->id]);
                        }else{
                            $mensagem = "erro ao salvar grupo";
                            $transaction->rollBack();
                        }
                    }else{
                        $mensagem = "erro ao salvar responsável";
                    }
                } catch (\Throwable $th) {
                    $transaction->rollBack();
                }
            }else{
                $mensagem = "revise os campos de responsavel";
            }
         
        }

        return $this->render('create', [
            'model' => $model,
            'responsavel' => $responsavel,
            'mensagem' => $mensagem
        ]);
    }

    public function actionAlterarstatus($id){
        $model = $this->findModel($id);
        if(Yii::$app->request->get()){

            if($model->grupo_status == Grupo::STATUS_ATIVO){
                $model->grupo_status = Grupo::STATUS_INATIVO;
                if($model->save()){
                    $log = Loghistorico::salvarLogHistorico(
                        intval(Yii::$app->user->identity->usuario_id),
                        'ALTERAR STATUS GRUPO',
                        'GRUPO',
                        "desativou GRUPO: ({$model->id}) {$model->grupo_nome}"
                    );
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            if($model->grupo_status == Grupo::STATUS_INATIVO){
                $model->grupo_status = Grupo::STATUS_ATIVO;
                $log = Loghistorico::salvarLogHistorico(
                    intval(Yii::$app->user->identity->usuario_id),
                    'ALTERAR STATUS GRUPO',
                    'GRUPO',
                    "ativou GRUPO Do usuario: ({$model->id}) {$model->grupo_nome} "
                );
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Grupo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);
    //     $responsavel = Responsavel::findOne($model->responsavel_id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Deletes an existing Grupo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Grupo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Grupo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Grupo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
